<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatCentroVentaCodigoPostal extends Model
{
    use HasFactory;
    protected $table = 'DatCentroVentaCodigoPostal';
    public $timestamps = false;
    protected $primaryKey = 'IdCenVtaCodPos';
}
