<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CapRecepcion extends Model
{
    use HasFactory;
    protected $table = 'CapRecepcion';
    public $timestamps = false;
}
