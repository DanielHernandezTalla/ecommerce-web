<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatClientestoOracle extends Model
{
    use HasFactory;

    protected $table = 'DatClientestoOracle';
    public $timestamps = false;
}
