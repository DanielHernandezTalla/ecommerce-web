<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatRecepcion extends Model
{
    use HasFactory;
    protected $table = 'DatRecepcion';
    public $timestamps = false;
}
