<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatServicios extends Model
{
    use HasFactory;
    protected $table = 'CatServicios';
    public $timestamps = false;
    protected $primaryKey = 'IdCatServicios';
}
