<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatMovInvDetalle extends Model
{
    use HasFactory;
    protected $table = 'DatMovInvDetalle';
    public $timestamps = false;
}
