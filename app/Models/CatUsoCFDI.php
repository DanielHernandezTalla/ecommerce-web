<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatUsoCFDI extends Model
{
    use HasFactory;
    protected $table = 'CatUsoCFDI';
    public $timestamps = false;
    protected $primaryKey = 'IdCatUsoCFDI';
}
