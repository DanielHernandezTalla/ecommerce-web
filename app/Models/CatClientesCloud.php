<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatClientesCloud extends Model
{
    use HasFactory;

    protected $table = 'CatClientesCloud';
    public $timestamps = false;
    protected $primaryKey = 'IdCatClienteCloud';

}
