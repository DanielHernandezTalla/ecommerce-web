<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\CatCategorias;
use App\Models\CatArticulosImagenes;

class DatArticulos extends Model
{
    use HasFactory;
    protected $table = 'DatArticulos';
    public $timestamps = false;
    protected $primaryKey = 'IdDatArticulos';    
}
