<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatPaquetes extends Model
{
    use HasFactory;
    protected $table = 'CatPaquetes';
    public $timestamps = false;
    protected $primaryKey = 'IdCatPaquete';
}
