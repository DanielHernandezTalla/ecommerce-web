<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatSolicitudFactura extends Model
{
    use HasFactory;

    protected $table = 'DatSolicitudFactura';
    public $timestamps = false;

    public function Ventas()
    {
        return $this->hasMany(DatVentas::class, 'IdDatVentas', 'IdDatVentas');
    }
}
