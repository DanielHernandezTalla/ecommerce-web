<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatMetododePago extends Model
{
    use HasFactory;
    protected $table = 'CatMetododePago';
    public $timestamps = false;
    protected $primaryKey = 'IdCatMetododePago';
}
