<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatCteDirEnv extends Model
{
    use HasFactory;

    protected $table = 'CatCteDirEnv';
    public $timestamps = false;

   
}