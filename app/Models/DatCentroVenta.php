<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatCentroVenta extends Model
{
    use HasFactory;
    protected $table = 'DatCentroVenta';
    public $timestamps = false;
    protected $primaryKey = 'IdDatCentroVenta';
}
