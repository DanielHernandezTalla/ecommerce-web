<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\CatCategorias;
use App\Models\CatArticulosImagenes;

class CatArticulos extends Model
{
    use HasFactory;
    protected $table = 'CatArticulos';
    public $timestamps = false;
    protected $primaryKey = 'IdCatArticulos';

    public function Categoria(){
        return $this->hasOne(CatCategorias::class, 'IdCatCategoria','IdCatCategoria');
    }

    public function ImagenArticulo(){
        return $this->hasOne(CatArticulosImagenes::class, 'IdCatArticulos','IdCatArticulos');
    }
    
}
