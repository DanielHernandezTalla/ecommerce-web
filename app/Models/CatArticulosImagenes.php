<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatArticulosImagenes extends Model
{
    use HasFactory;
    protected $table = 'CatArticuloImagenes';
    public $timestamps = false;
    protected $primaryKey = 'IdCatArticuloImagenes';
}
