<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatListaPrecio extends Model
{
    use HasFactory;
    protected $table = 'CatListaPrecio';
    public $timestamps = false;
    protected $primaryKey = 'IdCatListaPrecio';
}
