<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\DatVtaDetalle;
use App\Models\CatClientes;
use App\Models\CatCteDirEnv;

class DatVentas extends Model
{
    use HasFactory;

    protected $table='DatVentas';
    public $timestamps = false;

    public function DetallesVentas(){
        return $this->hasMany(DatVtaDetalle::class, 'IdDatVentas','IdDatVentas');
    }

    public function Cliente(){
        return $this->hasOne(CatClientes::class, 'IdUser','IdCatUsers');
    }

    public function ClienteDir(){
        return $this->hasOne(CatCteDirEnv::class, 'IdCatCteDirEnv','IdCatCteDirEnv');
    }

    
}
