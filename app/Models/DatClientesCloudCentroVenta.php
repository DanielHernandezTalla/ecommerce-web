<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatClientesCloudCentroVenta extends Model
{
    use HasFactory;

    protected $table = 'DatClientesCloudCentroVenta';

    public function Ventas()
    {
        return $this->hasMany(DatVentas::class, 'IdCatTipoPago', 'IdCatTipopago');
    }

    public function Total()
    {
        return $this->hasMany(DatVentas::class, 'IdCatTipoPago', 'IdCatTipopago');
    }
}
