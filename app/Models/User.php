<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\DatCentroVenta;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'CatUsers';

    protected $fillable = [
        'username',
        'password',
        'IdCatTipoUser',
        'IdDatCentroVenta',
        'Status'
    ];
    
    protected $primaryKey = 'IdCatUser';
    
    public $timestamps = false;

    public function CentroVenta(){
        return $this->hasOne(DatCentroVenta::class, 'IdDatCentroVenta', 'IdDatCentroVenta');
    }
}
