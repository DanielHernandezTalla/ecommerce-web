<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatDiasDisponibles extends Model
{
    use HasFactory;
    protected $table = 'CatDiasDisponibles';
    public $timestamps = false;
    protected $primaryKey = 'IdCatDiaDisponible';
}
