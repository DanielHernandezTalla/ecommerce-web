<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatMovInvConcen extends Model
{
    use HasFactory;
    protected $table = 'DatMovInvConcen';
    public $timestamps = false;

    protected $primaryKey = 'IdDatInvConcen';    
}
