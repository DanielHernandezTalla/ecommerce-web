<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$roles)
    {
        if (!Auth::check()) {
            return redirect('login');
        }

        $descripcion = Auth::user()->leftJoin('CatTipoUser', 'CatTipoUser.IdCatTipoUser', 'CatUsers.IdCatTipoUser')->where('CatUsers.IdCatUser', Auth::user()->IdCatUser)->first()->Descripcion;

        foreach ($roles as $role) {
            if (strtolower($descripcion) == $role) {
                return $next($request);
            }
        }

        return redirect()->route('unautorize');

    }
}
