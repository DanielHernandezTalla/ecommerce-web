<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientesCloudCentroVentaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'IDCATCLIENTESCLOUD' => ['required', 'numeric', 'min:1'],
            'IdTipoPago' => ['required', 'numeric', 'min:1'],
            'IdCatMetododePago' => ['required', 'numeric', 'min:1'],
            'IdDatCentroVenta' => ['required', 'numeric', 'min:1'],
            'IdCatUsoCFDI' => ['required', 'numeric', 'min:1'],
        ];
    }

    public function messages()
    {
        return [
            'IDCATCLIENTESCLOUD.required' => 'El cliente cloud es requerido',
            'IDCATCLIENTESCLOUD.min' => 'El cliente cloud es requerido',
            'IdTipoPago.required' => 'El tipo de pago es requerido',
            'IdTipoPago.min' => 'El tipo de pago es requerido',
            'IdCatMetododePago.required' => 'El método de pago es requerido',
            'IdCatMetododePago.min' => 'El método de pago es requerido',
            'IdDatCentroVenta.required' => 'El centro de venta es requerido',
            'IdDatCentroVenta.min' => 'El centro de venta es requerido',
            'IdCatUsoCFDI.required' => 'El uso CFDI es requerido',
            'IdCatUsoCFDI.min' => 'El uso CFDI es requerido',
        ];
    }
}
