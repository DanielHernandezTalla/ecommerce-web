<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ArticulosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'username' => ['required', 'max:50'],
            'Codigo' => ['required', 'string', 'between:5,7'],
            'Descripcion' => ['required', 'string'],
            'DesCorta' => ['required', 'string'],
            'DesCorta1' => [],
            'DescIva' => ['nullable'],
            'Iva' => ['nullable', 'numeric'],
            'IdCatCategoria' => ['required', 'numeric', 'min:1'],
            'IdCatPaquete' => ['nullable', 'numeric'],
            'Unidad' => ['required', 'string'],
            'CantPesoProm' => ['required'],
            'IdCatArticuloImagenes' => [],
            'DescripcionGeneral' => ['required', 'min:10', 'max:255'],
            'Status' => [],
        ];
    }

    public function messages()
    {
        return [
            'Codigo.required' => 'El codigo es obligatorio',
            'Codigo.strign' => 'El codigo debe ser cadena',
            'Codigo.between' => 'El largo del codigo es entre 5 y 7 caracteres',
            'Descripcion.required' => 'El nombre es obligatorio',
            'Descripcion.string' => 'El nombre debe ser una cadena',
            'DesCorta.required' => 'El la descripcion corta es requerida',
            'IdCatCategoria.required' => 'La categoria es obligatoria',
            'IdCatCategoria.min' => 'Selecciona una categoria',
            'Unidad.required' => 'La unidad de medida es requerida',
            'CantPesoProm.required' => 'El peso promedio es requerido',
            'IdCatArticuloImagenes.required' => 'La imagen es requerida',
            'DescripcionGeneral.required' => 'La descripcion general requerida',
            'DescripcionGeneral.min' => 'Minimo 10 caracteres para la descripcion general',
            'DescripcionGeneral.max' => 'Maximo 256 caracteres para la descripcion general',
        ];
    }
}
