<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CiudadesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Descripcion' => ['required', 'max:50'],
            'IdCatListaPrecio' => ['required', 'numeric','min:1'],
            // 'Hora_entrada' => [],
            // 'Hora_salida' => [],
            'Status' => [],
        ];
    }

    public function messages()
    {
        return [
            'Descripcion.required' => 'La descripcion es obligatorio',
            'Descripcion.max' => 'El maximo de caracteres en el usuario es 50',
            'IdCatListaPrecio.required' => 'La lista de precio es necesaria',
            'IdCatListaPrecio.min' => 'Selecciona una lista de precios'
        ];
    }
}
