<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'username' => ['required', 'max:50'],
            'username' => ['required', 'max:50', Rule::unique('CatUsers')->ignore($this->IdCatUsers, 'IdCatUsers')],
            'password' => ['required', 'string', 'min:8', Password::min(8)
                ->mixedCase()
                ->letters()
                ->numbers()
                ->symbols()
                ->uncompromised(), 'confirmed'],
            'IdCatTipoUser' => ['required', 'numeric', 'min:1'],
            'IdDatCentroVenta' => ['numeric', 'min:0'],
            'Status' => [],
        ];
    }

    public function messages()
    {
        return [
            'username.required' => 'El nombre de usuario es obligatorio',
            'username.max' => 'El maximo de caracteres en el usuario es 50',
            'username.unique' => 'Usuario existente',
            'password.required' => 'La contraseña es obligatoria',
            'password.confirmed' => 'Las contraseñas no coinciden',
            'password.min' => 'La contraseña tiene que tener minimo 6 caracteres',
            'IdCatTipoUser.required' => 'El tipo de usuario es requerido',
            'IdCatTipoUser.min' => 'El tipo de usuario es requerido',
        ];
    }
}
