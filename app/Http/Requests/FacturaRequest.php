<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FacturaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ID_CLIENTE' => ['required'],
            'NOMBRE' => ['required'],
            'TIPO_CLIENTE' => ['required'],
            'Bill_To' => ['required'],
            'Ship_To' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'ID_CLIENTE.required' => 'El id del cliente es necesario',
            'NOMBRE.required' => 'El nombre del cliente es necesario',
            'TIPO_CLIENTE.required' => 'El tipo del cliente es necesario',
            'Bill_To.required' => 'El bill to del cliente es necesario',
            'Ship_To.required' => 'El ship to del cliente es necesario',
        ];
    }
}
