<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteNuevoFacturacionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'username' => ['required', 'max:50'],
            'ID_CLIENTE' => [],
            'Bill_To' => [],
            'Ship_To' => [],

            'TIPOPERSONA' => ['required'],
            'RFC' => ['required'],
            'NOMBRE' => ['required'],
            'MAIL' => [],
            'COLONIA' => ['required'],
            'CALLE' => ['required'],
            'NUM_EXT' => [],
            'NUM_INT' => [],
            'CP' => ['required'],
            'CIUDAD' => ['required'],
            'MUNICIPIO' => ['required'],
            'ESTADO' => ['required'],
            'PAIS' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'ID_CLIENTE.required' => 'El id del cliente es necesario',
            'Bill_To.required' => 'El bill to del cliente es necesario',
            'Ship_To.required' => 'El ship to del cliente es necesario',

            'TIPOPERSONA.required' => 'El tipo de persona es obligatorio',
            'RFC.required' => 'El RFC es obligatorio',
            'NOMBRE.required' => 'El nombre de cliente es obligatorio',
            'COLONIA.required' => 'La colonia es obligatoria',
            'CALLE.required' => 'La calle de cliente es obligatora',
            'CP.required' => 'El codigo postal es obligatoro',
            'CIUDAD.required' => 'La ciudad es obligatoria',
            'MUNICIPIO.required' => 'El municipio es obligatorio',
            'ESTADO.required' => 'El estado es obligatorio',
            'PAIS.required' => 'El pais es obligatorio',
        ];
    }
}
