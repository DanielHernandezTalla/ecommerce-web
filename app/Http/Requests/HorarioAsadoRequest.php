<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HorarioAsadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'DescServicio' => ['required', 'max:50'],
            'Hora_Inicio' => [],
            'Hora_Final' => [],
            'Status' => [],
        ];
    }

    public function messages()
    {
        return [
            'DescServicio.required' => 'La descripcion es obligatorio',
            'DescServicio.max' => 'El maximo de caracteres en el usuario es 50',
        ];
    }
}
