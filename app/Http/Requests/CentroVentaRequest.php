<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CentroVentaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'username' => ['required', 'max:50'],
            'Descripcion' => ['required', 'max:50'],
            'IdCatCiudad' => ['required', 'numeric','min:1'],
            'Almacen_Oracle' => [],
            'ORDER_TYPE_CLOUD' => [],
            'CostoEnvio' => ['nullable', 'numeric'],
            'Status' => [],
        ];
    }

    public function messages()
    {
        return [
            'Descripcion.required' => 'El nombre del centro de venta es obligatorio',
            'Descripcion.max' => 'El maximo de caracteres es 50',
            'Descripcion.unique' => 'Centro de venta existente',
            'IdCatCiudad.required' => 'La ciudad es obligatoria',
            'IdCatCiudad.min' => 'Selecciona una ciudad',
            'CostoEnvio.numeric' => 'El costo envio tiene que ser positivo'
        ];
    }
}
