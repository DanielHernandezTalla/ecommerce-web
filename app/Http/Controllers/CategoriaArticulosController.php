<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoriasArticulosRequest;
use App\Models\CatCategorias;
use App\Models\CatTipoUser;

class CategoriaArticulosController extends Controller
{
    public function index()
    {
        $categorias = CatCategorias::
            paginate(10);

        // return $roles;

        return view('categoriaarticulos.index', compact('categorias'))->with('title', 'Catalogo categorías');
    }

    public function create()
    {
        return view('categoriaarticulos.create')->with('title', 'Registrar categorías');
    }

    public function edit($id)
    {
        $categoria = CatCategorias::where('IdCatCategoria', $id)->first();

        return view('categoriaarticulos.edit', compact('categoria'))->with('title', 'Editar categoría');
    }

    public function store(CategoriasArticulosRequest $request)
    {
        try {
            $categoria = new CatCategorias();

            $categoria->Status = isset($request->validated()['Status']);
            $categoria->Descripcion = $request->validated()['Descripcion'];

            $categoria->save();

            return redirect()
                ->route('categoriaarticulos.index')
                ->withSuccess("Categoria creada correctamente: {$categoria->Description}");
        } catch (\Exception $error) {
            return redirect()
                ->route('categoriaarticulos.index')
                ->withErrors("Error al agregar la categoria");
        }
    }

    public function update($id, CategoriasArticulosRequest $request)
    {
        try {
            CatCategorias::where('IdCatCategoria', $id)
                ->update([
                    'Descripcion' => $request->validated()['Descripcion'],
                    'Status' => isset($request->validated()['Status']),
                ]);

            return redirect()
                ->route('categoriaarticulos.index')
                ->withSuccess("Rol actualizado correctamente");
        } catch (\Exception $error) {
            return redirect()
                ->back()
                ->withInput($request->all())
                ->withErrors($error->getMessage());
        }
    }

    public function destroy($id)
    {
        $categoria = CatCategorias::where('IdCatCategoria', $id)->first();

        if (!$categoria) {
            return redirect()
                ->route('categoriaarticulos.index')
                ->withErrors("Error al eliminar la categoria");
        }

        $categoria->delete();

        return redirect()
            ->route('categoriaarticulos.index')
            ->withSuccess("Categoria eliminada correctamente");
    }
}
