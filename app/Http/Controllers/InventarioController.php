<?php

namespace App\Http\Controllers;

use App\Models\DatCentroVenta;
use App\Models\DatMovInvConcen;
use App\Models\DatMovInvDetalle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InventarioController extends Controller
{
    public function index(Request $request)
    {
        $idCentroVenta = $request->idCentroVenta; // Id por parametro
        $IdDatCentroVenta = Auth::user()->CentroVenta; // Id desde el usuario

        // Si el usuario no pertenece a un centro de venta (es autitor)
        // Se valida que si no trae un centro de venta, se pone por default 0
        if (!$idCentroVenta) {
            $idCentroVenta = 0;
        }

        $productos = DB::table('CatArticulos as a')
            ->select('a.Codigo', 'a.Descripcion', 'a.Unidad', 'b.Stock', 'a.CantPesoProm', 'c.Activo')
            ->leftJoin('DatMovInvConcen as b', 'a.Codigo', 'b.Codigo')
            ->leftJoin('DatArticulos as c', 'a.IdCatArticulos', 'c.IdCatArticulos')
            ->where('b.IdDatCentroVenta', $IdDatCentroVenta->IdDatCentroVenta ?? $idCentroVenta)
            ->where('c.IdDatCentroVenta', $IdDatCentroVenta->IdDatCentroVenta ?? $idCentroVenta)
            ->where('c.Activo', 1)
            ->orderBy('a.Descripcion')
            ->get();

        $conInventario = 0;
        $sinInventario = 0;

        foreach ($productos as $product) {
            if ($product->Stock > 0) {
                $conInventario++;
            } else {
                $sinInventario++;
            }
        }

        $centrosVenta = null;
        if (!Auth::user()->CentroVenta) {
            $centrosVenta = DatCentroventa::where('Status', 1)
                ->get();
        }

        return view('inventario.index', compact('productos', 'centrosVenta', 'idCentroVenta', 'conInventario', 'sinInventario'))->with('title', 'Inventario');
    }

    function print(Request $request)
    {
        $idCentroVenta = $request->idCentroVenta; // Id por parametro
        $IdDatCentroVenta = Auth::user()->CentroVenta; // Id desde el usuario

        // Si el usuario no pertenece a un centro de venta (es autitor)
        // Se valida que si no trae un centro de venta, se pone por default 0
        if (!$idCentroVenta) {
            $idCentroVenta = 0;
        }

        $productos = DB::table('CatArticulos as a')
            ->select('a.Codigo', 'a.Descripcion', 'a.Unidad', 'b.Stock', 'a.CantPesoProm', 'c.Activo')
            ->leftJoin('DatMovInvConcen as b', 'a.Codigo', 'b.Codigo')
            ->leftJoin('DatArticulos as c', 'a.IdCatArticulos', 'c.IdCatArticulos')
            ->where('b.IdDatCentroVenta', $IdDatCentroVenta->IdDatCentroVenta ?? $idCentroVenta)
            ->where('c.IdDatCentroVenta', $IdDatCentroVenta->IdDatCentroVenta ?? $idCentroVenta)
            ->where('c.Activo', 1)
            ->orderBy('a.Descripcion')
            ->get();

        $conInventario = 0;
        $sinInventario = 0;

        foreach ($productos as $product) {
            if ($product->Stock > 0) {
                $conInventario++;
            } else {
                $sinInventario++;
            }
        }

        $centroVenta = DatCentroventa::where('Status', 1)
            ->where('DatCentroventa.idDatCentroventa', $IdDatCentroVenta->IdDatCentroVenta ?? $idCentroVenta)
            ->first();

        $nameReport = 'Inventario ' . $centroVenta->Descripcion . ucfirst(\Carbon\Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) . '.pdf';
        $title = 'Inventario ' . $centroVenta->Descripcion;

        $pdf = \PDF::loadView('inventario.print', compact('productos', 'centroVenta', 'conInventario', 'sinInventario', 'title'));
        return $pdf->download($nameReport);

        return view('inventario.print', compact('productos', 'centroVenta', 'conInventario', 'sinInventario'))->with('title', 'Inventario ' . $centroVenta->Descripcion);
    }

    public function edit(Request $request)
    {
        $idCentroVenta = $request->idCentroVenta; // Id por parametro
        $IdDatCentroVenta = Auth::user()->CentroVenta; // Id desde el usuario

        $idCentroVenta = $request->idCentroVenta; // Id por parametro

        // Si el usuario no pertenece a un centro de venta (es autitor)
        // Se valida que si no trae un centro de venta, se pone por default 0
        if (!$idCentroVenta) {
            $idCentroVenta = 0;
        }

        $productos = DB::table('CatArticulos as a')
            ->leftJoin('DatMovInvConcen as b', 'a.Codigo', 'b.Codigo')
            ->where('b.IdDatCentroVenta', $IdDatCentroVenta->IdDatCentroVenta ?? $idCentroVenta)
            ->orderBy('a.Descripcion')
            ->get();

        $centrosVenta = DatCentroventa::where('Status', 1)
            ->get();

        if (!$idCentroVenta) {
            $idCentroVenta = $IdDatCentroVenta;
        }

        // return $productos;
        return view('inventario.edit', compact('productos', 'centrosVenta', 'idCentroVenta'))->with('title', 'Ajuste de inventario');
    }

    public function update($IdDatCentroVenta, Request $request)
    {
        $stock = $request->stock;


        $stocksActual = DatMovInvConcen::where('IdDatCentroVenta', $IdDatCentroVenta)
            ->get();

        $idCiudad = DatCentroVenta::where('IdDatCentroVenta', $IdDatCentroVenta)
            ->value('IdCatCiudad');

        try {
            DB::beginTransaction();
            $contReg = 0;
            foreach ($stock as $codigo => $ajusteStock) {
                foreach ($stocksActual as $key => $stockActual) {
                    if ($codigo == $stockActual->Codigo && $ajusteStock != $stockActual->Stock) {
                        DatMovInvConcen::where('IdDatCentroVenta', $IdDatCentroVenta)
                            ->where('Codigo', '' . $codigo . '')
                            ->update([
                                'Stock' => $ajusteStock,
                            ]);

                        $referencia = DatMovInvConcen::where('Codigo', '' . $codigo . '')
                            ->where('IdDatCentroVenta', $IdDatCentroVenta)
                            ->max('IdDatInvConcen');

                        $ajuste = $ajusteStock - $stockActual->Stock;

                        DatMovInvDetalle::insert([
                            'IdCiudad' => $idCiudad,
                            'IdDatCentroVenta' => $IdDatCentroVenta,
                            'Codigo' => '' . $codigo . '',
                            'Cantidad' => $ajuste,
                            'FechaMovimiento' => date('d-m-Y H:i:s'),
                            'Referencia' => $referencia,
                            'IdMovimientoinventario' => 5,
                            'IdCatUser' => Auth::user()->IdCatUser,
                        ]);

                        $contReg = $contReg + 1;
                    }
                }
            }

            if ($contReg == 0) {
                return back()->with('msjdelete', 'No ha realizado ajustes de inventario');
            }
        } catch (\Throwable $th) {
            DB::rollback();
            return back()->with('msjdelete', 'Error: ' . $th->getMessage());
        }

        DB::commit();
        return back()->with('msjAdd', 'Ajuste de inventario realizado con éxito');
    }
}
