<?php

namespace App\Http\Controllers;

use App\Models\CatListaPrecio;
use App\Models\Ciudad;
use App\Models\DatCentroVenta;
use App\Models\Precio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PreciosController extends Controller
{
    public function Precios(Request $request)
    {
        $listaprecios = CatListaPrecio::where('Status', 1)
            ->get();

        $ciudades = Ciudad::where('Status', 1)
            ->get();

        $IdDatCentroVenta = $request->IdDatCentroVenta ? $request->IdDatCentroVenta : 0;

        $precios = DB::table('CatArticulos as a')
            ->leftJoin('DatPrecios as c', 'c.IdCatArticulos', 'a.IdCatArticulos')
            ->where('c.IdCatListaPrecio', $IdDatCentroVenta)
            ->get();

        return view('precios.index', compact('listaprecios', 'IdDatCentroVenta', 'ciudades', 'precios'))->with('title', 'Precios');
    }

    public function EditarPrecio(Request $request, $idDatPrecio)
    {
        try {

            $precio = $request->precio;

            Precio::where('IdDatPrecios', $idDatPrecio)
                ->update([
                    'Precio' => $precio,
                ]);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }

        return back();
    }

    public function EditarNultiplePrecio(Request $request)
    {
        $codigos = $request->chkPrecio;

        if (empty($codigos)) {
            return back()->with('msjdelete', 'Debe seleccionar al menos un check');
        }

        $IdCatListaPrecio = $request->IdDatCentroVenta;

        $precios = DB::table('CatArticulos as a')
            ->leftJoin('DatPrecios as c', 'c.IdCatArticulos', 'a.IdCatArticulos')
            ->where('c.IdCatListaPrecio', $IdCatListaPrecio)
            ->whereIn('a.Codigo', $codigos)
            ->get();

        // return $precios;

        return view('precios.editarMultiplePrecio', compact('precios', 'IdCatListaPrecio'))->with('title', 'Actualizar precios');
    }

    public function GuardarMultiplePrecios(Request $request)
    {
        $IdDatCentroVenta = $request->IdCatListaPrecio;
        $preciosNuevos = $request->precioNuevo;

        try {
            foreach ($preciosNuevos as $idDatPrecio => $precio) {
                Precio::where('IdDatPrecios', $idDatPrecio)
                    ->update([
                        'Precio' => $precio,
                    ]);
            }
        } catch (\Throwable $th) {
            return $th->getMessage();
        }

        return redirect()->route('precios.index', compact("IdDatCentroVenta"))->with('msjAdd', 'Precios actualizados');
    }
}
