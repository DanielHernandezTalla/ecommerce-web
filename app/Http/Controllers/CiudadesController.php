<?php

namespace App\Http\Controllers;

use App\Http\Requests\CiudadesRequest;
use App\Models\CatDiasDisponibles;
use App\Models\CatListaPrecio;
use App\Models\Ciudad;
use Illuminate\Http\Request;

class CiudadesController extends Controller
{
    public function index(Request $request)
    {
        $ciudades = Ciudad::select(
            'CatCiudades.IdCatCiudades',
            'li.Descripcion as lista',
            'CatCiudades.Descripcion as ciudad',
            'CatCiudades.IdCatListaPrecio',
            'CatCiudades.Organization_id',
            'CatCiudades.Hora_entrada',
            'CatCiudades.Hora_salida',
            'CatCiudades.Status'
        )
            ->leftJoin('CatListaPrecio as li', 'li.IdCatListaPrecio', 'CatCiudades.IdCatListaPrecio')
            ->paginate(10);

        // return $ciudades;
        return view('ciudades.index', compact('ciudades'))->with('title', 'Catálogo de ciudades');
    }

    public function edit($id)
    {
        $semana = [1 => 'Lunes', 2 => 'Martes', 3 => 'Miercoles', 4 => 'Jueves', 5 => 'Viernes', 6 => 'Sabado', 7 => 'Domingo'];
        $ciudad = Ciudad::where('IdCatCiudades', $id)->first();
        $listaPrecios = CatListaPrecio::where('Status', 1)->get();
        $diasDisponibles = CatDiasDisponibles::where('IdCatCiudad', $id)
            ->get();

        return view('ciudades.edit', compact('ciudad', 'listaPrecios', 'semana', 'diasDisponibles'))->with('title', 'Editar ciudad');
    }

    public function create()
    {
        $listaPrecios = CatListaPrecio::where('Status', 1)->get();

        return view('ciudades.create', compact('listaPrecios'))->with('title', 'Registrar ciudades');
    }

    public function store(CiudadesRequest $request)
    {
        try {
            $ciudad = new Ciudad();

            $ciudad->Status = isset($request->validated()['Status']);
            $ciudad->Descripcion = $request->validated()['Descripcion'];
            $ciudad->IdCatListaPrecio = $request->validated()['IdCatListaPrecio'];
            $ciudad->Hora_entrada = $request->validated()['Hora_entrada'];
            $ciudad->Hora_salida = $request->validated()['Hora_salida'];

            $ciudad->save();

            return redirect()
                ->route('ciudades.index')
                ->withSuccess("Ciudad creada correctamente: {$ciudad->Description}");
        } catch (\Exception $error) {
            return redirect()
                ->route('ciudades.index')
                ->withErrors("Error al agregar la ciudad");
        }
    }

    public function update($id, CiudadesRequest $request)
    {
        try {
            Ciudad::where('IdCatCiudades', $id)
                ->update([
                    'Descripcion' => $request->validated()['Descripcion'],
                    'IdCatListaPrecio' => $request->validated()['IdCatListaPrecio'],
                    // 'Hora_entrada' => $request->validated()['Hora_entrada'],
                    // 'Hora_salida' => $request->validated()['Hora_salida'],
                    'Status' => $request->validated()['Status'],
                ]);
            return redirect()
                ->route('ciudades.index')
                ->withSuccess("Ciudad actualizada correctamente");
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function horario($id, Request $request)
    {
        try {
            $diasDisponibles = new CatDiasDisponibles();

            $diasDisponibles->IdCatCiudad = $id;
            $diasDisponibles->DiaInicial = $request->diaInicio;
            $diasDisponibles->DiaFinal = $request->diaCierre;
            $diasDisponibles->HoraEntrada = $request->Hora_inicio;
            $diasDisponibles->HoraSalida = $request->Hora_salida;

            $diasDisponibles->save();

            return redirect()
                ->back()
                ->withSuccess("Día agregado correctamente");
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function horarioDestroy($id, Request $request)
    {
        $diasDisponibles = CatDiasDisponibles::where('IdCatDiaDisponible', $id)->first();

        if (!$diasDisponibles) {
            return redirect()
                ->back()
                ->withErrors("Error al eliminar el día");
        }

        $diasDisponibles->delete();

        return redirect()
            ->back()
            ->withSuccess("Día eliminado correctamente");
    }

    public function destroy($ciudad)
    {
        $ciudad = Ciudad::where('IdCatCiudades', $ciudad)->first();

        if (!$ciudad) {
            return redirect()
                ->route('ciudades.index')
                ->withErrors("Error al eliminar la ciudad");
        }

        $ciudad->delete();

        return redirect()
            ->route('ciudades.index')
            ->withSuccess("Ciudad eliminda correctamente");
    }
}
