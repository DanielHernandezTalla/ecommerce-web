<?php

namespace App\Http\Controllers;

use App\Http\Requests\CiudadesRequest;
use App\Models\CatImagenPromocion;
use App\Models\CatListaPrecio;
use App\Models\Ciudad;
use Illuminate\Http\Request;

class ImagenesPromocionesController extends Controller
{
    public function index(Request $request)
    {
        $imagenes = CatImagenPromocion::get();

        // return $ciudades;
        return view('imagenespromociones.index', compact('imagenes'))->with('title', 'Catálogo de imagenes para promociones');
    }

    public function edit($id)
    {
        $imagen = CatImagenPromocion::where('id', $id)->first();

        return view('imagenespromociones.edit', compact('imagen'))->with('title', 'Editar imagenes para las promociones');
    }

    public function create()
    {
        return view('imagenespromociones.create')->with('title', 'Registrar imagen para promoción');
    }

    public function store(Request $request)
    {
        try {
            // Obtenemos la imagen que mando el usuario
            $image = $request->file('imagen');

            // Si el usuario envio imagen, esta sera movida al directorio del proyecto y guardada en la base de datos
            if ($image !== null) {
                $nombreArchivo = $image->getClientOriginalName();
                $image->move('images/slider', $nombreArchivo);

                $imagen = new CatImagenPromocion();
                $imagen->imagen = 'https://ecommerce.kowi.com.mx/images/slider/' . $nombreArchivo;
                $imagen->save();

                return redirect()
                    ->route('imagenespromocioens.index')
                    ->withSuccess("Imagen agregada correctamente");
            }

            return redirect()
                ->route('imagenespromocioens.index')
                ->withSuccess("No ingresaste una imagen");
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function update($id, Request $request)
    {
        try {
            // Obtenemos la imagen que mando el usuario
            $image = $request->file('imagen');

            // Si el usuario envio imagen, esta sera movida al directorio del proyecto y guardada en la base de datos
            if ($image !== null) {
                $nombreArchivo = $image->getClientOriginalName();
                $image->move('images/slider', $nombreArchivo);

                CatImagenPromocion::where('id', $id)
                    ->update([
                        'imagen' => 'https://ecommerce.kowi.com.mx/images/slider/' . $nombreArchivo,
                    ]);
                return redirect()
                    ->route('imagenespromocioens.index')
                    ->withSuccess("Imagen actualizada correctamente");
            }

            return redirect()
                ->route('imagenespromocioens.index')
                ->withSuccess("No ingresaste una imagen");
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function destroy($imagen)
    {
        $imagen = CatImagenPromocion::where('id', $imagen)->first();
        $aux = explode('/', $imagen->imagen);
        unlink('images/slider/' . end($aux));

        if (!$imagen) {
            return redirect()
                ->route('imagenespromocioens.index')
                ->withErrors("Error al eliminar la imagen");
        }

        $imagen->delete();

        return redirect()
            ->route('imagenespromocioens.index')
            ->withSuccess("Imagen eliminda correctamente");
    }
}
