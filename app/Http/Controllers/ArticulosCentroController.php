<?php

namespace App\Http\Controllers;

use App\Models\CatArticulos;
use App\Models\DatArticulos;
use App\Models\DatCentroVenta;
use App\Models\DatMovInvConcen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticulosCentroController extends Controller
{
    public function index(Request $request)
    {
        $articulos = CatArticulos::where('Status', 1)
            ->get();

        $centroVenta = DatCentroVenta::where('Status', 1)
            ->get();

        $IdDatCentroVenta = $request->IdDatCentroVenta;

        $articulosCentro = DB::table('DatArticulos as a')
            ->leftJoin('CatArticulos as b', 'b.IdCatArticulos', 'a.IdCatArticulos')
            ->where('a.IdDatCentroVenta', $IdDatCentroVenta)
            ->get();

        // return $articulosCentro;

        return view('articuloscentro.index', compact('centroVenta', 'IdDatCentroVenta', 'articulosCentro', 'articulos'))->with('title', 'Artículos por centro de venta');
    }

    public function addArticle(Request $request)
    {
        try {
            if (!$request->IdDatCentroVenta) {
                return back()->with('msjdelete', 'Selecciona un centro de venta');
            }

            $codigo = $request->Codigo;
            $IdDatCentroVenta = $request->IdDatCentroVenta;

            $articulo = CatArticulos::where('Codigo', $codigo)
                ->first();

            $centroVenta = DatCentroVenta::where('IdDatCentroVenta', $IdDatCentroVenta)->first();

            $res = DatArticulos::where([['IdDatCentroVenta', $IdDatCentroVenta], ['idCatArticulos', $articulo->IdCatArticulos]])->first();

            if($res){
                return back()->with('msjdelete', 'El producto ya existe en la lista');
            }

            DatArticulos::insert([
                'idCatArticulos' => $articulo->IdCatArticulos,
                'IdCatCiudades' => $centroVenta->IdCatCiudad,
                'IdDatCentroVenta' => $IdDatCentroVenta,
                'Activo' => 1,
                'IdCatUser' => auth()->user()->IdCatUser,
            ]);

            DatMovInvConcen::insert([
                'IdCatCiudades' => $centroVenta->IdCatCiudad,
                'IdDatCentroVenta' => $IdDatCentroVenta,
                'Codigo' => $articulo->Codigo,
                'Stock' => 0,
            ]);

        } catch (\Throwable $th) {
            return back()->with('msjdelete', 'Error: ' . $th->getMessage());
        }

        return back();
    }

    public function update(Request $request)
    {
        $codigos = [];
        if ($request->Activo) {
            $codigos = $request->Activo;
        }

        $IdDatCentroVenta = $request->IdDatCentroVenta;

        try {
            $articulosCentro = DB::table('DatArticulos as a')
                ->leftJoin('CatArticulos as b', 'b.IdCatArticulos', 'a.IdCatArticulos')
                ->where('a.IdDatCentroVenta', $IdDatCentroVenta)
                ->get();

            foreach ($articulosCentro as $articulo) {
                DatArticulos::where('IdDatArticulos', $articulo->IdDatArticulos)
                    ->update([
                        'Activo' => in_array($articulo->Codigo, $codigos),
                    ]);
            }
        } catch (\Throwable $th) {
            return $th->getMessage();
        }

        return back()->with('msjAdd', 'Estatus de articulos actualizado');
    }

    public function destroy($id)
    {
        try {

            $inv = DatMovInvConcen::
                select('IdDatInvConcen', 'Stock')
                ->leftjoin('CatArticulos', 'CatArticulos.Codigo', 'DatMovInvConcen.Codigo')
                ->leftjoin('DatArticulos', 'CatArticulos.IdCatArticulos', 'DatArticulos.IdCatArticulos')
                ->whereColumn('DatArticulos.IdDatCentroVenta', 'DatMovInvConcen.IdDatCentroVenta')
                ->where('DatArticulos.IdDatArticulos', $id)
                ->first();

            if (floatval($inv->Stock) == '0') {
                $inv->delete();
            } else {
                return back()->with('msjdelete', 'El producto contiene stock, no se puede eliminar.');
            }

            $articulo = DatArticulos::find($id);

            $articulo->delete();

            return back()->with('msjAdd', 'Producto eliminado correctamente');

        } catch (\Throwable $th) {
            return back()->with('msjdelete', 'Error: ' . $th->getMessage());
        }
    }
}
