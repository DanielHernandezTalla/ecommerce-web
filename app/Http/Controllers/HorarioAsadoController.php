<?php

namespace App\Http\Controllers;

use App\Http\Requests\HorarioAsadoRequest;
use App\Models\CatServicios;
use Illuminate\Http\Request;

class HorarioAsadoController extends Controller
{
    public function index(Request $request)
    {
        $servicios = CatServicios::paginate(10);

        return view('horarioasado.index', compact('servicios'))->with('title', 'Catálogo de servicios');
    }

    public function edit($id)
    {
        $servicio = CatServicios::where('IdCatServicios', $id)->first();

        return view('horarioasado.edit', compact('servicio'))->with('title', 'Editar servicio');
    }

    public function create()
    {
        return view('horarioasado.create')->with('title', 'Registrar servicio');
    }

    public function store(HorarioAsadoRequest $request)
    {
        try {
            $servicio = new CatServicios();

            $servicio->DescServicio = $request->validated()['DescServicio'];
            $servicio->Hora_Inicio = $request->validated()['Hora_Inicio'];
            $servicio->Hora_Final = $request->validated()['Hora_Final'];
            $servicio->Status = isset($request->validated()['Status']);

            $servicio->save();

            return redirect()
                ->route('horarioasado.index')
                ->withSuccess("Servicio creado correctamente: {$servicio->DescServicio}");
        } catch (\Exception $error) {
            return redirect()
                ->route('horarioasado.index')
                ->withErrors("Error al agregar el servicio");
        }
    }

    public function update($id, HorarioAsadoRequest $request)
    {
        try {
            CatServicios::where('IdCatServicios', $id)
                ->update([
                    'DescServicio' => $request->validated()['DescServicio'],
                    'Hora_Inicio' => $request->validated()['Hora_Inicio'],
                    'Hora_Final' => $request->validated()['Hora_Final'],
                    'Status' => isset($request->validated()['Status']),
                ]);

            return redirect()
                ->route('horarioasado.index')
                ->withSuccess("Servicio actualizado correctamente");
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function destroy($id)
    {
        $servicio = CatServicios::where('IdCatServicios', $id)->first();

        if (!$servicio) {
            return redirect()
                ->route('horarioasado.index')
                ->withErrors("Error al eliminar el servicio");
        }

        $servicio->delete();

        return redirect()
            ->route('horarioasado.index')
            ->withSuccess("Servicio elimindo correctamente");
    }
}
