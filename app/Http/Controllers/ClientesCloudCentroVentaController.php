<?php

namespace App\Http\Controllers;

use App\Http\Requests\CiudadesRequest;
use App\Http\Requests\ClientesCloudCentroVentaRequest;
use App\Models\CatClientesCloudCentroVenta;
use App\Models\CatListaPrecio;
use App\Models\CatMetododePago;
use App\Models\CatTipoPago;
use App\Models\CatUsoCFDI;
use App\Models\Ciudad;
use App\Models\DatCentroVenta;
use App\Models\DatClientesCloudCentroVenta;
use Illuminate\Http\Request;

class ClientesCloudCentroVentaController extends Controller
{
    public function index(Request $request)
    {
        $clientes = DatClientesCloudCentroVenta::
            leftJoin('DatCentroVenta as cv', 'cv.IdDatCentroVenta', 'DatClientesCloudCentroVenta.IdDatCentroVenta')
            ->leftJoin('CatTipoPago as tp', 'tp.IdTipoPago', 'DatClientesCloudCentroVenta.IdCatTipopago')
            ->leftJoin('CatMetododePago as mp', 'mp.IdCatMetododePago', 'DatClientesCloudCentroVenta.IdMetodoPago')
            ->leftJoin('CatUsoCFDI as CFDI', 'CFDI.IdCatUsoCFDI', 'DatClientesCloudCentroVenta.IdUsoCfdi')
            ->paginate(10);

        // return $clientes;
        return view('clientescloud.index', compact('clientes'))->with('title', 'Catálogo de clientes');
    }

    public function create()
    {
        $clientes = CatClientesCloudCentroVenta::get();
        $tipoPago = CatTipoPago::get();
        $metodoPago = CatMetododePago::get();
        $usocfdi = CatUsoCFDI::get();
        $centroVenta = DatCentroVenta::where('Status', 1)->get();

        return view('clientescloud.create', compact('clientes', 'centroVenta', 'tipoPago', 'metodoPago', 'usocfdi'))->with('title', 'Registrar cliente');
    }

    public function store(ClientesCloudCentroVentaRequest $request)
    {
        try {

            $cliente = CatClientesCloudCentroVenta::where('IDCATCLIENTESCLOUD', $request->validated()['IDCATCLIENTESCLOUD'])->first();

            DatClientesCloudCentroVenta::insert([
                'IdDatCentroVenta' => $request->validated()['IdDatCentroVenta'],
                'ID_CLIENTE' => $cliente->ID_CLIENTE,
                'NOMBRE' => $cliente->NOMBRE,
                'NUMERO_CLIENTE' => $cliente->NUMERO_CLIENTE,
                'TIPO_CLIENTE' => $cliente->TIPO_CLIENTE,
                'SHIP_TO' => $cliente->SHIP_TO,
                'BILL_TO' => $cliente->BILL_TO,
                'CODIGO_ENVIO' => $cliente->CODIGO_ENVIO,
                'DIRECCION' => $cliente->DIRECCION,
                'LOCACION' => $cliente->LOCACION,
                'PAIS' => $cliente->PAIS,
                'CIUDAD' => $cliente->CIUDAD,
                'CODIGO_POSTAL' => $cliente->CODIGO_POSTAL,
                'IdCatTipopago' => $request->validated()['IdTipoPago'],
                'IdMetodoPago' => $request->validated()['IdCatMetododePago'],
                'IdUsoCfdi' => $request->validated()['IdCatUsoCFDI'],
            ]);

            return redirect()
                ->route('clientescloud.index')
                ->withSuccess("Cliente creado correctamente.");
        } catch (\Exception $error) {
            return $error;
            return redirect()
                ->route('clientescloud.index')
                ->withErrors("Error al agregar la un nuevo cliente");
        }
    }
}
