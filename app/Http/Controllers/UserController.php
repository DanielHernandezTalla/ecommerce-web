<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\CatTipoUser;
use App\Models\DatCentroVenta;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $users = User::
            select('IdCatUser', 'username', 'DatCentroVenta.Descripcion as centroVenta', 'CatTipoUser.Descripcion as tipoUser', 'CatUsers.status as status')
            ->leftJoin('DatCentroVenta', 'DatCentroVenta.IdDatCentroVenta', 'CatUsers.IdDatCentroVenta')
            ->leftJoin('CatTipoUser', 'CatTipoUser.IdCatTipoUser', 'CatUsers.IdCatTipoUser')
            ->where('CatUsers.IdCatTipoUser', '>', 1)
            ->orderBy('centroVenta')
            ->orderBy('tipoUser')
            ->paginate(10);
            // return $users;


        return view('users.index', compact('users'))->with('title', 'Catálogo usuarios');
    }

    public function create()
    {
        $typeUsers = CatTipoUser::all();
        $centroVenta = DatCentroVenta::all();

        return view('users.create', compact('typeUsers', 'centroVenta'))->with('title', 'Registrar usuario');
    }

    public function edit($user)
    {
        $user = User::find($user);
        $typeUsers = CatTipoUser::all();
        $centroVenta = DatCentroVenta::all();

        return view('users.edit', compact('typeUsers', 'centroVenta', 'user'))->with('title', 'Editar usuario');
    }

    public function store(UserRequest $request)
    {
        try {
            $user = new User();

            $user->Status = isset($request->validated()['Status']);
            $user->username = $request->validated()['username'];
            $user->password = bcrypt($request->validated()['password']);
            $user->IdCatTipoUser = $request->validated()['IdCatTipoUser'];
            $user->IdDatCentroVenta = $request->validated()['IdDatCentroVenta'] == 0 ? null : $request->validated()['IdDatCentroVenta'];

            $user->save();

            return redirect()
                ->route('usuarios')
                ->withSuccess("Usuario creado correctamente: {$user->IdCatUser}");
        } catch (\Exception $error) {
            return redirect()
                ->route('usuarios')
                ->withErrors("Error al agregar el usuario");
        }
    }

    public function update(Request $request, $user)
    {
        try {
            // Reglas de validacion
            $rules = ['username' => ['required', 'max:50'],
                'password' => ['nullable', 'string', 'min:6', 'confirmed'],
                'IdCatTipoUser' => ['required', 'numeric', 'min:1'],
                'IdDatCentroVenta' => ['numeric', 'min:0'],
                'Status' => []];

            $messages = [
                'username.required' => 'El nombre de usuario es obligatorio',
                'username.max' => 'El maximo de caracteres en el usuario es 50',
                'username.unique' => 'Usuario existente',
                'password.required' => 'La contraseña es obligatoria',
                'password.confirmed' => 'Las contraseñas no coinciden',
                'password.min' => 'La contraseña tiene que tener minimo 6 caracteres',
                'IdCatTipoUser.required' => 'El tipo de usuario es requerido',
                'IdCatTipoUser.min' => 'El tipo de usuario es requerido',
            ];

            // Validando
            $validator = Validator::make($request->all(), $rules, $messages);

            // Devolviendo los errores
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            // Pasando los nuevos valores al objeto
            $user = User::find($user);
            $user->Status = isset($request->all()['Status']);
            $user->username = $request->all()['username'];
            $user->IdCatTipoUser = $request->all()['IdCatTipoUser'];
            $user->IdDatCentroVenta = $request->all()['IdDatCentroVenta'] == 0 ? null : $request->all()['IdDatCentroVenta'];

            // En caso de que no venga contraseña
            if (isset($request->all()['password'])) {
                $user->password = bcrypt($request->all()['password']);
            }

            // Actualizando el usuario
            $user->update();

            // return $user;

            return redirect()
                ->route('usuarios')
                ->withSuccess("Usuario actualizado correctamente: {$user->IdCatUser}");
        } catch (\Exception $error) {
            return redirect()
                ->back()
                ->withInput($request->all())
                ->withErrors($error->getMessage());
        }
    }

    public function destroy($user)
    {
        $user = User::find($user);

        if (!$user) {
            return redirect()
                ->route('usuarios')
                ->withErrors("Error al eliminar el usuario");
        }

        $user->delete();

        return redirect()
            ->route('usuarios')
            ->withSuccess("Usuario elimindo correctamente");
    }
}
