<?php

namespace App\Http\Controllers;

use App\Models\CatArticulos;
use App\Models\CatListaPrecio;
use App\Models\Ciudad;
use App\Models\DatCentroVenta;
use App\Models\DatMovInvConcen;
use App\Models\Precio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticulosListaController extends Controller
{
    public function index(Request $request)
    {
        $articulos = CatArticulos::where('Status', 1)
            ->get();

        $listaprecios = CatListaPrecio::where('Status', 1)
            ->get();

        $IdCatListaPrecio = $request->IdCatListaPrecio ? $request->IdCatListaPrecio : 0;

        $precios = DB::table('CatArticulos as a')
            ->leftJoin('DatPrecios as c', 'c.IdCatArticulos', 'A.IdCatArticulos')
            ->where('c.IdCatListaPrecio', $IdCatListaPrecio)
            ->get();

        return view('articuloslista.index', compact('listaprecios', 'IdCatListaPrecio', 'precios', 'articulos'))->with('title', 'Artículos por lista');
    }

    public function addArticle(Request $request)
    {
        try {
            $codigo = $request->Codigo;
            $precio = $request->Precio;
            $IdCatListaPrecio = $request->IdCatListaPrecio;

            $articulo = CatArticulos::where('Codigo', $codigo)
                ->first();

            $exists =  Precio::where('idCatArticulos', $articulo->IdCatArticulos)
                ->where('IdCatListaPrecio ', $IdCatListaPrecio,)
                ->get();

            if (count($exists)) {
                return back()->with('msjdelete', 'El articulo ya existe en la lista de precios');
            }

            Precio::insert([
                'IdCatListaPrecio ' => $IdCatListaPrecio,
                'idCatArticulos' => $articulo->IdCatArticulos,
                'Precio' => $precio,
                'Status' => 1,
            ]);

            $ciudades = Ciudad::select('IdCatCiudades')
                ->where('IdCatListaPrecio', $IdCatListaPrecio)
                ->where('Status', 1)
                ->get();

            $ciudadesIds = [];

            foreach ($ciudades as $ciudad) {
                array_push($ciudadesIds, $ciudad->IdCatCiudades);
            }

            $centros = DatCentroVenta::where('Status', 1)->whereIn('IdCatCiudad', $ciudadesIds)->get();

            foreach ($centros as $centro) {
                // return $centro;
                $articuloMov = DatMovInvConcen::where('IdCatCiudades', $centro->IdCatCiudad)
                    ->where('IdDatCentroVenta', $centro->IdDatCentroVenta)
                    ->where('Codigo', $codigo)
                    ->get();

                if (count($articuloMov) == 0)
                    DatMovInvConcen::insert([
                        'IdCatCiudades' => $centro->IdCatCiudad,
                        'IdDatCentroVenta' => $centro->IdDatCentroVenta,
                        'Codigo' => $codigo,
                        'Stock' => 0,
                    ]);
            }
        } catch (\Throwable $th) {
            return back()->with('msjdelete', 'Error: ' . $th->getMessage());
        }

        return back();
    }

    public function destroy($id)
    {
        try {
            $precio = Precio::find($id);

            $precio->delete();

            return back()->with('msjAdd', 'Producto eliminado correctamente');
        } catch (\Throwable $th) {
            return back()->with('msjdelete', 'Error: ' . $th->getMessage());
        }
    }
}
