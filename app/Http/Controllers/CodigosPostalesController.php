<?php

namespace App\Http\Controllers;

use App\Models\CatListaPrecio;
use App\Models\Ciudad;
use App\Models\DatCentroVentaCodigoPostal;
use Illuminate\Http\Request;

class CodigosPostalesController extends Controller
{
    public function index(Request $request)
    {
        $cp = DatCentroVentaCodigoPostal::select('DatCentroVentaCodigoPostal.IdCenVtaCodPos', 'CatCiudades.Descripcion', 'DatCentroVentaCodigoPostal.CodigoPostal')
            ->leftjoin('CatCiudades', 'CatCiudades.IdCatCiudades', 'DatCentroVentaCodigoPostal.IdCatCiudad')
            ->paginate(10);

        return view('codigospostales.index', compact('cp'))->with('title', 'Catálogo de codigos postales');
    }

    public function create()
    {
        $ciudades = Ciudad::where('Status', 1)->get();

        return view('codigospostales.create', compact('ciudades'))->with('title', 'Registrar codigos postales');
    }

    public function edit($id)
    {
        $ciudad = Ciudad::where('IdCatCiudades', $id)->first();

        $listaPrecios = CatListaPrecio::where('Status', 1)->get();

        return view('ciudades.edit', compact('ciudad', 'listaPrecios'))->with('title', 'Editar ciudad');
    }

    public function store(Request $request)
    {
        try {

            $ciudad = DatCentroVentaCodigoPostal::where('CodigoPostal', $request->CodigoPostal)
                ->where('IdCatCiudad', $request->IdCatCiudad)
                ->first();

            if ($ciudad) {
                return redirect()
                    ->back()
                    ->withErrors('Código postal existente para esa ciudad');
            }

            $cp = new DatCentroVentaCodigoPostal();

            $cp->IdCatCiudad = $request->IdCatCiudad;
            $cp->CodigoPostal = $request->CodigoPostal;

            $cp->save();

            return redirect()
                ->route('codigospostales.index')
                ->withSuccess("Codigo postal creado correctamente: {$cp->Description}");
        } catch (\Exception $error) {
            return redirect()
                ->route('codigospostales.index')
                ->withErrors("Error al agregar el codigo postal");
        }
    }

    public function destroy($idCP)
    {
        $ciudad = DatCentroVentaCodigoPostal::where('IdCenVtaCodPos', $idCP)->first();

        if (!$ciudad) {
            return redirect()
                ->route('codigospostales.index')
                ->withErrors("Error al eliminar el código postal");
        }

        $ciudad->delete();

        return redirect()
            ->back()
            ->withSuccess("Código postal eliminado correctamente");
    }
}
