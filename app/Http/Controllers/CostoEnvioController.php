<?php

namespace App\Http\Controllers;

use App\Http\Requests\CentroVentaRequest;
use App\Models\Ciudad;
use App\Models\DatCentroVenta;
use Illuminate\Http\Request;

class CostoEnvioController extends Controller
{
    public function index(Request $request)
    {

        $centroDeVentas = DatCentroVenta::
            select('c.Descripcion as ciudad', 'DatCentroVenta.Descripcion as centroVenta',
            'DatCentroVenta.Almacen_Oracle', 'DatCentroVenta.CostoEnvio', 'DatCentroVenta.ORDER_TYPE_CLOUD',
            'DatCentroVenta.Status', 'DatCentroVenta.IdDatCentroVenta')
            ->leftJoin('CatCiudades as c', 'c.IdCatCiudades', 'DatCentroVenta.IdCatCiudad')
            ->orderby('c.Descripcion')
            ->orderby('DatCentroVenta.Descripcion')
            ->paginate();

        return view('costoenvio.index', compact('centroDeVentas'))->with('title', 'Centros de venta');
    }

    public function edit($id)
    {
        $centroVenta = DatCentroVenta::
            select('c.Descripcion as ciudad', 'DatCentroVenta.Descripcion as centroVenta',
            'DatCentroVenta.Almacen_Oracle', 'DatCentroVenta.CostoEnvio', 'DatCentroVenta.ORDER_TYPE_CLOUD',
            'DatCentroVenta.Status', 'DatCentroVenta.IdDatCentroVenta')
            ->leftJoin('CatCiudades as c', 'c.IdCatCiudades', 'DatCentroVenta.IdCatCiudad')
            ->where('DatCentroVenta.IdDatCentroVenta', $id)
            ->first();

        // return $centroVenta;
        $ciudades = Ciudad::all();

        return view('costoenvio.edit', compact('centroVenta', 'ciudades'))->with('title', 'Editar centro de venta');
    }

    public function create()
    {
        $ciudades = Ciudad::where('Status', 1)->get();

        return view('costoenvio.create', compact('ciudades'))->with('title', 'Registrar centro de venta');
    }

    public function store(CentroVentaRequest $request)
    {
        try {
            $centroVenta = new DatCentroVenta();

            $centroVenta->IdCatCiudad = $request->validated()['IdCatCiudad'];
            $centroVenta->Descripcion = $request->validated()['Descripcion'];
            $centroVenta->Almacen_Oracle = $request->validated()['Almacen_Oracle'];
            $centroVenta->CostoEnvio = $request->validated()['CostoEnvio'];
            $centroVenta->Status = isset($request->validated()['Status']);
            $centroVenta->ORDER_TYPE_CLOUD = $request->validated()['ORDER_TYPE_CLOUD'];

            $centroVenta->save();

            return redirect()
                ->route('costoenvio.index')
                ->withSuccess("Centro de venta creado correctamente: {$centroVenta->Descripcion}");
        } catch (\Exception $error) {
            return $error;
            return redirect()
                ->route('costoenvio.index')
                ->withErrors("Error al agregar el centro de venta");
        }
    }

    public function update($id, CentroVentaRequest $request)
    {
        try {
            DatCentroVenta::where('IdDatCentroVenta', $id)
                ->update([
                    'Descripcion' => $request->validated()['Descripcion'],
                    'IdCatCiudad' => $request->validated()['IdCatCiudad'],
                    'Almacen_Oracle' => $request->validated()['Almacen_Oracle'],
                    'CostoEnvio' => $request->validated()['CostoEnvio'],
                    'Status' => isset($request->validated()['Status']),
                    'ORDER_TYPE_CLOUD' => $request->validated()['ORDER_TYPE_CLOUD'],
                ]);

            return redirect()
                ->route('costoenvio.index')
                ->withSuccess("Centro de venta actualizado correctamente");
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function destroy($id)
    {
        $centroVenta = DatCentroVenta::find($id);

        if (!$centroVenta) {
            return redirect()
                ->route('costoenvio.index')
                ->withErrors("Error al eliminar el centro de venta");
        }

        $centroVenta->delete();

        return redirect()
            ->route('costoenvio.index')
            ->withSuccess("Centro de venta elimindo correctamente");
    }
}
