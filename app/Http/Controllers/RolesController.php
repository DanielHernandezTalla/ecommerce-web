<?php

namespace App\Http\Controllers;

use App\Http\Requests\RolesRequest;
use App\Models\CatTipoUser;
use App\Models\User;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    public function index()
    {
        $roles = CatTipoUser::
            paginate(10);

        // return $roles;

        return view('roles.index', compact('roles'))->with('title', 'Catálogo roles');
    }

    public function create()
    {
        return view('roles.create')->with('title', 'Registrar rol');
    }

    public function edit($rol)
    {
        $rol = CatTipoUser::where('IdCatTipoUser', $rol)->first();
        // return $rol;

        return view('roles.edit', compact('rol'))->with('title', 'Editar rol');
    }

    public function store(RolesRequest $request)
    {
        try {
            $rol = new CatTipoUser();

            $rol->Status = isset($request->validated()['Status']);
            $rol->Descripcion = $request->validated()['Descripcion'];

            $rol->save();

            return redirect()
                ->route('roles')
                ->withSuccess("Rol creado correctamente: {$rol->Description}");
        } catch (\Exception $error) {
            return redirect()
                ->route('roles')
                ->withErrors("Error al agregar el rol");
        }
    }

    public function update(Request $request, $user)
    {
        try {

            // return $user;

            CatTipoUser::where('IdCatTipoUser', $user)
                ->update([
                    'Descripcion' => $request->Descripcion,
                    'Status' => $request->Status == 'on' ? 1 : 0,
                ]);

            return redirect()
                ->route('roles')
                ->withSuccess("Rol actualizado correctamente");
        } catch (\Exception $error) {
            return redirect()
                ->back()
                ->withInput($request->all())
                ->withErrors($error->getMessage());
        }
    }

    public function destroy($rol)
    {
        $rol = CatTipoUser::where('IdCatTipoUser', $rol)->first();

        // return $rol;

        if (!$rol) {
            return redirect()
                ->route('roles')
                ->withErrors("Error al eliminar el rol");
        }

        $rol->delete();

        return redirect()
            ->route('roles')
            ->withSuccess("Rol elimindo correctamente");
    }
}
