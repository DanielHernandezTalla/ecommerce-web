<?php

namespace App\Http\Controllers;

use App\Models\CapRecepcion;
use App\Models\CapRecepcionManualTmp;
use App\Models\CatArticulos;
use App\Models\DatCentroVenta;
use App\Models\DatMovInvConcen;
use App\Models\DatMovInvDetalle;
use App\Models\DatRecepcion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RecepcionController extends Controller
{
    // Muestra las recepciones con sus detalles de recepcion
    public function index(Request $request)
    {
        $almacen = Auth::user()->CentroVenta->Almacen_Oracle;

        $recepcion = CapRecepcion::where('Almacen', $almacen)
            ->where('IdStatusRecepcion', 1)
            ->get();

        $idRecepcion = $request->idRecepcion;
        empty($idRecepcion) ? $idRecepcion = 0 : $idRecepcion = $idRecepcion;

        $detalleRecepcion = DB::table('DatRecepcion as a')
            ->leftJoin('CatArticulos as b', 'b.Codigo', 'a.CodArticulo')
            ->leftJoin('CapRecepcion as c', 'c.IdCapRecepcion', 'a.IdCapRecepcion')
            ->where('a.IdCapRecepcion', $idRecepcion)
            ->where('a.IdStatusRecepcion', 1)
            ->get();

        $totalRecepcion = DatRecepcion::where('IdCapRecepcion', $idRecepcion)
            ->where('IdStatusRecepcion', 1)
            ->sum('CantEnviada');

        // return $detalleRecepcion;

        return view('recepcion.index', compact('recepcion', 'detalleRecepcion', 'totalRecepcion', 'idRecepcion'))
            ->with('title', 'Recepcion de producto');
    }

    // Agrega al inventario la recepcion
    public function RecepcionarProducto($idRecepcion, Request $request)
    {
        $chkArticulo = $request->chkArticulo;
        $cantRecepcionada = $request->cantRecepcionada;

        if (empty($chkArticulo)) {
            return redirect()->route('recepcion.index')->with('msjdelete', 'Debe Seleccionar Productos a Recepcionar!');
        }

        try {
            DB::beginTransaction();

            $idCiudad = DatCentroVenta::where('IdDatCentroVenta', Auth::user()->IdDatCentroVenta)
                ->value('IdCatCiudad');

            foreach ($chkArticulo as $key => $referencia) {
                foreach ($cantRecepcionada as $codArticulo => $cRecepcionada) {
                    if ($key == $codArticulo) {
                        DatRecepcion::where('IdCapRecepcion', $idRecepcion)
                            ->where('CodArticulo', '' . $key . '')
                            ->update([
                                'CantRecepcionada' => $cRecepcionada,
                                'IdStatusRecepcion' => 2,
                            ]);

                        //Si hay inventario de ese articulo para centro de venta
                        $inventario = DatMovInvConcen::where('Codigo', '' . $key . '')
                            ->where('IdDatCentroVenta', Auth::user()->IdDatCentroVenta)
                            ->first();

                        $referencia = 0;

                        if (empty($inventario)) {
                            DatMovInvConcen::insert([
                                'IdCatCiudades' => $idCiudad,
                                'IdDatCentroVenta' => Auth::user()->IdDatCentroVenta,
                                'Codigo' => $key,
                                'Stock' => $cRecepcionada,
                            ]);

                            $referencia = DatMovInvConcen::where('IdDatCentroVenta', Auth::user()->IdDatCentroVenta)
                                ->max('IdDatInvConcen');
                        } else {
                            DatMovInvConcen::where('Codigo', '' . $key . '')
                                ->where('IdDatCentroVenta', Auth::user()->IdDatCentroVenta)
                                ->update([
                                    'Stock' => $inventario->Stock + $cRecepcionada,
                                ]);

                            $referencia = DatMovInvConcen::where('Codigo', '' . $key . '')
                                ->where('IdDatCentroVenta', Auth::user()->IdDatCentroVenta)
                                ->max('IdDatInvConcen');
                        }

                        DatMovInvDetalle::insert([
                            'IdCiudad' => $idCiudad,
                            'IdDatCentroVenta' => Auth::user()->IdDatCentroVenta,
                            'Codigo' => $key,
                            'Cantidad' => $cRecepcionada,
                            'FechaMovimiento' => date('d-m-Y H:i:s'),
                            'Referencia' => $idRecepcion,
                            'IdMovimientoinventario' => 3,
                            'IdCatUser' => Auth::user()->IdCatUser,
                        ]);
                    }
                }
            }

            // $faltantesPorRecepcionar = DatRecepcion::where('IdCapRecepcion', $idRecepcion)
            //     ->where('IdStatusRecepcion', 1)
            //     ->count();

            // if($faltantesPorRecepcionar == 0){
            CapRecepcion::where('IdCapRecepcion', $idRecepcion)
                ->update([
                    'IdStatusRecepcion' => 2,
                    'FechaRecepcion' => date('d-m-Y H:i:s'),
                    'IdCatUser' => Auth::user()->IdCatUser,
                ]);
            // }

            DB::commit();
            return redirect()->route('recepcion.index')->with('msjAdd', 'Productos Recepcionados Correctamente!');

        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->route('recepcion.index')->with('msjdelete', 'Error' . $th->getMessage());
        }
    }

    // Cancela una recepcion
    public function CancelarRecepcion($idRecepcion, Request $request)
    {
        $motivoCancelacion = $request->motivoCancelacion;

        try {
            DB::beginTransaction();

            CapRecepcion::where('IdCapRecepcion', $idRecepcion)
                ->update([
                    'IdStatusRecepcion' => 3,
                    'FechaCancelacion' => date('d-m-Y H:i:s'),
                    'MotivoCancelacion' => $motivoCancelacion,
                    'IdCatUser' => Auth::user()->IdCatUser,
                ]);

            DatRecepcion::where('IdCapRecepcion', $idRecepcion)
                ->update([
                    'IdStatusRecepcion' => 3,
                ]);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            return back()->with('msjdelete', 'Error: ' . $th->getMessage());
        }

        return back()->with('msjdelete', 'Recepción Cancelada Correctamente!');
    }

    // Vista para recepcionar producto manual
    public function ProductoManual(Request $request)
    {
        $articulos = CatArticulos::where('Status', 1)
            ->get();

        $productos = DB::table('CapRecepcionManualTmp as a')
            ->leftJoin('CatArticulos as b', 'b.Codigo', 'a.Codigo')
            ->where('Almacen_Oracle', Auth::user()->CentroVenta->Almacen_Oracle)
            ->get();

        //return $productos;

        return view('Recepcion.ProductoManual', compact('articulos', 'productos'))->with('title', 'Captura de producto manual');
    }

    public function AgregarProductoManual(Request $request)
    {
        $radioBuscar = $request->radioBuscar;
        $filtroArticulo = $request->filtroArticulo;

        $tienda = Tienda::where('IdTienda', Auth::user()->usuarioTienda->IdTienda)
            ->first();

        $articulos = Articulo::where('CodArticulo', '0')
            ->get();

        $articuloPendiente = 1;

        if ($radioBuscar == 'codigo') {
            $dRecepcion = DB::connection('server')->table('CapRecepcion as a')
                ->leftJoin('DatRecepcion as b', 'b.IdCapRecepcion', 'a.IdCapRecepcion')
                ->where('a.Almacen', $tienda->Almacen)
                ->whereNull('a.FechaRecepcion')
                ->where('a.IdStatusRecepcion', 1)
                ->where('b.CodArticulo', $filtroArticulo)
                ->get();

            if ($dRecepcion->count() == 0) {
                $articulos = Articulo::where('CodArticulo', $filtroArticulo)
                    ->get();
            } else {
                $articuloPendiente = 0;
            }
        } else {
            $articulos = Articulo::where('NomArticulo', 'like', '%' . $filtroArticulo . '%')
                ->whereRaw("CodArticulo not in" .
                    " (select c.CodArticulo" .
                    " from CapRecepcion as a" .
                    " left join DatRecepcion as b on b.IdCapRecepcion=a.IdCapRecepcion " .
                    " left join CatArticulos as c on c.CodArticulo=b.CodArticulo" .
                    " where a.Almacen = 'ALP-114'" .
                    " and a.IdStatusRecepcion = 1" .
                    " and c.NomArticulo like '%" . $filtroArticulo . "%')")
                ->get();
        }

        return view('Recepcion.AgregarProductoManual', compact('articulos', 'articuloPendiente'));
    }

    public function CapturaManualTmp(Request $request)
    {
        $codArticulo = $request->codArticulo;
        $cantArticulo = $request->cantArticulo;
        $idTienda = Auth::user()->usuarioTienda->IdTienda;

        $capturaManual = CapturaManualTmp::where('CodArticulo', $codArticulo)
            ->where('IdTienda', $idTienda)
            ->first();

        if (!empty($codArticulo) && !empty($cantArticulo)) {
            if (empty($capturaManual)) {
                CapturaManualTmp::insert([
                    'IdTienda' => $idTienda,
                    'CodArticulo' => $codArticulo,
                    'CantArticulo' => $cantArticulo,
                    'Referencia' => 'MANUAL',
                    'IdMovimiento' => 3,
                ]);
            } else {
                CapturaManualTmp::where('IdTienda', $idTienda)
                    ->where('CodArticulo', $codArticulo)
                    ->update([
                        'CantArticulo' => $capturaManual->CantArticulo + $cantArticulo,
                    ]);
            }
        }

        $articulosManual = DB::connection('server')->table('CapRecepcionManualTmp as a')
            ->leftJoin('CatArticulos as b', 'b.CodArticulo', 'a.CodArticulo')
            ->where('a.IdTienda', $idTienda)
            ->get();

        return view('Recepcion.CapturaManualTmp', compact('articulosManual'));
    }

    public function EliminarProductoManual($idCapRecepcionManual)
    {
        CapturaManualTmp::where('IdCapRecepcionManual', $idCapRecepcionManual)
            ->delete();

        return redirect('CapturaManualTmp');
    }

    public function TmpProductoManual(Request $request)
    {
        try {

            $codigo = $request->codigo;
            $cantidad = $request->cantidad;

            CapRecepcionManualTmp::insert([
                'Almacen_Oracle' => Auth::user()->CentroVenta->Almacen_Oracle,
                'Codigo' => $codigo,
                'Cantidad' => $cantidad,
            ]);

        } catch (\Throwable $th) {
            return back()->with('msjdelete', 'Error: ' . $th->getMessage());
        }

        return back();
    }

    public function EliminarProductoTmp($idProductoTmp)
    {
        try {

            CapRecepcionManualTmp::where('IdCapRecepcionManual', $idProductoTmp)
                ->delete();

        } catch (\Throwable $th) {
            return back()->with('msjdelete', 'Error: ' . $th->getMessage());
        }

        return back();
    }

    public function TmpRecepcionarProducto()
    {
        $productosManual = CapRecepcionManualTmp::where('Almacen_Oracle', Auth::user()->CentroVenta->Almacen_Oracle)
            ->get();

        $idCiudad = DatCentroVenta::where('IdDatCentroVenta', Auth::user()->IdDatCentroVenta)
            ->value('IdCatCiudad');

        try {
            foreach ($productosManual as $key => $producto) {
                $inventario = DatMovInvConcen::where('Codigo', '' . $producto->Codigo . '')
                    ->where('IdDatCentroVenta', Auth::user()->IdDatCentroVenta)
                    ->first();

                $referencia = 0;

                if (empty($inventario)) {
                    DatMovInvConcen::insert([
                        'IdCatCiudades' => $idCiudad,
                        'IdDatCentroVenta' => Auth::user()->IdDatCentroVenta,
                        'Codigo' => $producto->Codigo,
                        'Stock' => $producto->Cantidad,
                    ]);

                    $referencia = DatMovInvConcen::where('IdDatCentroVenta', Auth::user()->IdDatCentroVenta)
                        ->max('IdDatInvConcen');
                } else {
                    DatMovInvConcen::where('Codigo', '' . $producto->Codigo . '')
                        ->where('IdDatCentroVenta', Auth::user()->IdDatCentroVenta)
                        ->update([
                            'Stock' => $inventario->Stock + $producto->Cantidad,
                        ]);

                    $referencia = DatMovInvConcen::where('Codigo', '' . $producto->Codigo . '')
                        ->where('IdDatCentroVenta', Auth::user()->IdDatCentroVenta)
                        ->max('IdDatInvConcen');
                }

                DatMovInvDetalle::insert([
                    'IdCiudad' => $idCiudad,
                    'IdDatCentroVenta' => Auth::user()->IdDatCentroVenta,
                    'Codigo' => $producto->Codigo,
                    'Cantidad' => $producto->Cantidad,
                    'FechaMovimiento' => date('d-m-Y H:i:s'),
                    'Referencia' => $referencia,
                    'IdMovimientoinventario' => 4,
                    'IdCatUser' => Auth::user()->IdCatUser,
                ]);
            }

            CapRecepcionManualTmp::where('Almacen_Oracle', Auth::user()->CentroVenta->Almacen_Oracle)
                ->delete();

        } catch (\Throwable $th) {
            return back()->with('msjdelete', 'Error: ' . $th->getMessage());
        }

        return back()->with('msjAdd', 'Recepcion de producto exitosa');

    }

    public function ReporteRecepciones(Request $request)
    {
        $fecha1 = $request->fecha1;
        $fecha2 = $request->fecha2;
        $chkReferencia = $request->chkReferencia;
        $referencia = $request->referencia;

        $idTienda = Auth::user()->usuarioTienda->IdTienda;
        $tienda = Tienda::where('IdTienda', $idTienda)
            ->first();

        if (!empty($chkReferencia)) {
            $recepciones = CapRecepcion::with(['DetalleRecepcion' => function ($query) {
                $query->leftJoin('CatArticulos', 'CatArticulos.CodArticulo', 'DatRecepcion.CodArticulo')
                    ->leftJoin('CatStatusRecepcion', 'CatStatusRecepcion.IdStatusRecepcion', 'DatRecepcion.IdStatusRecepcion');
            }, 'StatusRecepcion'])
                ->where('Almacen', $tienda->Almacen)
                ->whereRaw("cast(FechaLlegada as date) between '" . $fecha1 . "' and '" . $fecha2 . "'")
                ->where('PackingList', $referencia)
                ->get();
        } else {
            $recepciones = CapRecepcion::with(['DetalleRecepcion' => function ($query) {
                $query->leftJoin('CatArticulos', 'CatArticulos.CodArticulo', 'DatRecepcion.CodArticulo')
                    ->leftJoin('CatStatusRecepcion', 'CatStatusRecepcion.IdStatusRecepcion', 'DatRecepcion.IdStatusRecepcion');
            }, 'StatusRecepcion'])
                ->where('Almacen', $tienda->Almacen)
                ->whereRaw("cast(FechaLlegada as date) between '" . $fecha1 . "' and '" . $fecha2 . "'")
                ->get();
        }

        //return $recepciones;

        return view('Recepcion.ReporteRecepciones', compact('recepciones', 'fecha1', 'fecha2', 'referencia', 'chkReferencia'));
    }

    public function RecepcionLocalSinInternet(Request $request)
    {
        exec("ping -n 1 google.com", $salida, $codigo);

        if ($codigo === 0) {
            return redirect('RecepcionProducto');
        }

        $articulos = Articulo::where('Status', 0)
            ->get();

        $capturasSinInternet = DB::table('CapRecepcionManualTmp as a')
            ->leftJoin('CatArticulos as b', 'b.CodArticulo', 'a.CodArticulo')
            ->where('a.IdTienda', Auth::user()->usuarioTienda->IdTienda)
            ->get();

        return view('Recepcion.RecepcionLocalSinInternet', compact('articulos', 'capturasSinInternet'));
    }

    public function AgregarProductoLocalSinInternet(Request $request)
    {
        try {
            DB::beginTransaction();

            RecepcionSinInternet::insert([
                'IdTienda' => Auth::user()->usuarioTienda->IdTienda,
                'CodArticulo' => $request->codArticulo,
                'CantArticulo' => $request->cantArticulo,
                'IdMovimiento' => 13,
            ]);

        } catch (\Throwable $th) {
            DB::rollback();
            return $th;
        }

        DB::commit();
        return back();
    }

    public function EliminarArticuloSinInternet($idCapRecepcionManual)
    {
        try {
            DB::beginTransaction();

            RecepcionSinInternet::where('IdCapRecepcionManual', $idCapRecepcionManual)
                ->where('IdTienda', Auth::user()->usuarioTienda->IdTienda)
                ->delete();

        } catch (\Throwable $th) {
            DB::rollback();
            return back()->with('msjDelete', 'Error: ' . $th->getMessage());
        }

        DB::commit();
        return back();
    }

    public function RecepcionarProductoSinInternet(Request $request)
    {
        $origen = $request->origen;

        try {
            DB::beginTransaction();

            $productos = RecepcionSinInternet::where('IdTienda', Auth::user()->usuarioTienda->IdTienda)
                ->get();

            $idCapRecepcion = DB::table('CapRecepcion')
                ->max('IdCapRecepcion') + 1;

            $numCaja = DatCaja::where('Status', 0)
                ->where('Activa', 0)
                ->where('IdTienda', Auth::user()->usuarioTienda->IdTienda)
                ->value('IdCaja');

            $idRecepcion = Auth::user()->usuarioTienda->IdTienda . $numCaja . $idCapRecepcion;

            DB::table('CapRecepcion')->insert([
                'IdRecepcionLocal' => $idRecepcion,
                'FechaRecepcion' => date('d-m-Y H:i:s'),
                'FechaLlegada' => date('d-m-Y H:i:s'),
                'PackingList' => 'RECEPCION SIN INTERNET MANUAL',
                'IdTiendaOrigen' => null,
                'Almacen' => null,
                'IdStatusRecepcion' => 2,
                'IdUsuario' => Auth::user()->IdUsuario,
                'IdTienda' => Auth::user()->usuarioTienda->IdTienda,
                'IdCaja' => $numCaja,
                'StatusInventario' => 0,
            ]);

            //Sacar el IdCapRecepcion que acabo insertar
            $idCapRecepcion = DB::table('CapRecepcion')->where('IdTienda', Auth::user()->usuarioTienda->IdTienda)
                ->max('IdCapRecepcion');

            //Insertar inventario y detalle de la recepcion
            foreach ($productos as $key => $producto) {
                $stock = InventarioTienda::where('IdTienda', Auth::user()->usuarioTienda->IdTienda)
                    ->where('CodArticulo', $producto->CodArticulo)
                    ->sum('StockArticulo');

                if (empty($stock)) {

                    InventarioTienda::insert([
                        'IdTienda' => Auth::user()->usuarioTienda->IdTienda,
                        'CodArticulo' => $producto->CodArticulo,
                        'StockArticulo' => $producto->CantArticulo,
                    ]);

                } else {
                    InventarioTienda::where('IdTienda', Auth::user()->usuarioTienda->IdTienda)
                        ->where('CodArticulo', $producto->CodArticulo)
                        ->update([
                            'StockArticulo' => $stock + $producto->CantArticulo,
                        ]);
                }

                DB::table('DatRecepcion')->insert([
                    'IdCapRecepcion' => $idCapRecepcion,
                    'IdRecepcionLocal' => $idRecepcion,
                    'CodArticulo' => $producto->CodArticulo,
                    'CantEnviada' => $producto->CantArticulo,
                    'CantRecepcionada' => $producto->CantArticulo,
                    'Linea' => $key + 1,
                    'IdStatusRecepcion' => 2,
                ]);

                DB::table('DatHistorialMovimientos')->insert([
                    'IdTienda' => Auth::user()->usuarioTienda->IdTienda,
                    'CodArticulo' => $producto->CodArticulo,
                    'CantArticulo' => $producto->CantArticulo,
                    'FechaMovimiento' => date('d-m-Y H:i:s'),
                    'Referencia' => strtoupper($origen),
                    'IdMovimiento' => 13,
                    'IdUsuario' => Auth::user()->IdUsuario,
                ]);
            }

            RecepcionSinInternet::where('IdTienda', Auth::user()->usuarioTienda->IdTienda)
                ->delete();

        } catch (\Throwable $th) {
            DB::rollback();
            return back()->with('msjdelete', 'Error: ' . $th->getMessage());
        }

        DB::commit();
        return back()->with('msjAdd', 'Se recepcionó el producto correctamente');
    }
}
