<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClienteNuevoFacturacionRequest;
use App\Http\Requests\FacturaRequest;
use App\Models\CatClientesCloud;
use App\Models\DatClientestoOracle;
use App\Models\DatSolicitudFactura;
use Illuminate\Http\Request;

class FacturacionController extends Controller
{
    // Muestra las recepciones con sus detalles de recepcion
    public function index(Request $request)
    {
        $solicitudes = DatSolicitudFactura::leftJoin('CatMetododePago', 'CatMetododePago.IdCatMetododePago', 'DatSolicitudFactura.IdCatMetodoPago')
            ->leftJoin('CatUsoCFDI', 'CatUsoCFDI.IdCatUsoCFDI', 'DatSolicitudFactura.IdCatUsoCFDI')
            ->orderBy('IdSolictudFe', 'desc')->paginate(10);

        // return $solicitudes;

        return view('facturacion.index', compact('solicitudes'))->with('title', 'Solicitudes de facturación');
    }

    public function create(ClienteNuevoFacturacionRequest $request)
    {
        try {
            $cliente = new DatClientestoOracle();

            $cliente->IDSOLICITUD = 1;
            $cliente->TIPOPERSONA = $request->validated()['TIPOPERSONA'];
            $cliente->RFC = strtoupper($request->validated()['RFC']);
            $cliente->NOMBRE = $request->validated()['NOMBRE'];
            $cliente->MAIL = $request->validated()['MAIL'];
            $cliente->COLONIA = $request->validated()['COLONIA'];
            $cliente->CALLE = $request->validated()['CALLE'];
            $cliente->NUM_EXT = $request->validated()['NUM_EXT'];
            $cliente->NUM_INT = $request->validated()['NUM_INT'];
            $cliente->CP = $request->validated()['CP'];
            $cliente->CIUDAD = $request->validated()['CIUDAD'];
            $cliente->MUNICIPIO = $request->validated()['MUNICIPIO'];
            $cliente->ESTADO = $request->validated()['ESTADO'];
            $cliente->PAIS = $request->validated()['PAIS'];
            $cliente->STATUS = 1;

            $cliente->save();

            return redirect()
                ->route('facturacion.index')
                ->withSuccess("Cliente creado correctamente");
        } catch (\Exception $error) {
            return redirect()
                ->route('facturacion.index')
                ->withErrors("Error al agregar el cliente");
        }
    }

    public function edit(Request $request)
    {
        $IdSolictudFe = $request->IdSolictudFe;

        $RFC = $request->RFC;

        $solicitud = DatSolicitudFactura::where('IdSolictudFe', $IdSolictudFe)->first();

        $datosCliente = CatClientesCloud::where('RFC', $RFC)->first();

        // return $datosCliente;

        return view('facturacion.edit', compact('solicitud', 'RFC', 'datosCliente'))->with('title', 'Solicitud de facturación');
    }

    public function update($IdSolictudFe, ClienteNuevoFacturacionRequest $request)
    {
        if ($request->isUpdate) {
            $cliente = new DatClientestoOracle();

            $cliente->IDSOLICITUD = 1;
            $cliente->TIPOPERSONA = $request->validated()['TIPOPERSONA'];
            $cliente->RFC = strtoupper($request->validated()['RFC']);
            $cliente->NOMBRE = $request->validated()['NOMBRE'];
            $cliente->MAIL = $request->validated()['MAIL'];
            $cliente->COLONIA = $request->validated()['COLONIA'];
            $cliente->CALLE = $request->validated()['CALLE'];
            $cliente->NUM_EXT = $request->validated()['NUM_EXT'];
            $cliente->NUM_INT = $request->validated()['NUM_INT'];
            $cliente->CP = $request->validated()['CP'];
            $cliente->CIUDAD = $request->validated()['CIUDAD'];
            $cliente->MUNICIPIO = $request->validated()['MUNICIPIO'];
            $cliente->ESTADO = $request->validated()['ESTADO'];
            $cliente->PAIS = $request->validated()['PAIS'];
            $cliente->STATUS = 2;

            $cliente->save();
            // Usuario creado correctamente Cliente en proceso de actualización

            return redirect()
                ->route('facturacion.index')
                ->withSuccess("Cliente en proceso de actualización");
        }
        if (!$request->isUpdate) {
            DatSolicitudFactura::where('IdSolictudFe', $IdSolictudFe)
                ->update([
                    'ID_CLIENTE' => $request->ID_CLIENTE,
                    'NOMBRE' => $request->NOMBRE,
                    'TIPO_CLIENTE' => $request->TIPOPERSONA,
                    'Bill_To' => $request->Bill_To,
                    'Ship_To' => $request->Ship_To,
                ]);

            return redirect()
                ->route('facturacion.index')
                ->withSuccess("Cliente relacionado correctamente");
        }
    }
}
