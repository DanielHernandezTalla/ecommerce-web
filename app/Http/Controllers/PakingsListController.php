<?php

namespace App\Http\Controllers;

use App\Models\CapRecepcion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PakingsListController extends Controller
{
    // Muestra las recepciones con sus detalles de recepcion
    public function index(Request $request)
    {
        $almacen = Auth::user()->CentroVenta->Almacen_Oracle;

        $recepcion = CapRecepcion::where('Almacen', $almacen)
            ->where('IdStatusRecepcion', 2)
            ->orderBy('CapRecepcion.IdCapRecepcion', 'desc')
            ->paginate(10);

        $idRecepcion = $request->idPaking;
        empty($idRecepcion) ? $idRecepcion = 0 : $idRecepcion = $idRecepcion;

        $detalleRecepcion = DB::table('DatRecepcion as a')
            ->leftJoin('CatArticulos as b', 'b.Codigo', 'a.CodArticulo')
            ->leftJoin('CapRecepcion as c', 'c.IdCapRecepcion', 'a.IdCapRecepcion')
            ->where('a.IdCapRecepcion', $idRecepcion)
            ->orderBy('a.IdCapRecepcion', 'asc')
            ->get();

        return view('pakinglist.index', compact('recepcion', 'detalleRecepcion', 'idRecepcion'))
            ->with('title', 'Pakings list');
    }

    function print(Request $request) {

        $idRecepcion = $request->idRecepcion;
        empty($idRecepcion) ? $idRecepcion = 0 : $idRecepcion = $idRecepcion;

        $almacen = Auth::user()->CentroVenta->Almacen_Oracle;

        $recepcion = CapRecepcion::where('Almacen', $almacen)
            ->where('IdStatusRecepcion', 2)
            ->where('CapRecepcion.IdCapRecepcion', $idRecepcion)
            ->first();

        // return $recepcion;

        $detalleRecepcion = DB::table('DatRecepcion as a')
            ->leftJoin('CatArticulos as b', 'b.Codigo', 'a.CodArticulo')
            ->leftJoin('CapRecepcion as c', 'c.IdCapRecepcion', 'a.IdCapRecepcion')
            ->where('a.IdCapRecepcion', $idRecepcion)
            ->orderBy('a.IdCapRecepcion', 'asc')
            ->get();

        $title = 'Pakings list ' . $recepcion->PackingList;

        $pdf = \PDF::loadView('pakinglist.print', compact('recepcion', 'detalleRecepcion', 'title'));
        return $pdf->download($title . ".pdf");

        return view('pakinglist.print', compact('recepcion', 'detalleRecepcion', 'title'));
    }
}
