<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Request as IlluminateRequest;
use Illuminate\support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        $userType = Auth::user();

        if (!$userType) {
            return view('Auth.Login');
        }

        $userType = Auth::user()->leftJoin('CatTipoUser', 'CatTipoUser.IdCatTipoUser', 'CatUsers.IdCatTipoUser')->where('CatUsers.IdCatUser', Auth::user()->IdCatUser)->first()->Descripcion;

        return $this->redirectTo($userType);
    }

    public function showLoginForm(IlluminateRequest $request)
    {
        if ($request->has('redirect_to')) {
            session()->put('redirect_to', $request->input('redirect_to'));
        }

        return view('Auth.Login');
    }

    public function Autenticarse(Request $request)
    {
        $reglas = [
            'username' => ['required'],
            'password' => ['required'],
        ];

        request()->validate($reglas);

        $credenciales = [
            'username' => $request->username,
            'password' => $request->password,
            'Status' => 1,
        ];

        if (Auth::attempt($credenciales)) {
            // $userType = Auth::user()->IdCatTipoUser;
            $userType = Auth::user()->leftJoin('CatTipoUser', 'CatTipoUser.IdCatTipoUser', 'CatUsers.IdCatTipoUser')->where('CatUsers.IdCatUser', Auth::user()->IdCatUser)->first()->Descripcion;

            return $this->redirectTo($userType);
        }
        return redirect()->route('login')->with('msjdelete', 'Credenciales incorrectas');
    }

    public function Logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('login');
    }

    public function unautorize()
    {
        return view('error.403');
    }

    public function redirectTo($userType)
    {
        if (strtolower($userType) == 'ejecutivo') return redirect()->route('pedidos.pendientes');

        if (strtolower($userType) == 'armador') return redirect()->route('pedidos.proceso');
        
        if (strtolower($userType) == 'admin') return redirect()->route('articulos.index');

        if (strtolower($userType) == 'facturista') return redirect()->route('cortediario');
        
        if (strtolower($userType) == 'auditor') return redirect()->route('inventario.index');

        if (strtolower($userType) == 'mercadotecnia') return redirect()->route('articulos.index');

        // return 'No perteneces aqui';
        return redirect()->route('unautorize')->with('msjdelete', 'Credenciales incorrectas');
    }
}
