<?php

namespace App\Http\Controllers;

use App\Models\Ciudad;
use App\Models\DatClientesCloudCentroVenta;
use App\Models\DatPagos;
use App\Models\DatSolicitudFactura;
use App\Models\DatVentas;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\support\Facades\Auth;
use Illuminate\support\Facades\DB;

class PedidoController extends Controller
{
    // Pedidos pendientes
    public function pendientes()
    {
        $statusPedido = 1;
        $pedidos = $this->getPedidos($statusPedido);

        return view('pedidos.pendientes', compact('pedidos'))->with('title', 'Pedidos pendientes');
    }

    // Pedidos en proceso de preparacion
    public function proceso()
    {
        $statusPedido = 2;
        $pedidos = $this->getPedidos($statusPedido);

        return view('pedidos.proceso', compact('pedidos'))->with('title', 'Pedidos pendientes');
    }

    // Pedidos preparados
    public function preparados()
    {
        $statusPedido = 3;
        $pedidosPreparados = $this->getPedidos($statusPedido);

        return view('pedidos.preparados', compact('pedidosPreparados'))->with('title', 'Pedidos preparados');
    }

    // Pedidos enviados
    public function enviados()
    {
        $statusPedido = 4;
        $pedidosEnviados = $this->getPedidos($statusPedido);

        return view('pedidos.enviados', compact('pedidosEnviados'))->with('title', 'Pedidos enviados');
    }

    // Pedidos entregados
    public function entregados()
    {
        $statusPedido = 5;

        $pedidos = DatVentas::select('DatVentas.IdCatUsers', 'DatVentas.IdDatVentas', 'DatVentas.Fecha_Pedido', 'DatVentas.Importe', 'DatVentas.Comentario', 'CatCteDirEnv.Direccion')
            ->with(['DetallesVentas' => function ($articulos) {
                $articulos->leftJoin('DatArticulos', 'DatArticulos.IdDatArticulos', 'DatVtaDetalle.IdDatArticulos');
                $articulos->leftJoin('CatArticulos', 'CatArticulos.IdCatArticulos', 'DatArticulos.IdCatArticulos');
                $articulos->leftJoin('CatServicios', 'CatServicios.IdCatServicios', 'DatVtaDetalle.IdCatServicios');
            }, 'Cliente'])
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            ->leftJoin('CatCteDirEnv', 'CatCteDirEnv.IdCatCteDirEnv', 'DatVentas.IdCatCteDirEnv')
            ->where('DatPagos.Status_Pedido', $statusPedido)
            ->where('DatVentas.IdDatCentroVenta ', auth()->user()->IdDatCentroVenta)
            ->orderBy('DatVentas.Fecha_Pedido', 'DESC')
            ->get();

        for ($i = 0; $i <= count($pedidos) - 1; $i++) {
            $totalVentas = DatVentas::leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
                ->where([['DatVentas.IdCatUsers', $pedidos[$i]->IdCatUsers]])
                ->get();

            $pedidos[$i]->isFirst = count($totalVentas) == 1 ? true : false;
        }

        return view('pedidos.entregados', compact('pedidos'))->with('title', 'Pedidos entregados');
    }

    // Cambiar pedido de pendiente a proceso
    public function UpdatePreapararPedido($id)
    {
        DatPagos::where('IdDatVentas', $id)
            ->update([
                'Status_Pedido' => 2,
            ]);

        return redirect()->route('pedidos.pendientes');
    }

    // Cambiar peddido de proceso a preparado
    public function UpdatePorEnviar($id)
    {
        DatPagos::where('IdDatVentas', $id)
            ->update([
                'Status_Pedido' => 3,
            ]);

        return redirect()->route('pedidos.proceso');
    }

    // Cambiar pedido de preparado a enviado
    public function UpdatePedidoEnviado($id)
    {
        DatPagos::where('IdDatVentas', $id)
            ->update([
                'Status_Pedido' => 4,
            ]);

        return redirect()->route('pedidos.preparados');
    }

    // Cambiar pedido de enviado a entregado
    public function UpdatePedidoEntregado($id)
    {
        DatPagos::where('IdDatVentas', $id)
            ->update([
                'Status_Pedido' => 5,
            ]);

        return redirect()->route('pedidos.enviados');
    }

    // Corte diario
    public function CorteDiario(Request $request)
    {
        // Aqui obtenemos el centro de centa auth()->user()->IdDatCentroVenta
        $ecommerce = Auth::user()
            ->leftJoin('DatCentroVenta', 'DatCentroVenta.IdDatCentroVenta', 'CatUsers.IdDatCentroVenta')
            ->where('CatUsers.IdDatCentroVenta', Auth::user()->IdDatCentroVenta)
            ->first()->Descripcion;

        $fecha = \Carbon\Carbon::now()->format('Y-m-d');

        if (isset($request->fecha)) {
            $fecha = $request->fecha;
        }

        // return $fecha;
        $pedidosSinFactura = DatClientesCloudCentroVenta::with(['Ventas' => function ($venta) use ($fecha) {
            $venta->select(
                'IdCatTipoPago',
                'CatArticulos.Codigo',
                'CatArticulos.Descripcion',
                'DatVtaDetalle.Precio',
                DB::raw('SUM(DatVtaDetalle.CantidadSecundario) as Cantidad'),
                DB::raw('SUM(DatVtaDetalle.Importe) as Importe'),
                DB::raw('SUM(DatPagos.Importe_Pago) as ImportePago'),
                DB::raw('SUM(DatPagos.Costo_Envio) as Costo_Envio')
            );

            $venta->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas');
            $venta->leftJoin('DatVtaDetalle', 'DatVtaDetalle.IdDatVentas', 'DatVentas.IdDatVentas');
            $venta->leftJoin('DatArticulos', 'DatArticulos.IdDatArticulos', 'DatVtaDetalle.IdDatArticulos');
            $venta->leftJoin('CatArticulos', 'CatArticulos.IdCatArticulos', 'DatArticulos.IdCatArticulos');

            $venta->groupBy('IdCatTipoPago', 'CatArticulos.Codigo', 'CatArticulos.Descripcion', 'DatVtaDetalle.Precio');
            $venta->whereNotNull('DatPagos.IdDatPagos');
            $venta->whereNull('DatPagos.SolicitudFE');
            $venta->whereDay('DatPagos.Fecha_Pago', substr($fecha, 8, 2));
            $venta->whereMonth('DatPagos.Fecha_Pago', substr($fecha, 5, 2));
            $venta->whereYear('DatPagos.Fecha_Pago', substr($fecha, 0, 4));
            $venta->where([['DatVentas.IdDatCentroVenta', Auth::user()->IdDatCentroVenta], ['DatPagos.Status_Pedido', 5]]);
        }, 'Total' => function ($venta) use ($fecha) {
            $venta->select(
                'IdCatTipoPago',
                DB::raw('SUM(DatPagos.Importe_Pago) as ImportePago'),
                DB::raw('SUM(DatPagos.Costo_Envio) as Costo_Envio')
            );

            $venta->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas');

            $venta->groupBy('IdCatTipoPago');
            $venta->whereNotNull('DatPagos.IdDatPagos');
            $venta->whereNull('DatPagos.SolicitudFE');
            $venta->whereDay('DatPagos.Fecha_Pago', substr($fecha, 8, 2));
            $venta->whereMonth('DatPagos.Fecha_Pago', substr($fecha, 5, 2));
            $venta->whereYear('DatPagos.Fecha_Pago', substr($fecha, 0, 4));
            $venta->where([['DatVentas.IdDatCentroVenta', Auth::user()->IdDatCentroVenta], ['DatPagos.Status_Pedido', 5]]);
        }])
            ->where('DatClientesCloudCentroVenta.IdDatCentroVenta', Auth::user()->IdDatCentroVenta)
            ->orderBy('DatClientesCloudCentroVenta.ID_CLIENTE')
            ->get();

        // return $pedidosSinFactura;
        $pedidosConFactura = DatSolicitudFactura::with(['Ventas' => function ($venta) use ($fecha) {
            $venta->select(
                'DatVentas.IdDatVentas',
                'CatArticulos.Codigo',
                'CatArticulos.Descripcion',
                'DatVtaDetalle.Precio',
                'DatPagos.SolicitudFE',
                DB::raw('SUM(DatVtaDetalle.CantidadSecundario) as Cantidad'),
                DB::raw('SUM(DatVtaDetalle.Importe) as Importe'),
                DB::raw('SUM(DatPagos.Importe_Pago) as ImportePago')
            );

            $venta->leftJoin('DatVtaDetalle', 'DatVtaDetalle.IdDatVentas', 'DatVentas.IdDatVentas');
            $venta->leftJoin('DatArticulos', 'DatArticulos.IdDatArticulos', 'DatVtaDetalle.IdDatArticulos');
            $venta->leftJoin('CatArticulos', 'CatArticulos.IdCatArticulos', 'DatArticulos.IdCatArticulos');
            $venta->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas');
            $venta->groupBy('DatVentas.IdDatVentas', 'CatArticulos.Codigo', 'CatArticulos.Descripcion', 'DatVtaDetalle.Precio', 'DatPagos.SolicitudFE');
            $venta->whereDay('DatPagos.Fecha_Pago', substr($fecha, 8, 2));
            $venta->whereMonth('DatPagos.Fecha_Pago', substr($fecha, 5, 2));
            $venta->whereYear('DatPagos.Fecha_Pago', substr($fecha, 0, 4));
            $venta->where('DatPagos.Status_Pedido', 5);
        }])
            ->leftJoin('DatVentas', 'DatVentas.IdDatVentas', 'DatSolicitudFactura.IdDatVentas')
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            ->get();

        // return $pedidosConFactura;
        $pedidos = [];
        $data = [];
        foreach ($pedidosSinFactura as $pedido) {
            if (count($pedido->Ventas) > 0) {
                // $source = DB::select("select [dbo].[FNgetSourceTransaction] (" . $pedido->IdCatTipopago . ", '" . $fecha . "' ) as Source_Transaction_Number");
                // $datadetail = ['cliente' => $pedido, 'source' => 'ECO_383659'];
                $source = DB::select("select [dbo].[FNgetSourceTransaction] (" . $pedido->IdCatTipopago . ", " . $pedido->IdDatCentroVenta . ", '" . $fecha . "' ) as Source_Transaction_Number");
                $datadetail = ['cliente' => $pedido, 'source' => $source[0]->Source_Transaction_Number];
                array_push($data, $datadetail);
            }
        }

        array_push($pedidos, $data);
        $data = [];
        foreach ($pedidosConFactura as $pedido) {
            if (count($pedido->Ventas) > 0) {
                // return $pedido['Ventas'][0]->IdDatVentas;
                // $source = DB::select("select [dbo].[FNgetSourceTransaction] (" . $pedido->IdCatTipopago . ", '" . $fecha . "' ) as Source_Transaction_Number");
                // $datadetail = ['cliente' => $pedido, 'source' => 'ECO_383659'];
                $source = DB::select("select [dbo].[FNgetSourceTransactionConFactura] (" . $pedido['Ventas'][0]->IdDatVentas . ") as Source_Transaction_Number");
                // return $source;
                $datadetail = ['cliente' => $pedido, 'source' => $source[0]->Source_Transaction_Number];
                array_push($data, $datadetail);
            }
        }
        array_push($pedidos, $data);

        // Calculamos el descuento stripe
        $pagos = DatPagos::select('DatPagos.Importe_Pago')
            ->leftjoin('DatVentas', 'DatVentas.IdDatVentas', 'DatPagos.IdDatVentas')
            ->whereDay('DatPagos.Fecha_Pago', substr($fecha, 8, 2))
            ->whereMonth('DatPagos.Fecha_Pago', substr($fecha, 5, 2))
            ->whereYear('DatPagos.Fecha_Pago', substr($fecha, 0, 4))
            ->where('DatPagos.Status_Pedido', 5)
            ->where('DatVentas.IdDatCentroVenta', Auth::user()->IdDatCentroVenta)
            ->get();

        $subtotalGeneral = 0;
        $totalGeneral = 0;
        foreach ($pagos as $pago) {
            $subtotalGeneral += $pago->Importe_Pago;
            $totalGeneral += $pago->Importe_Pago - (($pago->Importe_Pago * .036) + 3) * 1.16;
        }

        $ciudades = Ciudad::get();

        // Este es el bueno de source
        // return $data->subTotal;
        // return $data[1]['cliente'];
        // return $data[1]['cliente']->Ventas;
        // return $data[count($data)-1]['total'];

        return view('pedidos.cortediario', compact('pedidos', 'totalGeneral', 'subtotalGeneral', 'ciudades', 'fecha'))->with(['title' => 'Corte diario', 'textSecondary' => $ecommerce]);
    }

    private function getPedidos($statusPedido)
    {
        $pedidos = DatVentas::select('DatVentas.IdCatUsers', 'DatVentas.IdDatVentas', 'DatVentas.Fecha_Pedido', 'DatVentas.Importe', 'DatVentas.Comentario', 'CatCteDirEnv.Direccion')
            ->with(['DetallesVentas' => function ($articulos) {
                $articulos->leftJoin('DatArticulos', 'DatArticulos.IdDatArticulos', 'DatVtaDetalle.IdDatArticulos');
                $articulos->leftJoin('CatArticulos', 'CatArticulos.IdCatArticulos', 'DatArticulos.IdCatArticulos');
                $articulos->leftJoin('CatServicios', 'CatServicios.IdCatServicios', 'DatVtaDetalle.IdCatServicios');
            }, 'Cliente'])
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            ->leftJoin('CatCteDirEnv', 'CatCteDirEnv.IdCatCteDirEnv', 'DatVentas.IdCatCteDirEnv')
            ->where('DatPagos.Status_Pedido', $statusPedido)
            ->where('DatVentas.IdDatCentroVenta ', auth()->user()->IdDatCentroVenta)
            ->orderBy('DatVentas.Fecha_Pedido', 'DESC')
            ->paginate(10);

        for ($i = 0; $i <= count($pedidos) - 1; $i++) {
            $totalVentas = DatVentas::leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
                ->where([['DatVentas.IdCatUsers', $pedidos[$i]->IdCatUsers]])
                ->get();

            $pedidos[$i]->isFirst = count($totalVentas) == 1 ? true : false;
        }
        return $pedidos;
    }

    public function pedidosPrint($id, Request $request)
    {
        $pedidosEnviados = DatVentas::with(['DetallesVentas' => function ($articulos) {
            $articulos->leftJoin('DatArticulos', 'DatArticulos.IdDatArticulos', 'DatVtaDetalle.IdDatArticulos');
            $articulos->leftJoin('CatArticulos', 'CatArticulos.IdCatArticulos', 'DatArticulos.IdCatArticulos');
        }, 'ClienteDir'])
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            // ->where('DatPagos.Status_Pedido', 4)
            ->where('DatVentas.IdDatVentas', $id)
            ->get();


        $nameReport = 'ticket venta ' . $pedidosEnviados[0]->IdDatVentas . '.pdf';
        $pdf = \PDF::loadView('reportes.pdf.ticketPedido', compact('pedidosEnviados'));
        return $pdf->download($nameReport);

        return view('reportes.pdf.ticketPedido', compact('pedidosEnviados'));
    }
}
