<?php

namespace App\Http\Controllers;

use App\Http\Requests\ListaPreciosRequest;
use App\Models\CatListaPrecio;

class ListaPreciosController extends Controller
{
    public function index()
    {
        $listaPrecios = CatListaPrecio::paginate(10);

        return view('listaprecios.index', compact('listaPrecios'))->with('title', 'Catalogo lista de precios');
    }

    public function create()
    {
        return view('listaprecios.create')->with('title', 'Registrar lista de precios');
    }

    public function edit($id)
    {
        $lista = CatListaPrecio::where('IdCatListaPrecio', $id)->first();

        return view('listaprecios.edit', compact('lista'))->with('title', 'Editar lista de usuarios');
    }

    public function store(ListaPreciosRequest $request)
    {
        try {
            $lista = new CatListaPrecio();

            $lista->Status = isset($request->validated()['Status']);
            $lista->Descripcion = $request->validated()['Descripcion'];

            $lista->save();

            return redirect()
                ->route('listadeprecios.index')
                ->withSuccess("Rol creado correctamente: {$lista->Description}");
        } catch (\Exception $error) {
            return redirect()
                ->route('listadeprecios.index')
                ->withErrors("Error al agregar el rol");
        }
    }

    public function update($id, ListaPreciosRequest $request)
    {
        try {
            CatListaPrecio::where('IdCatListaPrecio', $id)
                ->update([
                    'Descripcion' => $request->validated()['Descripcion'],
                    'Status' => isset($request->validated()['Status']),
                ]);

            return redirect()
                ->route('listadeprecios.index')
                ->withSuccess("Lista de precios actualizada correctamente actualizado correctamente");
        } catch (\Exception $error) {
            return redirect()
                ->back()
                ->withInput($request->all())
                ->withErrors($error->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            $lista = CatListaPrecio::where('IdCatListaPrecio', $id)->first();
            $lista->delete();

            return redirect()
                ->route('listadeprecios.index')
                ->withSuccess("Lista de precios eliminada correctamente");
        } catch (\Throwable $th) {
            return redirect()
                ->route('listadeprecios.index')
                ->withErrors($th->getMessage());
        }
    }

}
