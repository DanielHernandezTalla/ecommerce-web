<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClienteNuevoFacturacionRequest;
use App\Http\Requests\FacturaRequest;
use App\Models\CatClientesCloud;
use App\Models\DatClientestoOracle;
use App\Models\DatSolicitudFactura;
use Illuminate\Http\Request;

class ClientesNuevosFacturacionController extends Controller
{
    // Muestra los clientes nuevos de facturaciones
    public function index(Request $request)
    {
        $clientes = DatClientestoOracle::orderBy('DatClientestoOracle.ID', 'DESC')->paginate(10);

        // return $clientes;

        return view('clientesnuevos.index', compact('clientes'))->with('title', 'Clientes nuevos de facturaciones');
    }
}
