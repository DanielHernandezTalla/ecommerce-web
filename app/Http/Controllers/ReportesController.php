<?php

namespace App\Http\Controllers;

use App\Models\Ciudad;
use App\Models\DatCentroVenta;
use App\Models\DatClientesCloudCentroVenta;
use App\Models\DatPagos;
use App\Models\DatSolicitudFactura;
use App\Models\DatVentas;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\support\Facades\Auth;
use Illuminate\support\Facades\DB;

class ReportesController extends Controller
{
    // Pedidos entregados
    public function ventas(Request $request)
    {
        $idCentroVenta = $request->idCentroVenta; // Id por parametro
        $fechaStart = $request->fechaStart;
        $fechaEnd = $request->fechaEnd;
        $dStart = $request->fechaStart;
        $dEnd = $request->fechaEnd;

        if (!$dStart) {
            $dStart = Carbon::now()->parse(date("2022-01-01"))->format('Y-m-d');
        } else {
            $dStart = Carbon::now()->parse($fechaStart)->format('Y-m-d');
        }

        if (!$dEnd) {
            $dEnd = Carbon::now()->parse(date(now()))->format('Y-m-d');
        } else {
            $dEnd = Carbon::now()->parse($dEnd)->format('Y-m-d');
        }

        $pedidosEntregados = DatVentas::select('DatVentas.IdCatUsers', 'DatVentas.IdDatVentas', 'DatPagos.Fecha_Pago as Fecha_Pedido', 'DatVentas.Importe', 'DatVentas.Comentario', 'CatCiudades.Descripcion as Ciudad')
            ->with(['Cliente'])
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            ->leftJoin('DatCentroVenta', 'DatCentroVenta.IdDatCentroVenta', 'DatVentas.IdDatCentroVenta')
            ->leftJoin('CatCiudades', 'CatCiudades.IdCatCiudades', 'DatCentroVenta.IdCatCiudad')
            ->whereNotNull('DatPagos.Status_Pedido')
            ->whereDate('DatPagos.Fecha_Pago', '>=', $dStart)
            ->whereDate('DatPagos.Fecha_Pago', '<=', $dEnd)
            ->when($idCentroVenta, function ($query) use ($idCentroVenta) {
                $query->where('DatVentas.IdDatCentroVenta', $idCentroVenta);
            })
            ->where('DatPagos.Status_Pedido', 5)
            ->orderBy('DatVentas.IdDatVentas', 'DESC')
            ->paginate(10);

        // return $pedidosEntregados;

        $centrosVenta = DatCentroVenta::where('Status', 1)
            ->get();

        return view('reportes.entregados', compact('pedidosEntregados', 'centrosVenta', 'idCentroVenta', 'fechaStart', 'fechaEnd'))->with('title', 'Reporte ventas');
    }

    public function concentrado(Request $request)
    {
        $idCentroVenta = $request->idCentroVenta; // Id por parametro
        $fechaStart = $request->fechaStart;
        $fechaEnd = $request->fechaEnd;
        $dStart = $request->fechaStart;
        $dEnd = $request->fechaEnd;

        if (!$dStart) {
            $dStart = Carbon::now()->parse(date("2022-01-01"))->format('Y-m-d');
        } else {
            $dStart = Carbon::now()->parse($fechaStart)->format('Y-m-d');
        }

        if (!$dEnd) {
            $dEnd = Carbon::now()->parse(date(now()))->format('Y-m-d');
        } else {
            $dEnd = Carbon::now()->parse($dEnd)->format('Y-m-d');
        }

        // return $pedidosEntregados = DatVentas::select('DatVentas.IdCatUsers', 'DatVentas.IdDatVentas', 'DatPagos.Fecha_Pago as Fecha_Pedido', 'DatVentas.Importe', 'DatVentas.Comentario', 'CatCiudades.Descripcion as Ciudad')
        $pedidosEntregados = DatVentas::select(
            'CatArticulos.Codigo',
            'CatArticulos.DesCorta',
            DB::raw('SUM(DatVtaDetalle.Cantidad) as Cantidad'),
            DB::raw('SUM(DatVtaDetalle.Importe) as Importe')
        )
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            ->leftJoin('DatCentroVenta', 'DatCentroVenta.IdDatCentroVenta', 'DatVentas.IdDatCentroVenta')
            ->leftJoin('CatCiudades', 'CatCiudades.IdCatCiudades', 'DatCentroVenta.IdCatCiudad')
            ->leftJoin('DatVtaDetalle', 'DatVtaDetalle.IdDatVentas', 'DatVentas.IdDatVentas')
            ->leftJoin('DatArticulos', 'DatArticulos.IdDatArticulos', 'DatVtaDetalle.IdDatArticulos')
            ->leftJoin('CatArticulos', 'CatArticulos.IdCatArticulos', 'DatArticulos.IdCatArticulos')
            ->whereNotNull('DatPagos.Status_Pedido')
            ->whereDate('DatPagos.Fecha_Pago', '>=', $dStart)
            ->whereDate('DatPagos.Fecha_Pago', '<=', $dEnd)
            ->when($idCentroVenta, function ($query) use ($idCentroVenta) {
                $query->where('DatVentas.IdDatCentroVenta', $idCentroVenta);
            })
            ->where('DatPagos.Status_Pedido', 5)
            ->groupBy('CatArticulos.Codigo', 'CatArticulos.DesCorta')
            ->get();

        $totalTickets = DatVentas::select(DB::raw('COUNT(DatPagos.IdDatPagos) as totalTickets'))
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            ->whereDate('DatPagos.Fecha_Pago', '>=', $dStart)
            ->whereDate('DatPagos.Fecha_Pago', '<=', $dEnd)
            ->when($idCentroVenta, function ($query) use ($idCentroVenta) {
                $query->where('DatVentas.IdDatCentroVenta', $idCentroVenta);
            })
            ->where('DatPagos.Status_Pedido', 5)
            ->value('totalTickets');

        $costo_Envio = DatVentas::select(DB::raw('SUM(DatPagos.Costo_Envio) as Costo_Envio'))
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            ->whereDate('DatPagos.Fecha_Pago', '>=', $dStart)
            ->whereDate('DatPagos.Fecha_Pago', '<=', $dEnd)
            ->when($idCentroVenta, function ($query) use ($idCentroVenta) {
                $query->where('DatVentas.IdDatCentroVenta', $idCentroVenta);
            })
            ->where('DatPagos.Status_Pedido', 5)
            ->value('Costo_Envio');

        $importe = DatVentas::select(DB::raw('SUM(DatPagos.Importe_Pago) as Importe_Pago'))
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            ->whereDate('DatPagos.Fecha_Pago', '>=', $dStart)
            ->whereDate('DatPagos.Fecha_Pago', '<=', $dEnd)
            ->when($idCentroVenta, function ($query) use ($idCentroVenta) {
                $query->where('DatVentas.IdDatCentroVenta', $idCentroVenta);
            })
            ->where('DatPagos.Status_Pedido', 5)
            ->value('Importe_Pago');

        // return $pedidosEntregados;

        $centrosVenta = DatCentroVenta::where('Status', 1)
            ->get();

        return view('reportes.concentrado', compact('pedidosEntregados', 'centrosVenta', 'idCentroVenta', 'fechaStart', 'fechaEnd', 'totalTickets', 'importe', 'costo_Envio'))->with('title', 'Concentrado de ventas');
    }

    // Pedidos entregados
    public function ventasPrint(Request $request)
    {
        $idCentroVenta = $request->idCentroVenta; // Id por parametro
        $fechaStart = $request->fechaStart;
        $fechaEnd = $request->fechaEnd;
        $dStart = $request->fechaStart;
        $dEnd = $request->fechaEnd;

        if (!$dStart) {
            $dStart = Carbon::now()->parse(date("2022-01-01"))->format('Y-m-d');
        } else {
            $dStart = Carbon::now()->parse($fechaStart)->format('Y-m-d');
        }

        if (!$dEnd) {
            $dEnd = Carbon::now()->parse(date(now()))->format('Y-m-d');
        } else {
            $dEnd = Carbon::now()->parse($dEnd)->format('Y-m-d');
        }

        $pedidosEntregados = DatVentas::select('DatVentas.IdCatUsers', 'DatVentas.IdDatVentas', 'DatPagos.Fecha_Pago as Fecha_Pedido', 'DatVentas.Importe', 'DatVentas.Comentario', 'CatCiudades.Descripcion as Ciudad')
            ->with(['Cliente'])
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            ->leftJoin('DatCentroVenta', 'DatCentroVenta.IdDatCentroVenta', 'DatVentas.IdDatCentroVenta')
            ->leftJoin('CatCiudades', 'CatCiudades.IdCatCiudades', 'DatCentroVenta.IdCatCiudad')
            ->whereNotNull('DatPagos.Status_Pedido')
            ->whereDate('DatPagos.Fecha_Pago', '>=', $dStart)
            ->whereDate('DatPagos.Fecha_Pago', '<=', $dEnd)
            ->when($idCentroVenta, function ($query) use ($idCentroVenta) {
                $query->where('DatVentas.IdDatCentroVenta', $idCentroVenta);
            })

            ->where('DatPagos.Status_Pedido', 5)
            ->orderBy('DatVentas.IdDatVentas', 'DESC')
            ->get();

        $total = 0;
        foreach ($pedidosEntregados as $pedido) {
            $total += $pedido->Importe;
        }

        $centrosVenta = DatCentroVenta::where('Status', 1)
            ->where('DatCentroVenta.IdDatCentroVenta', $idCentroVenta)
            ->get();

        $title = 'Reporte ventas';

        $nameReport = 'reporte ventas ' . ucfirst(Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) . '.pdf';

        $pdf = \PDF::loadView('reportes.pdf.entregados', compact('pedidosEntregados', 'centrosVenta', 'idCentroVenta', 'fechaStart', 'fechaEnd', 'title', 'total'));
        return $pdf->download($nameReport);

        return view('reportes.pdf.entregados', compact('pedidosEntregados', 'centrosVenta', 'idCentroVenta', 'fechaStart', 'fechaEnd', 'title', 'total'));
    }

    // Pedidos entregados
    public function concentradoPrint(Request $request)
    {
        $idCentroVenta = $request->idCentroVenta; // Id por parametro
        $fechaStart = $request->fechaStart;
        $fechaEnd = $request->fechaEnd;
        $dStart = $request->fechaStart;
        $dEnd = $request->fechaEnd;

        if (!$dStart) {
            $dStart = Carbon::now()->parse(date("2022-01-01"))->format('Y-m-d');
        } else {
            $dStart = Carbon::now()->parse($fechaStart)->format('Y-m-d');
        }

        if (!$dEnd) {
            $dEnd = Carbon::now()->parse(date(now()))->format('Y-m-d');
        } else {
            $dEnd = Carbon::now()->parse($dEnd)->format('Y-m-d');
        }

        // return $pedidosEntregados = DatVentas::select('DatVentas.IdCatUsers', 'DatVentas.IdDatVentas', 'DatPagos.Fecha_Pago as Fecha_Pedido', 'DatVentas.Importe', 'DatVentas.Comentario', 'CatCiudades.Descripcion as Ciudad')
        $pedidosEntregados = DatVentas::select(
            'CatArticulos.Codigo',
            'CatArticulos.DesCorta',
            DB::raw('SUM(DatVtaDetalle.Cantidad) as Cantidad'),
            DB::raw('SUM(DatVtaDetalle.Importe) as Importe')
        )
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            ->leftJoin('DatCentroVenta', 'DatCentroVenta.IdDatCentroVenta', 'DatVentas.IdDatCentroVenta')
            ->leftJoin('CatCiudades', 'CatCiudades.IdCatCiudades', 'DatCentroVenta.IdCatCiudad')
            ->leftJoin('DatVtaDetalle', 'DatVtaDetalle.IdDatVentas', 'DatVentas.IdDatVentas')
            ->leftJoin('DatArticulos', 'DatArticulos.IdDatArticulos', 'DatVtaDetalle.IdDatArticulos')
            ->leftJoin('CatArticulos', 'CatArticulos.IdCatArticulos', 'DatArticulos.IdCatArticulos')
            ->whereNotNull('DatPagos.Status_Pedido')
            ->whereDate('DatPagos.Fecha_Pago', '>=', $dStart)
            ->whereDate('DatPagos.Fecha_Pago', '<=', $dEnd)
            ->when($idCentroVenta, function ($query) use ($idCentroVenta) {
                $query->where('DatVentas.IdDatCentroVenta', $idCentroVenta);
            })
            ->where('DatPagos.Status_Pedido', 5)
            ->groupBy('CatArticulos.Codigo', 'CatArticulos.DesCorta')
            ->get();

        $totalTickets = DatVentas::select(DB::raw('COUNT(DatPagos.IdDatPagos) as totalTickets'))
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            ->whereDate('DatPagos.Fecha_Pago', '>=', $dStart)
            ->whereDate('DatPagos.Fecha_Pago', '<=', $dEnd)
            ->when($idCentroVenta, function ($query) use ($idCentroVenta) {
                $query->where('DatVentas.IdDatCentroVenta', $idCentroVenta);
            })
            ->where('DatPagos.Status_Pedido', 5)
            ->value('totalTickets');

        $costo_Envio = DatVentas::select(DB::raw('SUM(DatPagos.Costo_Envio) as Costo_Envio'))
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            ->whereDate('DatPagos.Fecha_Pago', '>=', $dStart)
            ->whereDate('DatPagos.Fecha_Pago', '<=', $dEnd)
            ->when($idCentroVenta, function ($query) use ($idCentroVenta) {
                $query->where('DatVentas.IdDatCentroVenta', $idCentroVenta);
            })
            ->where('DatPagos.Status_Pedido', 5)
            ->value('Costo_Envio');

        $importe = DatVentas::select(DB::raw('SUM(DatPagos.Importe_Pago) as Importe_Pago'))
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            ->whereDate('DatPagos.Fecha_Pago', '>=', $dStart)
            ->whereDate('DatPagos.Fecha_Pago', '<=', $dEnd)
            ->when($idCentroVenta, function ($query) use ($idCentroVenta) {
                $query->where('DatVentas.IdDatCentroVenta', $idCentroVenta);
            })
            ->where('DatPagos.Status_Pedido', 5)
            ->value('Importe_Pago');

        // return $pedidosEntregados;

        $centrosVenta = DatCentroVenta::where('Status', 1)
            ->where('DatCentroVenta.IdDatCentroVenta', $idCentroVenta)
            ->get();

        $title = 'Concentrado de ventas';

        $nameReport = 'reporte concentrado ventas ' . ucfirst(Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) . '.pdf';

        $pdf = \PDF::loadView('reportes.pdf.concentrado', compact('pedidosEntregados', 'centrosVenta', 'idCentroVenta', 'fechaStart', 'fechaEnd', 'totalTickets', 'importe', 'costo_Envio', 'title'));
        return $pdf->download($nameReport);

        return view('reportes.pdf.concentrado', compact('pedidosEntregados', 'centrosVenta', 'idCentroVenta', 'fechaStart', 'fechaEnd', 'totalTickets', 'importe', 'costo_Envio', 'title'));
    }

    public function corteDiario(Request $request)
    {
        // Aqui obtenemos el centro de centa auth()->user()->IdDatCentroVenta
        $ecommerce = Auth::user()
            ->leftJoin('DatCentroVenta', 'DatCentroVenta.IdDatCentroVenta', 'CatUsers.IdDatCentroVenta')
            ->where('CatUsers.IdDatCentroVenta', Auth::user()->IdDatCentroVenta)
            ->first()->Descripcion;

        $fecha = \Carbon\Carbon::now()->format('Y-m-d');

        if (isset($request->fecha)) {
            $fecha = $request->fecha;
        }

        // return $fecha;
        $pedidosSinFactura = DatClientesCloudCentroVenta::with(['Ventas' => function ($venta) use ($fecha) {
            $venta->select(
                'IdCatTipoPago',
                'CatArticulos.Codigo',
                'CatArticulos.Descripcion',
                'DatVtaDetalle.Precio',
                DB::raw('SUM(DatVtaDetalle.CantidadSecundario) as Cantidad'),
                DB::raw('SUM(DatVtaDetalle.Importe) as Importe'),
                DB::raw('SUM(DatPagos.Importe_Pago) as ImportePago'),
                DB::raw('SUM(DatPagos.Costo_Envio) as Costo_Envio')
            );

            $venta->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas');
            $venta->leftJoin('DatVtaDetalle', 'DatVtaDetalle.IdDatVentas', 'DatVentas.IdDatVentas');
            $venta->leftJoin('DatArticulos', 'DatArticulos.IdDatArticulos', 'DatVtaDetalle.IdDatArticulos');
            $venta->leftJoin('CatArticulos', 'CatArticulos.IdCatArticulos', 'DatArticulos.IdCatArticulos');

            $venta->groupBy('IdCatTipoPago', 'CatArticulos.Codigo', 'CatArticulos.Descripcion', 'DatVtaDetalle.Precio');
            $venta->whereNotNull('DatPagos.IdDatPagos');
            $venta->whereNull('DatPagos.SolicitudFE');
            $venta->whereDay('DatPagos.Fecha_Pago', substr($fecha, 8, 2));
            $venta->whereMonth('DatPagos.Fecha_Pago', substr($fecha, 5, 2));
            $venta->whereYear('DatPagos.Fecha_Pago', substr($fecha, 0, 4));
            $venta->where([['DatVentas.IdDatCentroVenta', Auth::user()->IdDatCentroVenta], ['DatPagos.Status_Pedido', 5]]);
        }, 'Total' => function ($venta) use ($fecha) {
            $venta->select(
                'IdCatTipoPago',
                DB::raw('SUM(DatPagos.Importe_Pago) as ImportePago'),
                DB::raw('SUM(DatPagos.Costo_Envio) as Costo_Envio')
            );

            $venta->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas');

            $venta->groupBy('IdCatTipoPago');
            $venta->whereNotNull('DatPagos.IdDatPagos');
            $venta->whereNull('DatPagos.SolicitudFE');
            $venta->whereDay('DatPagos.Fecha_Pago', substr($fecha, 8, 2));
            $venta->whereMonth('DatPagos.Fecha_Pago', substr($fecha, 5, 2));
            $venta->whereYear('DatPagos.Fecha_Pago', substr($fecha, 0, 4));
            $venta->where([['DatVentas.IdDatCentroVenta', Auth::user()->IdDatCentroVenta], ['DatPagos.Status_Pedido', 5]]);
        }])
            ->where('DatClientesCloudCentroVenta.IdDatCentroVenta', Auth::user()->IdDatCentroVenta)
            ->orderBy('DatClientesCloudCentroVenta.ID_CLIENTE')
            ->get();

        // return $pedidosSinFactura;
        $pedidosConFactura = DatSolicitudFactura::with(['Ventas' => function ($venta) use ($fecha) {
            $venta->select(
                'DatVentas.IdDatVentas',
                'CatArticulos.Codigo',
                'CatArticulos.Descripcion',
                'DatVtaDetalle.Precio',
                'DatPagos.SolicitudFE',
                DB::raw('SUM(DatVtaDetalle.CantidadSecundario) as Cantidad'),
                DB::raw('SUM(DatVtaDetalle.Importe) as Importe'),
                DB::raw('SUM(DatPagos.Importe_Pago) as ImportePago')
            );

            $venta->leftJoin('DatVtaDetalle', 'DatVtaDetalle.IdDatVentas', 'DatVentas.IdDatVentas');
            $venta->leftJoin('DatArticulos', 'DatArticulos.IdDatArticulos', 'DatVtaDetalle.IdDatArticulos');
            $venta->leftJoin('CatArticulos', 'CatArticulos.IdCatArticulos', 'DatArticulos.IdCatArticulos');
            $venta->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas');
            $venta->groupBy('DatVentas.IdDatVentas', 'CatArticulos.Codigo', 'CatArticulos.Descripcion', 'DatVtaDetalle.Precio', 'DatPagos.SolicitudFE');
            $venta->whereDay('DatPagos.Fecha_Pago', substr($fecha, 8, 2));
            $venta->whereMonth('DatPagos.Fecha_Pago', substr($fecha, 5, 2));
            $venta->whereYear('DatPagos.Fecha_Pago', substr($fecha, 0, 4));
            $venta->where('DatPagos.Status_Pedido', 5);
        }])
            ->leftJoin('DatVentas', 'DatVentas.IdDatVentas', 'DatSolicitudFactura.IdDatVentas')
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            ->get();

        // return $pedidosConFactura;
        $pedidos = [];
        $data = [];
        foreach ($pedidosSinFactura as $pedido) {
            if (count($pedido->Ventas) > 0) {
                // $source = DB::select("select [dbo].[FNgetSourceTransaction] (" . $pedido->IdCatTipopago . ", '" . $fecha . "' ) as Source_Transaction_Number");
                // $datadetail = ['cliente' => $pedido, 'source' => 'ECO_383659'];
                $source = DB::select("select [dbo].[FNgetSourceTransaction] (" . $pedido->IdCatTipopago . ", " . $pedido->IdDatCentroVenta . ", '" . $fecha . "' ) as Source_Transaction_Number");
                $datadetail = ['cliente' => $pedido, 'source' => $source[0]->Source_Transaction_Number];
                array_push($data, $datadetail);
            }
        }

        array_push($pedidos, $data);
        $data = [];
        foreach ($pedidosConFactura as $pedido) {
            if (count($pedido->Ventas) > 0) {
                // return $pedido['Ventas'][0]->IdDatVentas;
                // $source = DB::select("select [dbo].[FNgetSourceTransaction] (" . $pedido->IdCatTipopago . ", '" . $fecha . "' ) as Source_Transaction_Number");
                // $datadetail = ['cliente' => $pedido, 'source' => 'ECO_383659'];
                $source = DB::select("select [dbo].[FNgetSourceTransactionConFactura] (" . $pedido['Ventas'][0]->IdDatVentas . ") as Source_Transaction_Number");
                // return $source;
                $datadetail = ['cliente' => $pedido, 'source' => $source[0]->Source_Transaction_Number];
                array_push($data, $datadetail);
            }
        }
        array_push($pedidos, $data);

        // Calculamos el descuento stripe
        $pagos = DatPagos::select('DatPagos.Importe_Pago')
            ->leftjoin('DatVentas', 'DatVentas.IdDatVentas', 'DatPagos.IdDatVentas')
            ->whereDay('DatPagos.Fecha_Pago', substr($fecha, 8, 2))
            ->whereMonth('DatPagos.Fecha_Pago', substr($fecha, 5, 2))
            ->whereYear('DatPagos.Fecha_Pago', substr($fecha, 0, 4))
            ->where('DatPagos.Status_Pedido', 5)
            ->where('DatVentas.IdDatCentroVenta', Auth::user()->IdDatCentroVenta)
            ->get();

        $subtotalGeneral = 0;
        $totalGeneral = 0;
        foreach ($pagos as $pago) {
            $subtotalGeneral += $pago->Importe_Pago;
            $totalGeneral += $pago->Importe_Pago - (($pago->Importe_Pago * .036) + 3) * 1.16;
        }

        $ciudades = Ciudad::get();

        // Este es el bueno de source
        // return $data->subTotal;
        // return $data[1]['cliente'];
        // return $data[1]['cliente']->Ventas;
        // return $data[count($data)-1]['total'];

        $title = 'Corte diario';

        $nameReport = 'corte diario ' . ucfirst(Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) . '.pdf';

        $pdf = \PDF::loadView('reportes.pdf.cortediario', compact('pedidos', 'totalGeneral', 'subtotalGeneral', 'ciudades', 'title', 'fecha', 'ecommerce'));
        return $pdf->download($nameReport);

        return view('reportes.pdf.cortediario', compact('pedidos', 'totales', 'ciudades'))->with(['title' => 'Corte diario', 'textSecondary' => $ecommerce]);
    }

    // ventasfallidas
    public function ventasFallidas(Request $request)
    {
        $idCentroVenta = $request->idCentroVenta; // Id por parametro
        $statusVente = $request->statusVente;
        $fechaStart = $request->fechaStart;
        $fechaEnd = $request->fechaEnd;
        $dStart = $request->fechaStart;
        $dEnd = $request->fechaEnd;

        if (!$dStart) {
            $dStart = Carbon::now()->parse(date("2022-01-01"))->format('Y-m-d');
        } else {
            $dStart = Carbon::now()->parse($fechaStart)->format('Y-m-d');
        }

        if (!$dEnd) {
            $dEnd = Carbon::now()->parse(date(now()))->format('Y-m-d');
        } else {
            $dEnd = Carbon::now()->parse($dEnd)->format('Y-m-d');
        }

        $pedidosEntregados = DatVentas::select(
            'DatVentas.IdDatVentas',
            'DatPagos.IdDatPagos',
            'DatVentas.IdCatUsers',
            'DatVentas.Fecha_Pedido',
            'DatVentas.Importe',
            'DatVentas.Comentario',
            'CatCiudades.Descripcion as Ciudad',
            'DatPagos.Status_Pedido',
        )
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            ->leftJoin('DatCentroVenta', 'DatCentroVenta.IdDatCentroVenta', 'DatVentas.IdDatCentroVenta')
            ->leftJoin('CatCiudades', 'CatCiudades.IdCatCiudades', 'DatCentroVenta.IdCatCiudad')
            ->when($idCentroVenta, function ($query) use ($idCentroVenta) {
                $query->where('DatVentas.IdDatCentroVenta', $idCentroVenta);
            })
            ->when($statusVente, function ($query) use ($statusVente) {
                if ($statusVente == 1) {
                    $query->where('DatPagos.Status_Pedido', 5);
                }

                if ($statusVente == 2) {
                    $query->where('DatPagos.Status_Pedido', 0);
                }

                if ($statusVente == 3) {
                    $query->whereNull('DatPagos.Status_Pedido');
                }
            })
            ->whereDate('DatVentas.Fecha_Pedido', '>=', $dStart)
            ->whereDate('DatVentas.Fecha_Pedido', '<=', $dEnd)
            ->orderBy('DatVentas.IdDatVentas', 'DESC')
            ->paginate(10);

        // return $pedidosEntregados;

        $centrosVenta = DatCentroVenta::where('Status', 1)
            ->get();

        return view('reportes.ventasfallidas', compact('pedidosEntregados', 'centrosVenta', 'idCentroVenta', 'fechaStart', 'fechaEnd', 'statusVente'))->with('title', 'Reporte ventas fallidas');
    }

    // ventasfallidas
    public function ventasFallidasPrint(Request $request)
    {
        $idCentroVenta = $request->idCentroVenta; // Id por parametro
        $statusVente = $request->statusVente;
        $fechaStart = $request->fechaStart;
        $fechaEnd = $request->fechaEnd;
        $dStart = $request->fechaStart;
        $dEnd = $request->fechaEnd;

        if (!$dStart) {
            $dStart = Carbon::now()->parse(date("2022-01-01"))->format('Y-m-d');
        } else {
            $dStart = Carbon::now()->parse($fechaStart)->format('Y-m-d');
        }

        if (!$dEnd) {
            $dEnd = Carbon::now()->parse(date(now()))->format('Y-m-d');
        } else {
            $dEnd = Carbon::now()->parse($dEnd)->format('Y-m-d');
        }

        $pedidosEntregados = DatVentas::select(
            'DatVentas.IdDatVentas',
            'DatPagos.IdDatPagos',
            'DatVentas.IdCatUsers',
            'DatVentas.Fecha_Pedido',
            'DatVentas.Importe',
            'DatVentas.Comentario',
            'CatCiudades.Descripcion as Ciudad',
            'DatPagos.Status_Pedido',
        )
            ->leftJoin('DatPagos', 'DatPagos.IdDatVentas', 'DatVentas.IdDatVentas')
            ->leftJoin('DatCentroVenta', 'DatCentroVenta.IdDatCentroVenta', 'DatVentas.IdDatCentroVenta')
            ->leftJoin('CatCiudades', 'CatCiudades.IdCatCiudades', 'DatCentroVenta.IdCatCiudad')
            ->when($idCentroVenta, function ($query) use ($idCentroVenta) {
                $query->where('DatVentas.IdDatCentroVenta', $idCentroVenta);
            })
            ->when($statusVente, function ($query) use ($statusVente) {
                if ($statusVente == 1) {
                    $query->where('DatPagos.Status_Pedido', 5);
                }

                if ($statusVente == 2) {
                    $query->where('DatPagos.Status_Pedido', 0);
                }

                if ($statusVente == 3) {
                    $query->whereNull('DatPagos.Status_Pedido');
                }
            })
            ->whereDate('DatVentas.Fecha_Pedido', '>=', $dStart)
            ->whereDate('DatVentas.Fecha_Pedido', '<=', $dEnd)
            ->orderBy('DatVentas.IdDatVentas', 'DESC')
            ->get();

        $centrosVenta = DatCentroVenta::where('Status', 1)
            ->where('DatCentroVenta.IdDatCentroVenta', $idCentroVenta)
            ->get();

        $completados = 0;
        $cancelados = 0;
        $fallidos = 0;

        foreach ($pedidosEntregados as $pedido) {
            if ($pedido->IdDatPagos == null) {
                $fallidos++;
            } else if ($pedido->IdDatPagos != null && $pedido->Status_Pedido == 0) {
                $cancelados++;
            } else {
                $completados++;
            }
        }

        $title = 'Ventas fallidas';
        $nameReport = 'reporte de ventas fallidas ' . ucfirst(Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) . '.pdf';

        $pdf = \PDF::loadView('reportes.pdf.ventasfallidas', compact('pedidosEntregados', 'centrosVenta', 'idCentroVenta', 'fechaStart', 'fechaEnd', 'statusVente', 'completados', 'cancelados', 'fallidos', 'title'));
        return $pdf->download($nameReport);

        return view('reportes.pdf.ventasfallidas', compact('pedidosEntregados', 'centrosVenta', 'idCentroVenta', 'fechaStart', 'fechaEnd', 'statusVente', 'title'));
    }
}
