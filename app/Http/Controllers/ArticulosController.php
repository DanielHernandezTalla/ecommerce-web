<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticulosRequest;
use App\Models\CatArticulos;
use App\Models\CatArticulosImagenes;
use App\Models\CatCategorias;
use App\Models\CatPaquetes;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ArticulosController extends Controller
{
    public function index()
    {
        $articulos = CatArticulos::with('Categoria', 'ImagenArticulo')->get();
        $catCategorias = CatCategorias::where('Status', 1)->get();

        // return $articulos;
        return view('articulos.index', compact('articulos', 'catCategorias'))->with('title', 'Catálogo de artículos');
    }

    public function create()
    {
        $categorias = CatCategorias::where('Status', 1)->get();
        $paquetes = CatPaquetes::where('Status', 1)->get();

        return view('articulos.create', compact('categorias', 'paquetes'))->with('title', 'Registrar articulo');
    }

    public function edit($id)
    {
        $articulo = CatArticulos::with('ImagenArticulo')->find($id);
        // return $articulo;
        $categorias = CatCategorias::where('Status', 1)->get();
        $paquetes = CatPaquetes::where('Status', 1)->get();

        return view('articulos.edit', compact('articulo', 'categorias', 'paquetes'))->with('title', 'Editar articulo');
    }

    public function store(ArticulosRequest $request)
    {
        try {
            // Agregar articulo a la base de datos
            $articulo = new CatArticulos();

            $articulo->Codigo = $request->validated()['Codigo'];
            $articulo->Descripcion = $request->validated()['Descripcion'];
            $articulo->DesCorta = $request->validated()['DesCorta'];
            $articulo->DesCorta1 = $request->validated()['DesCorta1'];
            $articulo->DescIva = $request->validated()['DescIva'];
            $articulo->Iva = $request->validated()['Iva'];
            $articulo->IdCatCategoria = $request->validated()['IdCatCategoria'];
            $articulo->IdCatPaquete = $request->validated()['IdCatPaquete'];
            $articulo->Unidad = $request->validated()['Unidad'];
            $articulo->CantPesoProm = $request->validated()['CantPesoProm'];
            $articulo->DescripcionGeneral = $request->validated()['DescripcionGeneral'];
            $articulo->Status = isset($request->validated()['Status']);

            $articulo = $articulo->save();

            // Obtenermos el ultimo articulo
            $articulo = CatArticulos::orderby('IdCatArticulos', 'desc')->first();

            // Obtenemos la imagen que mando el usuario
            $image = $request->file('IdCatArticuloImagenes');

            // Si el usuario envio imagen, esta sera movida al directorio del proyecto y guardada en la base de datos
            if ($image !== null) {
                $nombreArchivo = Carbon::now()->getPreciseTimestamp(3) . '.' . $image->getClientOriginalExtension();
                $image->move('images/products', $nombreArchivo);

                $CatArticulosImagenes = new CatArticulosImagenes;
                $CatArticulosImagenes->IdCatArticulos = $articulo->IdCatArticulos;
                $CatArticulosImagenes->Imagen = $nombreArchivo;
                $CatArticulosImagenes->Fecha = date("d-m-Y H:i:s");
                $CatArticulosImagenes->Status = 1;
                $CatArticulosImagenes->save();
            }

            return redirect()
                ->route('articulos.index')
                ->withSuccess("Articulo creado correctamente");
        } catch (\Exception $error) {
            return redirect()
                ->route('articulos.index')
                ->withErrors("Error al agregar el articulo");
        }
    }

    public function update($id, ArticulosRequest $request)
    {
        try {
            // Actualizamos el articulo con ese id
            CatArticulos::where('IdCatArticulos', $id)
                ->update([
                    'Codigo' => $request->validated()['Codigo'],
                    'Descripcion' => $request->validated()['Descripcion'],
                    'DesCorta' => $request->validated()['DesCorta'],
                    'DesCorta1' => $request->validated()['DesCorta1'],
                    'DescIva' => $request->validated()['DescIva'],
                    'Iva' => $request->validated()['Iva'],
                    'IdCatCategoria' => $request->validated()['IdCatCategoria'],
                    'IdCatPaquete' => $request->validated()['IdCatPaquete'],
                    'Unidad' => $request->validated()['Unidad'],
                    'CantPesoProm' => $request->validated()['CantPesoProm'],
                    'DescripcionGeneral' => $request->validated()['DescripcionGeneral'],
                    'Status' => isset($request->validated()['Status']),
                ]);

            // Obtenemos la imagen que mando el usuario
            $image = $request->file('IdCatArticuloImagenes');

            // Si el usuario envio imagen
            if ($image !== null) {
                // Esta sera movida al directorio del proyecto
                $nombreArchivo = Carbon::now()->getPreciseTimestamp(3) . '.' . $image->getClientOriginalExtension();
                $image->move('images/products', $nombreArchivo);

                // Se busca otra imagen en la base de datos
                $image = DB::table('CatArticuloImagenes')->where('IdCatArticulos', $id)->first();

                // Si ya existe una imagen
                if ($image) {
                    // Se actualiza en la base de datos
                    CatArticulosImagenes::where('IdCatArticulos', $id)
                        ->update([
                            'Imagen' => $nombreArchivo,
                            'Fecha' => date("d-m-Y H:i:s"),
                        ]);

                    // return __DIR__ . "../../../../public/images/products/" . $image->Imagen;
                    // Se elimina el la imagen fisicamente
                    unlink(__DIR__ . "../../../../public/images/products/" . $image->Imagen);

                    return redirect()
                        ->route('articulos.index')
                        ->withSuccess("Articulo actualizado correctamente");
                }

                // Si no existe una imagen, esta sera creada
                $CatArticulosImagenes = new CatArticulosImagenes;
                $CatArticulosImagenes->IdCatArticulos = $id;
                $CatArticulosImagenes->Imagen = $nombreArchivo;
                $CatArticulosImagenes->Fecha = date("d-m-Y H:i:s");
                $CatArticulosImagenes->Status = 1;
                $CatArticulosImagenes->save();

            }

            return redirect()
                ->route('articulos.index')
                ->withSuccess("Articulo actualizado correctamente");
        } catch (\Exception $error) {
            return redirect()
                ->route('articulos.index')
                ->withErrors("Error al actualizar el articulo");
        }
    }

    public function destroy($id)
    {
        try {
            // Buscamos las imagenes y las eliminamos
            $images = CatArticulosImagenes::where('IdCatArticulos', $id)->get();

            foreach ($images as $image) {
                $image->delete();
                if (file_exists(__DIR__ . "../../../../public/images/products/" . $image->Imagen)) {
                    unlink(__DIR__ . "../../../../public/images/products/" . $image->Imagen);
                }
            }

            // Obtenermos y eliminamos el articulo
            $articulo = CatArticulos::find($id);
            $articulo->delete();

            return redirect()
                ->route('articulos.index')
                ->withSuccess("Articulo elimindo correctamente");
        } catch (\Throwable $th) {
            return redirect()
                ->route('articulos.index')
                ->withErrors($th);
        }
    }

}
