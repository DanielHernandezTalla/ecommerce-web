<?php

namespace App\Http\Controllers;

use App\Models\CatClientes;
use App\Models\Ciudad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Twilio\Rest\Client;

class MensajesesController extends Controller
{
    // Pedidos entregados
    public function index(Request $request)
    {
        $IdCatCiudades = $request->IdCatCiudades; // Id por parametro
        $primerventa = $request->primerventa; // Dato por parametro
        $numero = $request->numero; // Dato por parametro

        if ($primerventa) {
            $usuariosCompra = DB::select("SELECT DISTINCT C.IdUser
            FROM CatClientes AS C
            LEFT JOIN DatVentas AS V ON C.IdUser = V.IdCatUsers
            LEFT JOIN DatPagos AS P ON P.IdDatVentas = V.IdDatVentas
            WHERE P.Importe_Pago IS NOT NULL
            GROUP BY C.IdUser, V.IdDatVentas");

            $UC = [];
            foreach ($usuariosCompra as $usecom) {
                array_push($UC, $usecom->IdUser);
            }

            $clientes = DB::table("CatClientes as c")
                ->leftJoin("DatVentas as v", function ($join) {
                    $join->on("c.IdUser", "=", "v.IdCatUsers");
                })
                ->leftJoin("DatPagos as p", function ($join) {
                    $join->on("p.IdDatVentas", "=", "v.IdDatVentas");
                })
                ->select("c.IdUser", "c.Nombre", "c.Ciudad", "c.Estado", "c.Telefono")
                ->whereNull("p.Importe_Pago")
                ->whereNotIn("c.IdUser", $UC)
                ->where('IdCatCiudad', 'LIKE', $IdCatCiudades)
                ->whereRaw(DB::raw('LEN(Telefono) = 10'))
                ->whereRaw(DB::raw('ISNUMERIC(Telefono) = 1'))
                ->where('Telefono', 'LIKE', '%' . $numero . '%')
                ->groupBy("c.IdUser", "c.Nombre", "c.Ciudad", "c.Estado", "c.Telefono")
                ->paginate(10);
        } else {
            $clientes = CatClientes::where('IdCatCiudad', 'LIKE', $IdCatCiudades)
                ->whereRaw(DB::raw('LEN(Telefono) = 10'))
                ->whereRaw(DB::raw('ISNUMERIC(Telefono) = 1'))
                ->where('Telefono', 'LIKE', '%' . $numero . '%')
                ->paginate(10);
        }

        // return $clientes;

        $ciudades = Ciudad::where('Status', 1)
            ->get();

        return view('mensajes.index', compact('clientes', 'ciudades', 'IdCatCiudades', 'primerventa', 'numero'))->with('title', 'Lista de clientes');
    }

    public function send(Request $request)
    {
        $IdCatCiudades = $request->IdCatCiudades; // Id por parametro
        $primerventa = $request->primerventa; // Dato por parametro
        $numero = $request->numero; // Dato por parametro
        $mensaje = $request->mensaje; // Dato por parametro

        // Sacamos los numeros de telefono a enviar el mensaje
        if ($primerventa) {
            $usuariosCompra = DB::select("SELECT DISTINCT C.IdUser
            FROM CatClientes AS C
            LEFT JOIN DatVentas AS V ON C.IdUser = V.IdCatUsers
            LEFT JOIN DatPagos AS P ON P.IdDatVentas = V.IdDatVentas
            WHERE P.Importe_Pago IS NOT NULL
            GROUP BY C.IdUser, V.IdDatVentas");

            $UC = [];
            foreach ($usuariosCompra as $usecom) {
                array_push($UC, $usecom->IdUser);
            }

            $clientes = DB::table("CatClientes as c")
                ->leftJoin("DatVentas as v", function ($join) {
                    $join->on("c.IdUser", "=", "v.IdCatUsers");
                })
                ->leftJoin("DatPagos as p", function ($join) {
                    $join->on("p.IdDatVentas", "=", "v.IdDatVentas");
                })
                ->select("c.IdCatClientes", "c.Telefono")
                ->whereNull("p.Importe_Pago")
                ->whereNotIn("c.IdUser", $UC)
                ->where('IdCatCiudad', 'LIKE', $IdCatCiudades)
                ->whereRaw(DB::raw('LEN(Telefono) = 10'))
                ->whereRaw(DB::raw('ISNUMERIC(Telefono) = 1'))
                ->where('Telefono', 'LIKE', '%' . $numero . '%')
                ->groupBy("c.IdCatClientes", "c.Telefono")
                ->get();
        } else {
            $clientes = CatClientes::select("CatClientes.IdCatClientes", "CatClientes.Telefono")
                ->where('IdCatCiudad', 'LIKE', $IdCatCiudades)
                ->whereRaw(DB::raw('LEN(Telefono) = 10'))
                ->whereRaw(DB::raw('ISNUMERIC(Telefono) = 1'))
                ->where('Telefono', 'LIKE', '%' . $numero . '%')
                ->groupBy("CatClientes.IdCatClientes", 'CatClientes.Telefono')
                ->get();
        }

        // Configuramos twilio para enviar los mensajes
        $account_sid = config('services.twilio')['account_sid'];
        $auth_token = config('services.twilio')['auth_token'];
        $phone = config('services.twilio')['phone'];

        $twilio = new Client($account_sid, $auth_token);

        // Aqui enviamos los mensajes
        foreach ($clientes as $cliente) {
            if (isset($cliente->Telefono) and preg_match("/^[0-9]{10}$/", $cliente->Telefono)) {
                try {
                    $twilio->messages->create('+52' . $cliente->Telefono, [
                        'from' => $phone,
                        'body' => $mensaje,
                    ]);
                } catch (\Throwable $th) {
                }
            }
        }

        return redirect()
            ->route('mensajes.index')
            ->withSuccess("Mensajes enviados correctamente");
    }
}
