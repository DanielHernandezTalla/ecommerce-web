@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    {{-- Formulario  --}}
    <div class="card p-4" style="max-width: 700px; margin: auto;">

        <form method="POST" action="{{ route('usuarios.update', ['usuario' => $user]) }}">

            @csrf
            @method('PUT')

            <div class="mb-3">
                <label class="form-label text-secondary fw-semibold">Nombre de usuario</label>
                <input type="text" name="username" class="form-control" value="{{ old('username') ?? $user->username }}"
                    placeholder="Daniel Hernandez Talla" autofocus>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label class="form-label text-secondary fw-semibold">Tipo de usuario</label>
                    <select name="IdCatTipoUser" class="form-select">
                        <option value="0">Elige un tipo de usuario</option>
                        @foreach ($typeUsers as $type)
                            @if ($type->IdCatTipoUser == $user->IdCatTipoUser)
                                <option value="{{ $type->IdCatTipoUser }}" selected>{{ $type->Descripcion }}</option>
                            @else
                                <option value="{{ $type->IdCatTipoUser }}">{{ $type->Descripcion }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6  mb-3">
                    <label class="form-label text-secondary fw-semibold">Centro de venta</label>
                    <select name="IdDatCentroVenta" class="form-select">
                        <option value="0" selected>Elige un centro de venta</option>
                        @foreach ($centroVenta as $centro)
                            @if ($centro->IdDatCentroVenta == $user->IdDatCentroVenta)
                                <option value="{{ $centro->IdDatCentroVenta }}" selected>{{ $centro->Descripcion }}</option>
                            @else
                                <option value="{{ $centro->IdDatCentroVenta }}">{{ $centro->Descripcion }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="" class="form-label text-secondary fw-semibold">Contraseña</label>
                    <input type="password" name="password" class="form-control" placeholder="**********">
                </div>
                <div class="col-md-6 mb-3">
                    <label for="" class="form-label text-secondary fw-semibold">Confirmar contraseña</label>
                    <input type="password" name="password_confirmation" class="form-control" placeholder="**********">
                </div>
            </div>

            <div>
                <label class="switch">
                    @if (old('Status') || $user->Status)
                        <input type="checkbox" name="Status" checked>
                    @else
                        <input type="checkbox" name="Status">
                    @endif
                    <span class="slider round"></span>
                </label>
                <label class="form-check-label fw-semibold text-secondary">Activo</label>
            </div>

            <div class="row mt-4">
                <div class="col-md-6 mt-3">
                    <a class="form-control btn submit-orange-outline" href="{{ route('usuarios') }}">Cancelar</a>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="submit" class="form-control btn submit-orange">
                </div>
            </div>

        </form>
    </div>
@endsection
