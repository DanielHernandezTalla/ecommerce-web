@extends('layouts.app')

@section('content')
    {{-- Title --}}
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    {{-- Alers --}}
    <div class="d-flex justify-content-center mb-3">
        <div class="col-auto">
            @include('Alertas.Alertas')
        </div>
    </div>

    {{-- Formulario para agregar productos --}}
    <div class="container-title pb-4 d-flex justify-content-between align-items-end">
        <form action="{{ route('recepcion.manual') }}" method="POST">
            @csrf
            <div class="d-flex align-items-end">
                <div class="col-auto">
                    <label>Buscar Articulo</label>
                    <input class="input-article form-control" list="articulos" name="codigo" id="codigo"
                        placeholder="Buscar articulo" autocomplete="off" onkeypress="return event.keyCode != 13;" required
                        autofocus>
                    <datalist id="articulos">
                        @foreach ($articulos as $articulo)
                            <option class="prom{{ $articulo->Codigo }}" value="{{ $articulo->Codigo }}"
                                data-prom="{{ $articulo->CantPesoProm }}">
                                {{ $articulo->Descripcion }}
                            </option>
                        @endforeach
                    </datalist>
                </div>
                <div class="col-auto ms-4">
                    <label>Cantidad piezas</label>
                    <input style="width: 100px" class="input-cantidad-piezas form-control form-control" min="0"
                        max="1000" step="any" type="number" placeholder="Piezas"
                        onkeypress="return event.keyCode != 13;" required>
                </div>
                <div class="col-auto ms-4">
                    <label>Peso promedio</label>
                    <label style="line-height: 34px;" class="input-cantidad-prom d-block text-center">0</label>
                </div>
                <div class="col-auto ms-4">
                    <label>Cantidad kilos</label>
                    <input style="width: 100px" class="input-cantidad-kilos form-control" min="0" max="1000"
                        step="any" type="number" name="cantidad" id="cantidad" placeholder="Kilos"
                        onkeypress="return event.keyCode != 13;" required>
                </div>
                <div class="col-auto ms-4">
                    <button class="input-submit btn btn-orange disabled"> Agregar </button>
                </div>
            </div>
        </form>

        @if ($productos->count() > 0)
            <form action="{{ route('recepcion.productos') }}" method="POST">
                @csrf
                <button class="btn btn-orange">Recepcionar producto </button>
            </form>
        @endif
    </div>

    {{-- Tabla  --}}
    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Codigo</th>
                    <th>Descripcion</th>
                    <th>Cantidad piezas</th>
                    <th>Peso promedio</th>
                    <th>Cantidad kilos</th>
                    <th class="rounded-end"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($productos as $producto)
                    <tr>
                        <td>{{ $producto->Codigo }}</td>
                        <td>{{ $producto->Descripcion }}</td>
                        <td>{{ $producto->Cantidad / $producto->CantPesoProm }} </td>
                        <td>{{ number_format($producto->CantPesoProm, 3) }} KG</td>
                        <td>{{ number_format($producto->Cantidad, 3) }} KG</td>
                        <td>
                            <form action="EliminarProductoTmp/{{ $producto->IdCapRecepcionManual }}" method="POST">
                                @csrf
                                <button class="btn">
                                    <i style="color: red; font-size: 22px;" class="fa fa-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    {{-- Scripts para validar que los kilos son piezas exactas --}}
    <script>
        document.addEventListener("keyup", (e) => {
            if (e.target.matches('.input-cantidad-piezas')) setKilos(e);
            if (e.target.matches('.input-cantidad-kilos')) setPiezas(e);
        });

        document.addEventListener("change", (e) => {
            if (e.target.matches('.input-cantidad-piezas')) setKilos(e);
            if (e.target.matches('.input-cantidad-kilos')) setPiezas(e);

            if (e.target.matches('.input-article')) {
                let article = document.querySelector('.input-article').value;
                if (!article) return;

                let pesoProm = document.querySelector(`.prom${article}`).getAttribute('data-prom');
                document.querySelector('.input-cantidad-prom').textContent = parseFloat(pesoProm) + ' KG';
            }
        });

        function setKilos(e) {
            let article = document.querySelector('.input-article').value;

            if (!article) return;

            let value = e.target.value;

            if (Number.isInteger(parseFloat(value)))
                document.querySelector('.input-submit').classList.remove('disabled');
            else document.querySelector('.input-submit').classList.add('disabled');

            let pesoProm = document.querySelector(`.prom${article}`).getAttribute('data-prom');

            document.querySelector('.input-cantidad-kilos').value = (value * pesoProm).toFixed(3);
            document.querySelector('.input-cantidad-prom').textContent = parseFloat(pesoProm) + ' KG';

            if (document.querySelector('.input-cantidad-kilos').value == 0)
                document.querySelector('.input-submit').classList.add('disabled');
        }

        function setPiezas(e) {
            let article = document.querySelector('.input-article').value;

            if (!article) return;

            let value = e.target.value;
            let pesoProm = document.querySelector(`.prom${article}`).getAttribute('data-prom');

            if (Number.isInteger(parseFloat((value / pesoProm).toFixed(3))))
                document.querySelector('.input-submit').classList.remove('disabled');
            else document.querySelector('.input-submit').classList.add('disabled');

            document.querySelector('.input-cantidad-piezas').value = (value / pesoProm).toFixed(3);
            document.querySelector('.input-cantidad-prom').textContent = parseFloat(pesoProm) + ' KG';

            if (document.querySelector('.input-cantidad-kilos').value == 0)
                document.querySelector('.input-submit').classList.add('disabled');
        }
    </script>
@endsection
