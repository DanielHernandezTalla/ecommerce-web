<!DOCTYPE html>
<html lang="en">

<head>
    <!--<base target="_parent" />-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/fontawesome.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Agregar Producto Manual</title>
    <style>
        button:active {
            transform: scale(1.2);
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        <table class="table table-responsive table-striped">
            <thead class="table-dark">
                <tr>
                    <th>Código</th>
                    <th>Articulo</th>
                    <th>Cantidad</th>
                    <th>Seleccionar</th>
                </tr>
            </thead>
            <tbody>
                @if ($articuloPendiente == 0)
                <tr>
                    <th colspan="4"><i style="color: red" class="fa fa-exclamation-triangle"></i> Articulo Pendiente Por Recepcionar, Vaya a Recepciones Pendientes!</th>
                </tr>
                @else
                    @if ($articulos->count() == 0)
                        <tr>
                            <td colspan="4">No se Encontro el Articulo!</td>
                        </tr>
                    @else
                        @foreach ($articulos as $articulo)
                            <form action="/CapturaManualTmp" target="ifrGuardarTmp">
                                <input type="hidden" name="codArticulo" value="{{ $articulo->CodArticulo }}">
                                <tr>
                                    <td>{{ $articulo->CodArticulo }}</td>
                                    <td>{{ $articulo->NomArticulo }}</td>
                                    <td>
                                        <input type="number" min="0.1" step="any" class="form-control form-control-sm"
                                            name="cantArticulo" id="cantArticulo" required>
                                    </td>
                                    <td>
                                        <button class="btn btn-sm">
                                            <span class="material-icons">add_circle</span>
                                        </button>
                                    </td>
                                </tr>
                            </form>
                        @endforeach
                    @endif
                @endif
            </tbody>
        </table>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>

</html>
