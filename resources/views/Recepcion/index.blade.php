@extends('layouts.app')

@section('content')

    <div class="container-title pb-4">
        @include('components.title')
        <a class="btn btn-orange rounded-pill" href="{{ route('recepcion.manual.index') }}">Captura producto manual</a>
    </div>

    <div class="container">
        @include('Alertas.Alertas')
    </div>

    {{-- Tabla  --}}
    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Id</th>
                    <th>Origen</th>
                    <th>Llegada</th>
                    <th>Status</th>
                    <th class="rounded-end"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($recepcion as $recepcionCV)
                    <tr>
                        <td>{{ $recepcionCV->IdCapRecepcion }}</td>
                        <td>{{ $recepcionCV->PackingList }}</td>
                        <td>{{ strftime('%d %B %Y, %H:%M', strtotime($recepcionCV->FechaLlegada)) }}</td>
                        <td>{{ $recepcionCV->IdStatusRecepcion }}</td>
                        <td class="text-end">
                            <form class="d-inline" action="{{ route('recepcion.index') }}">
                                <input type="hidden" name="idRecepcion" value="{{ $recepcionCV->IdCapRecepcion }}">
                                <button class="btn text-warning" data-bs-toggle="mensaje" title="Recepcionar">
                                    <i class="bi bi-file-earmark-text"></i>
                                </button>
                            </form>
                            <button type="button" class="btn text-danger" data-bs-toggle="modal"
                                data-bs-target="#ModalCancelarRecepcion{{ $recepcionCV->IdCapRecepcion }}">
                                <i class="bi bi-trash3-fill"></i>
                            </button>
                        </td>
                        @include('recepcion.ModalCancelarRecepcion')
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    @if (!empty($idRecepcion))

        <form class="detalle-products" action="{{ route('recepcion.store', ['idRecepcion' => $idRecepcion]) }}"
            method="POST">
            @csrf
            <div class="container-title py-4">
                <h4>Detalle de productos</h4>
                <button type="button" class="input-submit btn btn-orange" data-bs-toggle="modal"
                    data-bs-target="#modalConfirmarRecepcion">Guardar </button>
            </div>
            <div class="content-table card p-4">
                <table>
                    <thead class="table-head">
                        <tr>
                            <th class="rounded-start">Origen</th>
                            <th>Paking list</th>
                            {{-- <th>Código</th> --}}
                            <th>Articulo</th>
                            <th>Cant Enviada</th>
                            <th>Piezas</th>
                            <th>Peso prom</th>
                            <th>Kilos</th>
                            <th class="rounded-end" style="vertical-align: middle;">
                                Recepcionar
                                <input checked type="checkbox" class="d-inline form-check-input ms-4" name="chkTodos"
                                    style="padding: 0px 10px" id="chkTodos">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (empty($detalleRecepcion))
                            <tr>
                                <td colspan="6">Productos a Recepcionar</td>
                            </tr>
                        @else
                            @foreach ($detalleRecepcion as $recepcion)
                                <tr>
                                    <td>{{ $recepcion->PackingList }}</td>
                                    <td>{{ $recepcion->CodArticulo }}</td>
                                    {{-- <td>{{ $recepcion->Codigo }}</td> --}}
                                    <td>{{ $recepcion->Descripcion }}</td>
                                    <td>{{ number_format($recepcion->CantEnviada, 2) }} KG</td>
                                    <td>
                                        @if (floatval($recepcion->CantPesoProm))
                                            <input class="input-cantidad-piezas form-control form-control-sm text-end"
                                                min="0" max="1000" step="any" type="number"
                                                data-prom="{{ $recepcion->CantPesoProm }}"
                                                value="{{ floatval($recepcion->CantEnviada) / $recepcion->CantPesoProm }}"
                                                required onkeypress="return event.keyCode != 13;" style="width: 75px">
                                        @else
                                            <input class="input-cantidad-piezas form-control form-control-sm text-end"
                                                min="0" max="1000" step="any" type="number"
                                                data-prom="{{ $recepcion->CantPesoProm }}" value="0" required
                                                onkeypress="return event.keyCode != 13;" style="width: 75px">
                                        @endif
                                    </td>
                                    <td>{{ number_format($recepcion->CantPesoProm, 2) }} KG</td>
                                    <td>
                                        <input style="width: 75px" type="number"
                                            class="input-cantidad-kilos form-control form-control-sm text-end"
                                            name="cantRecepcionada[{{ $recepcion->Codigo }}]"
                                            data-prom="{{ $recepcion->CantPesoProm }}"
                                            value="{{ $recepcion->CantEnviada }}" step="any"
                                            onkeypress="return event.keyCode != 13;">
                                    </td>
                                    <td class="d-flex align-items-center justify-content-center">
                                        <input checked type="checkbox" class="form-check-input"
                                            name="chkArticulo[{{ $recepcion->Codigo }}]" id="chkArticulo"
                                            value="{{ $recepcion->PackingList }}">
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th style="text-align: center">Total: </th>
                            <th>{{ number_format($totalRecepcion, 2) }}</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
                @include('recepcion.ModalConfirmarRecepcion')
            </div>
        </form>
    @endif

    <script>
        // Cuando das click en el check
        document.addEventListener('click', (e) => {
            if (e.target.matches('#chkTodos')) {
                const chkArticulos = document.querySelectorAll('#chkArticulo');
                const chkTodos = document.getElementById('chkTodos');
                if (chkTodos.checked == true) {
                    chkArticulos.forEach(element => {
                        element.checked = true;
                    });
                } else {
                    chkArticulos.forEach(element => {
                        element.checked = false;
                    });
                }
            }
        });

        // Cuando cambia un valor en los input
        document.addEventListener("change", (e) => {
            changeValues(e);
        });

        // Al levantar la tecla cuando cambia un valor en los input 
        document.addEventListener("keyup", (e) => {
            changeValues(e);
        });

        function changeValues(e) {
            document.querySelectorAll('.input-cantidad-piezas').forEach(item => {
                document.querySelector('.input-submit').classList.remove('disabled');
            });

            document.querySelectorAll('.input-cantidad-piezas').forEach(item => {
                // document.querySelector('.input-submit').classList.remove('disabled');
                if (!Number.isInteger(parseFloat(item.value)))
                    document.querySelector('.input-submit').classList.add('disabled');
            });

            if (e.target.matches('.input-cantidad-piezas')) {
                let value = e.target.value;

                let pesoProm = e.target.getAttribute('data-prom');
                e.target.parentNode.parentNode.querySelectorAll('input')[1].value = (value * pesoProm).toFixed(2);
            }

            if (e.target.matches('.input-cantidad-kilos')) {
                let value = e.target.value;

                let pesoProm = e.target.getAttribute('data-prom');
                e.target.parentNode.parentNode.querySelector('input').value = (value / pesoProm).toFixed(2);
            }
        }

        document.addEventListener("DOMContentLoaded", (e) => {
            document.querySelectorAll('.input-cantidad-piezas').forEach(item => {
                // console.log(Number.isInteger(parseFloat(item.value)));
                if (!Number.isInteger(parseFloat(item.value)))
                    document.querySelector('.input-submit').classList.add('disabled');
            });
        });
    </script>
@endsection
