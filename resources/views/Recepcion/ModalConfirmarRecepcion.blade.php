<!--modalConfirmarRecepcion-->
<div class="modal fade" id="modalConfirmarRecepcion" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-4">
                <h5 class="text-secondary modal-title pb-2" id="exampleModalLabel">Recepcionar Producto</h5>
                <span class="text-secondary">¿Desea Recepcionar el Producto?</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-orange-outline" data-bs-dismiss="modal">Cerrar </button>
                <button type="submit" class="btn btn-orange">Recepcionar Producto </button>
            </div>
        </div>
    </div>
</div>
