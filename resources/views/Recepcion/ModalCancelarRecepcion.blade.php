<!-- Modal Cancelar Recepción-->
<div class="modal fade" id="ModalCancelarRecepcion{{ $recepcionCV->IdCapRecepcion }}" tabindex="-1"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {{-- <div class="modal-header">
            </div> --}}
            <form id="formCancelar"
                action="{{ route('recepcion.destroy', ['idRecepcion' => $recepcionCV->IdCapRecepcion]) }}"
                method="POST">
                @csrf
                <div class="modal-body p-4">
                    <h5 class="text-secondary modal-title pb-2" id="exampleModalLabel">Cancelar Recepción</h5>
                    <label>Motivo de Cancelación</label>
                    <textarea class="form-control" name="motivoCancelacion" id="motivoCancelacion" cols="30" rows="5" required></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-orange-outline" data-bs-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-orange">Cancelar Recepción </button>
                </div>
            </form>
        </div>
    </div>
</div>
