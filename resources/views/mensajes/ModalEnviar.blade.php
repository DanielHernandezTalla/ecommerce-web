<!-- Modal Agregar-->
<div class="modal fade" id="ModalEnviar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <h5 class="modal-title" id="exampleModalLabel">Enviar mensaje</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('mensajes.send') }}" method="POST">
                    @csrf
                    <input type="hidden" name="IdCatCiudades" value="{{ $IdCatCiudades }}">
                    <input type="hidden" name="primerventa" value="{{ $primerventa }}">
                    <input type="hidden" name="numero" value="{{ $numero }}">
                    <div class="">
                        <label for="" class="form-label">Escribe un mensaje</label>
                        <textarea class="form-control" name="mensaje" id="mensaje" cols="30" rows="3" maxlength="100"></textarea>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-orange-outline rounded-pill"
                    data-bs-dismiss="modal">Cerrar</button>
                <input id="submit" type="submit" class="btn btn-orange rounded-pill" value="Enviar Mensaje">
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    let flat = 1;
    document.addEventListener('click', e=>{
        if(e.target.matches('#submit')){
            if(flat>1){
                e.preventDefault();
            }
            flat++;
        }
    })
</script>
