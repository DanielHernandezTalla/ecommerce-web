@extends('layouts.app')

@section('content')
    <div class="container-title">
        @include('components.title')
        <button class="btn btn-orange rounded-pill" role="tooltip" title="Enviar mensaje" data-bs-toggle="modal"
            data-bs-target="#ModalEnviar">Enviar mensaje</button>
    </div>

    <form class="container-title align-items-end justify-content-end gap-4 pb-4" id="formPrecios"
        action="{{ route('mensajes.index') }}" method="GET">
        <div>
            <input type="hidden" class="idPagination" value="&IdCatCiudades={{ $IdCatCiudades }}">
            <input type="hidden" class="idPagination" value="&primerventa={{ $primerventa }}">
            <input type="hidden" class="idPagination" value="&numero={{ $numero }}">

            <label class="fw-semibold text-secondary">Seleccione la ciudad</label>
            <select class="form-select" name="IdCatCiudades" id="IdCatCiudades">
                <option value="">Seleccione ciudad</option>
                @foreach ($ciudades as $ciudad)
                    <option {!! $IdCatCiudades == $ciudad->IdCatCiudades ? 'selected' : '' !!} value="{{ $ciudad->IdCatCiudades }}">
                        {{ $ciudad->Descripcion }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-auto">
            <label>Teléfono</label>
            <input type="text" name="numero" class="form-control" value="{{ $numero }}" placeholder="Numero de teléfono">
        </div>
        <div class="d-flex flex-column">
            <label class="mb-2 fw-semibold text-secondary">Primer venta</label>
            <label class="switch">
                <input id="filtro-check" name="primerventa" type="checkbox" {{ $primerventa ? 'checked' : '' }}>
                <span class="slider round"></span>
            </label>
        </div>
        <button type="submit" class="btn btn-orange rounded-pill">Buscar</button>
    </form>

    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Nombre</th>
                    <th>Ciudad</th>
                    <th>Estado</th>
                    <th class="rounded-end">Teléfono</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clientes as $cliente)
                    <tr>
                        <td>{{ $cliente->Nombre }}</td>
                        <td>{{ $cliente->Ciudad }}</td>
                        <td>{{ $cliente->Estado }}</td>
                        <td>{{ $cliente->Telefono }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    @include('mensajes.ModalEnviar')

    <div class="d-flex justify-content-center p-4">
        {{ $clientes->links() }}
    </div>
@endsection
