@extends('layouts.app')

@section('content')
    <div class="unautorize-content d-flex justify-content-center align-items-center">
        <div class="pe-5">
            <span>404 error</span>
            <h1>Pagina no encontrada</h1>
            <p class="text-secondary">Es posible que haya escrito mal la dirección o que la página se haya movido</p>
            <div class="pt-2">
                @guest
                    <a href="{{ route('login.home') }}" class="btn btn-orange">Ir al inicio de sesión</a>
                @else
                    <a href="{{ route('login.home') }}" class="btn btn-orange">Ir al inicio</a>
                @endguest
            </div>
        </div>
        <div class="">
            <img class="img-403" src="https://merakiui.com/images/components/illustration.svg" alt="404">
        </div>
    </div>
@endsection
