@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    {{-- Formulario  --}}
    <div class="card p-4" style="max-width: 700px; margin: auto;">

        <form action=" {{ route('horarioasado.store') }} " method="POST">
            @csrf

            <div class="mb-3">
                <label>Nombre servicio</label>
                <input type="text" id="DescServicio" name="DescServicio" class="form-control" tabindex="1"
                    value="{{ old('DescServicio') }}" placeholder="Escribe el nombre del servicio" required autofocus>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label>Hora de inicio</label>
                    <input type="time" id="Hora_Inicio" name="Hora_Inicio" class="form-control" tabindex="1"
                        value="{{ old('Hora_Inicio') }}">
                </div>
                <div class="col-md-6 mb-3">
                    <label>Hora de cierre</label>
                    <input type="time" id="Hora_Final" name="Hora_Final" class="form-control" tabindex="1"
                        value="{{ old('Hora_Final') }}">
                </div>
            </div>

            <div class="mb-3 d-flex align-items-center">
                <label class="switch">
                    <input type="checkbox" name="Status" {!! old('Status') == 1 ? 'checked' : '' !!}>
                    <span class="slider round"></span>
                </label>
                <label class="ps-2">Activo</label>
            </div>

            <div class="row mt-4">
                <div class="col-md-6 mt-3">
                    <a class="form-control btn submit-orange-outline" href="{{ route('horarioasado.index') }}">Cancelar</a>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="submit" class="form-control btn submit-orange">
                </div>
            </div>
        </form>
    </div>
@endsection
