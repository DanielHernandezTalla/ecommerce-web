@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    {{-- Formulario  --}}
    <div class="card p-4" style="max-width: 700px; margin: auto;">

        <form action=" {{ route('horarioasado.update', ['id' => $servicio]) }} " method="POST">
            @csrf

            <div class="mb-3">
                <label>Nombre</label>
                <input type="text" id="DescServicio" name="DescServicio" class="form-control" tabindex="1"
                    value="{{ old('DescServicio') ?? $servicio->DescServicio }}"
                    placeholder="Escribe el nombre de la ciudad" required autofocus>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label>Hora de entrada</label>
                    <input type="time" id="Hora_Inicio" name="Hora_Inicio" class="form-control" tabindex="1"
                        value="{{ old('Hora_Inicio') ?? $servicio->Hora_Inicio }}">
                </div>
                <div class="col-md-6 mb-3">
                    <label>Hora de salida</label>
                    <input type="time" id="Hora_Final" name="Hora_Final" class="form-control" tabindex="1"
                        value="{{ old('Hora_Final') ?? $servicio->Hora_Final }}">
                </div>
            </div>

            <div>
                <label class="switch">
                    @if (old('Status') || $servicio->Status)
                        <input type="checkbox" name="Status" checked>
                    @else
                        <input type="checkbox" name="Status">
                    @endif
                    <span class="slider round"></span>
                </label>
                <label class="form-check-label fw-semibold text-secondary">Activo</label>
            </div>

            <div class="row mt-4">
                <div class="col-md-6 mt-3">
                    <a class="form-control btn submit-orange-outline" href="{{ route('horarioasado.index') }}">Cancelar</a>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="submit" class="form-control btn submit-orange">
                </div>
            </div>
        </form>
    </div>
@endsection
