@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    {{-- Tabla  --}}
    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">RFC</th>
                    <th>Tipo de persona</th>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Calle</th>
                    <th>Número ext</th>
                    <th>Número int</th>
                    <th>Colonia</th>
                    <th>Codigo postal</th>
                    <th>Ciudad</th>
                    <th>Municipio</th>
                    <th>Estado</th>
                    <th>Pais</th>
                    <th>Estado</th>
                    <th class="rounded-end"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clientes as $cliente)
                    <tr>
                        <td>{{ strtoupper($cliente->RFC) }}</td>
                        <td>{{ $cliente->TIPOPERSONA }}</td>
                        <td>{{ $cliente->NOMBRE }}</td>
                        <td>{{ $cliente->MAIL }}</td>
                        <td>{{ $cliente->CALLE }}</td>
                        <td>{{ $cliente->NUM_EXT }}</td>
                        <td>{{ $cliente->NUM_INT }}</td>
                        <td>{{ $cliente->COLONIA }}</td>
                        <td>{{ $cliente->CP }}</td>
                        <td>{{ $cliente->CIUDAD }}</td>
                        <td>{{ $cliente->MUNICIPIO }}</td>
                        <td>{{ $cliente->ESTADO }}</td>
                        <td>{{ $cliente->PAIS }}</td>
                        <td style="color: rgb(22 163 74);">
                            <span class="px-4 py-0 rounded-pill border d-block"
                                style="color: #b3b3b3; white-space: nowrap; width: 150px; line-height: 2rem;">
                                @if ($cliente->STATUS == 1)
                                    <i class="bi bi-circle-fill" style="color: #b3b3b3; font-size: 14px;"></i>
                                    Creando
                                @elseif ($cliente->STATUS == 2)
                                    <i class="bi bi-circle-fill" style="color: #b3b3b3; font-size: 14px;"></i>
                                    Actualizando
                                @else
                                    <i class="bi bi-circle-fill" style="color: #4cc077; font-size: 14px;"></i>
                                    Finalizado
                                @endif
                            </span>
                        </td>
                        <td class="text-end">
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="d-flex justify-content-center p-4">
        {{ $clientes->links() }}
    </div>
@endsection
