<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> Ecommerce - {{ isset($title) ? $title : '' }}</title>

    <!-- Icon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <!-- Fonts -- icons -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    {{-- Bootstrap icons --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous" defer>
    </script>
</head>

<body>
    <div id="app">

        <nav class="nav-content p-4 border-end">
            <button class="btn btn-absolute" id="btn-toggle-menu"><i class="icon-x bi bi-x"></i><i
                    class="icon-list bi bi-list"></i></button>
            <div class="nav-links">
                <a class="nav-logo" href="{{ url('/') }}"> <img
                        src="https://www.kowi.com.mx/wp-content/uploads/elementor/thumbs/01_LOGO_KOWI-pdb6yay1990vudjiiivqobds1rhtcw2u1qinecxwpy.png"
                        alt="Logo Kowi"></a>
                <p class="d-none">
                    {{ $user = \Auth::user()->leftJoin('CatTipoUser', 'CatTipoUser.IdCatTipoUser', 'CatUsers.IdCatTipoUser')->where('CatUsers.IdCatUser', \Auth::user()->IdCatUser)->first()->Descripcion }}
                </p>
                <p class="d-none">
                    {{ $place = \Auth::user()->leftJoin('DatCentroVenta', 'DatCentroVenta.IdDatCentroVenta', 'CatUsers.IdDatCentroVenta')->where('CatUsers.IdDatCentroVenta', \Auth::user()->IdDatCentroVenta)->first()->Descripcion }}
                </p>
                @if (strtolower($user) == 'admin')
                    <div class="dropdown dropdown-small" style="--cant-items-menu: 4;">
                        <a class="dropdown-head">
                            <div><i class="bi bi-boxes"></i>Artículos</div>
                            <i class="btn-dropdown icon-dropdown-up bi bi-chevron-up"></i>
                            <i class="btn-dropdown icon-dropdown-down bi bi-chevron-down"></i>
                        </a>
                        <div class="dropdown-body">
                            <a href=" {{ route('categoriaarticulos.index') }} "><i
                                    class="bi bi-journal-text"></i>Categorías</a>
                            <a href=" {{ route('articulos.index') }}"><i class="bi bi-boxes"></i>Artículos</a>
                            <a href=" {{ route('articuloscentro.index') }}"><i
                                    class="bi bi-database"></i><small>Artículo por centro</small></a>
                        </div>
                    </div>
                    <div class="dropdown dropdown-small" style="--cant-items-menu: 4;">
                        <a class="dropdown-head">
                            <div><i class="bi bi-cash-stack"></i>Precios</div>
                            <i class="btn-dropdown icon-dropdown-up bi bi-chevron-up"></i>
                            <i class="btn-dropdown icon-dropdown-down bi bi-chevron-down"></i>
                        </a>
                        <div class="dropdown-body">
                            <a href=" {{ route('listadeprecios.index') }} "><i class="bi bi-card-checklist"></i>Lista
                                de precios</a>
                            <a href=" {{ route('articuloslista.index') }} "><i
                                    class="bi bi-layout-text-sidebar-reverse"></i>Articulo por lista</a>
                            <a href=" {{ route('precios.index') }} "><i class="bi bi-cash-stack"></i>Precios por
                                lista</a>
                        </div>
                    </div>
                    <div class="dropdown dropdown-small" style="--cant-items-menu: 4;">
                        <a class="dropdown-head">
                            <div><i class="bi bi-calendar2-range"></i>Reportes</div>
                            <i class="btn-dropdown icon-dropdown-up bi bi-chevron-up"></i>
                            <i class="btn-dropdown icon-dropdown-down bi bi-chevron-down"></i>
                        </a>
                        <div class="dropdown-body">
                            <a href="{{ route('reporte.ventas') }} "><i class="bi bi-calendar4-week"></i>Reporte
                                ventas</a>
                            <a href="{{ route('reporte.concentrado') }} " style="font-size: 14px"><i
                                    class="bi bi-graph-up"></i>Reporte concentrado</a>
                            <a href="{{ route('reporte.ventasfallidas') }} "><i
                                    class="bi bi-file-earmark-text"></i>Ventas fallidas</a>
                        </div>
                    </div>
                    <a href="{{ route('facturacion.index') }} "><i class="bi bi-file-earmark-text"></i>Facturación</a>
                    <div class="dropdown dropdown-small" style="--cant-items-menu: 5;">
                        <a class="dropdown-head">
                            <div><i class="bi bi-buildings"></i>Ciudades</div>
                            <i class="btn-dropdown icon-dropdown-up bi bi-chevron-up"></i>
                            <i class="btn-dropdown icon-dropdown-down bi bi-chevron-down"></i>
                        </a>
                        <div class="dropdown-body">
                            <a href=" {{ route('ciudades.index') }} "><i class="bi bi-building"></i>Ciudades</a>
                            <a href=" {{ route('costoenvio.index') }} "><i class="bi bi-bank"></i>Centro de venta</a>
                            <a href=" {{ route('codigospostales.index') }} "><i class="bi bi-qr-code"></i>Codigos
                                postales</a>
                            <a href=" {{ route('horarioasado.index') }} "><i class="bi bi-stopwatch"></i>Servicios</a>
                        </div>
                    </div>
                    <div class="dropdown dropdown-small" style="--cant-items-menu: 4;">
                        <a class="dropdown-head">
                            <div><i class="bi bi-person-video"></i>Usuarios</div>
                            <i class="btn-dropdown icon-dropdown-up bi bi-chevron-up"></i>
                            <i class="btn-dropdown icon-dropdown-down bi bi-chevron-down"></i>
                        </a>
                        <div class="dropdown-body">
                            <a href=" {{ route('clientescloud.index') }} "><i
                                    class="bi bi-person-gear"></i>Clientescloud</a>
                            <a href=" {{ route('usuarios') }} "><i class="bi bi-people"></i>Usuarios</a>
                            <a href=" {{ route('roles') }} "><i class="bi bi-grid"></i>Roles</a>
                        </div>
                    </div>
                @endif
                @if (strtolower($user) == 'ejecutivo')
                    <a href="{{ route('pedidos.pendientes') }}"><i class="bi bi-bell"></i>Pedidos</a>
                    <a href="{{ route('inventario.index') }} "><i class="bi bi-gear"></i>Inventarios</a>
                    <a href="{{ route('ejecutivo.inventario.edit') }} "><i class="bi bi-gear"></i>Ajuste de
                        inventario</a>
                    <a href="{{ route('facturacion.index') }} "><i
                            class="bi bi-file-earmark-text"></i>Facturación</a>
                    <a href="{{ route('clientesnuevos.index') }} "><i class="bi bi-people"></i>Clientes nuevos</a>
                @endif
                @if (strtolower($user) == 'armador')
                    <a href="{{ route('pedidos.proceso') }}"><i class="bi bi-bell"></i>Pedidos</a>
                    <a href="{{ route('pakinglist.index') }}"><i class="bi bi-box-seam"></i>Pakings list</a>
                    <a href="{{ route('recepcion.index') }}"><i class="bi bi-card-checklist"></i>Recepcion</a>
                    <a href="{{ route('inventario.index') }} "><i class="bi bi-gear"></i>Inventario</a>
                @endif
                @if (strtolower($user) == 'auditor')
                    <a href="{{ route('inventario.index') }} "><i class="bi bi-gear"></i>Inventarios</a>
                    <a href="{{ route('inventario.edit') }} "><i class="bi bi-gear"></i>Ajuste de inventario</a>
                @endif
                @if (strtolower($user) == 'facturista')
                    <a href="{{ route('cortediario') }}"><i class="bi bi-file-earmark-text"></i>Corte Diario</a>
                @endif
                @if (strtolower($user) == 'mercadotecnia')
                    <div class="dropdown dropdown-small" style="--cant-items-menu: 4;">
                        <a class="dropdown-head">
                            <div>
                                <i class="bi bi-boxes"></i>Artículos
                            </div>
                            <i class="btn-dropdown icon-dropdown-up bi bi-chevron-up"></i>
                            <i class="btn-dropdown icon-dropdown-down bi bi-chevron-down"></i>
                        </a>
                        <div class="dropdown-body">
                            <a href=" {{ route('categoriaarticulos.index') }} "><i
                                    class="bi bi-journal-text"></i>Categorías</a>
                            <a href=" {{ route('articulos.index') }}"><i class="bi bi-boxes"></i>Artículos</a>
                            <a href=" {{ route('articuloscentro.index') }}"><i
                                    class="bi bi-database"></i><small>Artículo por centro</small></a>
                        </div>
                    </div>
                    <a href="{{ route('inventario.index') }} "><i class="bi bi-gear"></i>Inventario</a>
                    <a href="{{ route('reporte.ventas') }} "><i class="bi bi-calendar4-week"></i>Reporte ventas</a>
                    <a class="d-flex align-items-center" href="{{ route('imagenespromocioens.index') }} "><i
                            class="bi bi-images"></i>Imagenes promociones</a>
                    <a class="d-flex align-items-center" href="{{ route('mensajes.index') }} "><i
                            class="bi bi-chat-dots"></i>Mensajes</a>
                @endif
            </div>

            <div class="nav-footer">
                <a class="nav-footer-link nav-link-select rounded">
                    <i class="bi bi-person-circle"></i>
                    <div class="nav-link-user">
                        <b>{{ Auth::user()->username }}</b>
                        <small>{{ $user }}</small>
                        <small>{{ $place }}</small>
                    </div>
                </a>
                <a class="nav-footer-link" href="/Logout"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <i class="bi bi-box-arrow-right"></i>
                    Cerrar Sesión
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
        </nav>

        <div class="master outline-hide bg-light">
            <div id="alert-confirm"
                class="confirm-alert confirm-alert-hide position-absolute w-100 d-flex align-items-center justify-content-center"
                style="min-height: calc(100vh - 74px);">
                <div class="confirm-alert-card card">
                    <div class="">
                        <p class="text-secondary">¿Estás seguro de eliminar?</p>
                        <span class="text-secondary">El elemento se eliminará permanentemente</span>
                    </div>
                    <div class="confirm-alert-buttons">
                        <button id="alert-button-cancel" class="btn btn-orange-outline">Cancelar</button>
                        <button id="alert-button-confirm" class="btn btn-orange">Confirmar</button>
                    </div>
                </div>
            </div>

            <main class="pt-4">
                <div id="main-content" class="p-4 container-xl">

                    @if (session()->has('success'))
                        <div id="alert-success" class="alert alert-success">
                            {{-- Usuario creado correctamente --}}
                            {{ session()->get('success') }}
                        </div>
                    @endif

                    @if (isset($errors) && $errors->any())
                        <div id="alert-danger" class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @yield('content')

                </div>
            </main>
        </div>
    </div>
</body>

</html>
