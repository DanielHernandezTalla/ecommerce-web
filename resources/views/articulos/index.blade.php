@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
        <a class="btn btn-orange rounded-pill" href="{{ route('articulos.create') }}">Crear artículo</a>
    </div>

    <div class="container-title pb-4">
        {{-- @include('components.title') --}}
        <div class="">
            <input type="text" name="filtro" id="filtro" class="form-control" placeholder="Buscar..." autofocus>
        </div>
    </div>

    {{-- Tabla  --}}
    <div class="content-table content-table-full card p-4">
        <table id="tblStock">
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Codigo</th>
                    <th>Nombre</th>
                    <th>Descripcion General</th>
                    <th>Peso Promedio</th>
                    <th>Categoria</th>
                    <th>Estatus</th>
                    <th class="rounded-end"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($articulos as $articulo)
                    <tr>
                        <td>{{ $articulo->Codigo }}</td>
                        <td><p class="text-wrap-puntitos">{{ $articulo->Descripcion }}</p></td>
                        <td><p class="text-wrap-puntitos">{{ $articulo->DescripcionGeneral }}</p></td>
                        <td>{{ ($articulo->CantPesoProm > 1 ? $articulo->CantPesoProm : '0' . $articulo->CantPesoProm) . ' ' . $articulo->Unidad }}
                        </td>
                        <td>{{ empty($articulo->Categoria->Descripcion) ? 'no hay categoria' : $articulo->Categoria->Descripcion }}
                        </td>
                        <td style="color: rgb(22 163 74);">
                            <span class="px-4 py-0 rounded-pill border d-block"
                                style="color: #b3b3b3; white-space: nowrap; width: 100px; line-height: 2rem;">
                                @if (!$articulo->Status == '0')
                                    <i class="bi bi-circle-fill" style="color: #4cc077; font-size: 14px;"></i>
                                @else
                                    <i class="bi bi-circle-fill" style="color: #b3b3b3; font-size: 14px;"></i>
                                @endif

                                Activo
                            </span>
                        </td>
                        <td>
                            <a class="btn text-warning" href="{{ route('articulos.edit', ['articulo' => $articulo->IdCatArticulos]) }}">
                                <i class="bi bi-pencil-fill"></i>
                            </a>
                            <form id="delete-user" method="POST" class="d-inline" action="{{ route('articulos.destroy', ['id' => $articulo->IdCatArticulos]) }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn text-danger">
                                    <i class="bi bi-trash3-fill"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
