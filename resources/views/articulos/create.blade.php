@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    {{-- Formulario  --}}
    <div class="card p-4" style="max-width: 700px; margin: auto;">

        <form method="POST" action="{{ route('articulos.store') }}" enctype="multipart/form-data">
            @csrf

            <div class="row">
                <div class="col-md-3 mb-3">
                    <label>Código</label>
                    <input type="text" name="Codigo" class="form-control" value="{{ old('Codigo') }}"
                        placeholder="Escribe el código del producto" autofocus>
                </div>
    
                <div class="col-md-9 mb-3">
                    <label>Nombre de articulo</label>
                    <input type="text" name="Descripcion" class="form-control" value="{{ old('Descripcion') }}"
                        placeholder="Escribe tu nombre del articulo">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label>Descripción corta</label>
                    <input type="text" name="DesCorta" class="form-control" value="{{ old('DesCorta') }}"
                        placeholder="Escribe la descripción corta">
                </div>
                <div class="col-md-6 mb-3">
                    <label>Descripción corta 2</label>
                    <input type="text" name="DesCorta1" class="form-control" value="{{ old('DesCorta1') }}"
                        placeholder="Escribe la descripción corta 2">
                </div>
            </div>

            <div class="row">
                <div class="col-md-9 mb-3">
                    <label>Descripción IVA</label>
                    <input type="text" name="DescIva" class="form-control" value="{{ old('DescIva') }}"
                        placeholder="Descripción IVA">
                </div>
                <div class="col-md-3 mb-3">
                    <label>IVA</label>
                    <input type="number" name="Iva" step="any" class="form-control" value="{{ old('Iva') }}"
                        placeholder="Ingresa el IVA">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label>Categoría</label>
                    <select name="IdCatCategoria" class="form-select">
                        <option value="0" selected>Elige la categoría</option>
                        @foreach ($categorias as $categoria)
                            @if ($categoria->IdCatCategoria == old('IdCatCategoria'))
                                <option value="{{ $categoria->IdCatCategoria }}" selected>{{ $categoria->Descripcion }}
                                </option>
                            @else
                                <option value="{{ $categoria->IdCatCategoria }}">{{ $categoria->Descripcion }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="col-md-6 mb-3">
                    <label>Paquete</label>
                    <select name="IdCatPaquete" class="form-select">
                        <option value="0" selected>Elige un paquete</option>
                        @foreach ($paquetes as $paquete)
                            @if ($paquete->IdCatPaquete == old('IdCatPaquete'))
                                <option value="{{ $paquete->IdCatPaquete }}" selected>{{ $paquete->DescPaquete }}</option>
                            @else
                                <option value="{{ $paquete->IdCatPaquete }}">{{ $paquete->DescPaquete }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label>Unidad de medida</label>
                    <input type="text" name="Unidad" class="form-control" value="{{ old('Unidad') }}"
                        placeholder="Escribe la unidad de media">
                </div>
                <div class="col-md-6 mb-3">
                    <label>Peso promedio</label>
                    <input type="number" step="any" name="CantPesoProm" class="form-control"
                        value="{{ old('CantPesoProm') }}" placeholder="Escribe el peso promedio">
                </div>
            </div>

            <div class="mb-3">
                <label>Imagen articulo</label>
                <input type="file" name="IdCatArticuloImagenes" class="form-control">
            </div>

            <div class="mb-3">
                <label>Descripción general</label>
                <textarea name="DescripcionGeneral" class="form-control" id="" cols="30" rows="2"
                    value="{{ old('DescripcionGeneral') }}"></textarea>
            </div>

            <div>
                <label class="switch">
                    @if (old('Status'))
                        <input type="checkbox" name="Status" checked>
                    @else
                        <input type="checkbox" name="Status">
                    @endif
                    <span class="slider round"></span>
                </label>
                <label class="form-check-label fw-semibold text-secondary">Activo</label>
            </div>

            <div class="row">
                <div class="col-md-6 mt-3">
                    <a class="form-control btn submit-orange-outline" href="{{ route('articulos.index') }}">Cancelar</a>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="submit" class="form-control btn submit-orange">
                </div>
            </div>

        </form>
    </div>
@endsection
