<!---Registrado Corectamente -->
@if(Session::has('msjAdd'))
<div class="alert alert-success" role="alert" id="alerta">
  <strong><i class="fa fa-check"></i> {{ Session::get('msjAdd') }} </strong>
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

<!-- Eliminado Correctamente --->
@if(Session::has('msjdelete'))
<div class="alert alert-danger" role="alert" id="alerta">
  <strong><i class="fa fa-exclamation-triangle"></i> {{ Session::get('msjdelete') }}</strong>
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

<!-- Editado Correctamente -->
@if(Session::has('msjupdate'))
<div class="alert alert-warning mt-2" role="alert" id="alerta">
  <strong><i class="fa fa-exclamation-triangle"></i> {{ Session::get('msjupdate') }} </strong>
</div>
@endif

<!-- Alerta para Validar Usuario -->
@if(Session::has('msjErrorValida'))
<div class="alert alert-danger mt-2" role="alert" id="alerta">
  <strong><i class="fa fa-exclamation-triangle"></i> {{ Session::get('msjErrorValida') }} </strong>
</div>
@endif

<!-- Eliminado Logico -->
@if(Session::has('msjEliminadoLogico'))
<div class="alert alert-warning mt-2" role="alert" id="alerta">
  <strong><i class="fa fa-exclamation-triangle"></i> {{ Session::get('msjEliminadoLogico') }} </strong>
</div>
@endif

@if (isset($errors) && $errors->any())
<div class="alert alert-danger mt-2" role="alert" id="alerta">
  <ul>
    @foreach ($errors->all() as $error)
    <li><i class="fa fa-exclamation-triangle"></i> <strong>{{ $error }}</strong></li>
    @endforeach
  </ul>
</div>
@endif
