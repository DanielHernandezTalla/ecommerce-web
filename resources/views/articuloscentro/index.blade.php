@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
        <form class="row" id="formPrecios" action="{{ route('articuloscentro.index') }}" method="GET">
            <div class="col-auto">
                <select class="form-select" name="IdDatCentroVenta" id="IdDatCentroVenta">
                    <option value="">Seleccione el centro de venta</option>
                    @foreach ($centroVenta as $centro)
                        <option {!! $IdDatCentroVenta == $centro->IdDatCentroVenta ? 'selected' : '' !!} value="{{ $centro->IdDatCentroVenta }}">
                            {{ $centro->Descripcion }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-auto">
                <button class="btn btn-orange">
                    Buscar
                </button>
            </div>
        </form>
    </div>

    <div class="container">
        <div class="d-flex justify-content-center">
            <div class="col-auto">
                @include('Alertas.Alertas')
            </div>
        </div>
    </div>

    {{-- Formulario para agregar productos --}}
    <div class="container-title pb-4 d-flex justify-content-between align-items-end">
        @if ($IdDatCentroVenta)
            <form action="{{ route('articuloscentro.addArticle') }}" method="POST">
                @csrf
                <div class="d-flex align-items-end">
                    <input type="hidden" name="IdDatCentroVenta" id="IdDatCentroVenta" value="{{ $IdDatCentroVenta }}">
                    <div class="col-auto">
                        <label>Buscar Articulo</label>
                        <input class="input-article form-control" list="articulos" name="Codigo" id="Codigo"
                            placeholder="Buscar articulo" autocomplete="off" onkeypress="return event.keyCode != 13;"
                            required autofocus>
                        <datalist id="articulos">
                            @foreach ($articulos as $articulo)
                                <option class="prom{{ $articulo->Codigo }}" value="{{ $articulo->Codigo }}">
                                    {{ $articulo->Descripcion }}
                                </option>
                            @endforeach
                        </datalist>
                    </div>
                    <div class="col-auto ms-4">
                        <button class="input-submit btn btn-orange"> Agregar </button>
                    </div>
                </div>
            </form>
            <a class="btn btn-orange" href="{{ route('articuloscentro.update') }}"
                onclick="event.preventDefault(); document.getElementById('articuloscentro-form').submit();">
                Actualizar estatus
            </a>
        @endif
    </div>

    {{-- Tabla  --}}
    <form id="articuloscentro-form" action="{{ route('articuloscentro.update') }}" method="POST">
        @csrf
        @method('PUT')
        <input type="hidden" name="IdDatCentroVenta" id="IdDatCentroVenta" value="{{ $IdDatCentroVenta }}">
        <div class="content-table content-table-full card p-4">
            <table id="tblStock">
                <thead class="table-head">
                    <tr>
                        <th class="rounded-start">Codigo</th>
                        <th>Articulo</th>
                        <th>Activo</th>
                        <th class="rounded-end"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($articulosCentro as $ac)
                        <tr>
                            <td>{{ $ac->Codigo }}</td>
                            <th>{{ $ac->Descripcion }}</th>
                            <th class="line-center">
                                <label class="switch">
                                    <input type="checkbox" name="Activo[]" value="{{ $ac->Codigo }}"
                                        {{ $ac->Activo == 1 ? 'checked' : '' }}>
                                    <span class="slider round"></span>
                                </label>
                            </th>
                            <td>
                                <a class="btn text-danger"
                                    href="{{ route('articuloscentro.destroy', ['id' => $ac->IdDatArticulos]) }}">
                                    <i class="bi bi-trash3-fill"></i>
                                </a>
                                {{-- <form id="delete-user" method="POST" class="d-inline"
                                    action="{{ route('articuloscentro.destroy', ['id' => $ac->IdDatArticulos]) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn text-danger">
                                        <i class="bi bi-trash3-fill"></i>
                                    </button>
                                </form> --}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </form>
@endsection
