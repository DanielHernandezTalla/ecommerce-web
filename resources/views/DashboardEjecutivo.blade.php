@extends('layouts.app')

@section('content')
<style>

.nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: white !important;
    background-color:#ff7514 !important;
}

</style>

<div class="container">
<div class="card shadow">

<div class="d-flex justify-content-center">
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
        <li class="nav-item" role="presentation">
          <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Pendientes</button>
        </li>
        <li class="nav-item" role="presentation">
          <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Preparados</button>
        </li>
        <li class="nav-item" role="presentation">
          <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Enviados</button>
        </li>
        <li class="nav-item" role="presentation">
          <button class="nav-link" id="pills-entregado-tab" data-bs-toggle="pill" data-bs-target="#pills-entregado" type="button" role="tab" aria-controls="pills-entregado" aria-selected="false">Entregados</button>
        </li>
      </ul>
    </div>
      <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
            <table style="text-align: center" class="table table-responsive table-striped">
                <thead style="background-color: #ff7514; color:white ">
                <tr>
               
                 <th >Id Pedido</th>
                 <th >Fecha Pedido</th>
                 <th >Importe</th>
                 <th >Usuario</th>
                 <th >Direccion</th>
                 <th>Telefono</th>
                 <th>Comentario</th>
                 <th>Acciones</th>
                                          
                </tr>
             </thead>
             <tbody class="text-center">
                @foreach ($pedidos as $pedido)
                <tr>
                <td>{{$pedido->IdDatVentas}}</td>
                <td>{{$pedido->Fecha_Pedido}}</td>
                <td>{{$pedido->Importe}}</td>
                <td>{{$pedido->Cliente->Nombre}}</td>
                <td>{{$pedido->Cliente->Direccion}}</td>
                <td>{{$pedido->Cliente->Telefono}}</td>
                <td>{{$pedido->Comentario}}</td>
                <td>
                    <button class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#ModalPedido{{$pedido->IdDatVentas}}">
                    Editar
                    </button>
                    @include('Modals.ModalPedido')
                </td>
                
                </tr>
                @endforeach
             </tbody>
             
            </table>
        </div>
        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
            <table style="text-align: center" class="table table-responsive table-striped">
                <thead style="background-color: #ff7514; color:white ">
                <tr>
               
                 <th >Id Pedido</th>
                 <th >Fecha Pedido</th>
                 <th >Importe</th>
                 <th >Usuario</th>
                 <th >Direccion</th>
                 <th>Comentario</th>
                 <th>Acciones</th>
                                          
                </tr>
             </thead>
             <tbody class="text-center">
                @foreach ($pedidosPreparados as $pedidoPreparado)
                <tr>
                <td>{{$pedidoPreparado->IdDatVentas}}</td>
                <td>{{$pedidoPreparado->Fecha_Pedido}}</td>
                <td>{{$pedidoPreparado->Importe}}</td>
                <td>{{$pedidoPreparado->Cliente->Nombre}}</td>
                <td>{{$pedidoPreparado->Cliente->Direccion}}</td>
                <td>{{$pedidoPreparado->Comentario}}</td>
                <td>
                    <button class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#ModalPedido{{$pedidoPreparado->IdDatVentas}}">
                    Editar
                    </button>
                    @include('Modals.ModalPedidoPreparado')
                </td>
                
                </tr>
                @endforeach
             </tbody>
             
            </table>
        </div>
        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
        <table style="text-align: center" class="table table-responsive table-striped">
                <thead style="background-color: #ff7514; color:white ">
                <tr>
               
                 <th >Id Pedido</th>
                 <th >Fecha Pedido</th>
                 <th >Importe</th>
                 <th >Usuario</th>
                 <th >Direccion</th>
                 <th>Comentario</th>
                 <th>Acciones</th>
                                          
                </tr>
             </thead>
             <tbody class="text-center">
                @foreach ($pedidosEnviados as $pedidosEnviado)
                <tr>
                <td>{{$pedidosEnviado->IdDatVentas}}</td>
                <td>{{$pedidosEnviado->Fecha_Pedido}}</td>
                <td>{{$pedidosEnviado->Importe}}</td>
                <td>{{$pedidosEnviado->Cliente->Nombre}}</td>
                <td>{{$pedidosEnviado->Cliente->Direccion}}</td>
                <td>{{$pedidosEnviado->Comentario}}</td>
                <td>
                    <button class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#ModalPedido{{$pedidosEnviado->IdDatVentas}}">
                    Editar
                    </button>
                    @include('Modals.ModalPedidoEnviado')
                </td>
                
                </tr>
                @endforeach
             </tbody>
             
            </table>
        </div>

             <div class="tab-pane fade" id="pills-entregado" role="tabpanel" aria-labelledby="pills-entregado-tab">
        <table style="text-align: center" class="table table-responsive table-striped">
                <thead style="background-color: #ff7514; color:white ">
                <tr>
               
                 <th >Id Pedido</th>
                 <th >Fecha Pedido</th>
                 <th >Importe</th>
                 <th >Usuario</th>
                 <th >Direccion</th>
                 <th>Comentario</th>
                                          
                </tr>
             </thead>
             <tbody class="text-center">
                @foreach ($pedidosEntregados as $pedidosEntregado)
                <tr>
                <td>{{$pedidosEntregado->IdDatVentas}}</td>
                <td>{{$pedidosEntregado->Fecha_Pedido}}</td>
                <td>{{$pedidosEntregado->Importe}}</td>
                <td>{{$pedidosEntregado->Cliente->Nombre}}</td>
                <td>{{$pedidosEntregado->Cliente->Direccion}}</td>
                <td>{{$pedidosEntregado->Comentario}}</td>
                </tr>
                @endforeach
             </tbody>
             
            </table>
        </div>
      </div>
    
</div>
</div>

@endsection