@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    <div class="d-flex justify-content-center align-items-center">
        {{-- Formulario  --}}
        <div class="card p-4 me-5" style="min-width: 700px;">

            <form method="GET" action="{{ route('facturacion.edit') }}">
                <input type="hidden" name="IdSolictudFe" value="{{ $solicitud->IdSolictudFe }}">
                <div class="row align-items-end">
                    <div class="col-md-9 mb-3">
                        <label>RFC</label>
                        <input type="text" name="RFC" class="form-control" placeholder="Ingresa el RFC a buscar"
                            autofocus style="text-transform:uppercase;">
                    </div>
                    <div class="col-md-3 mb-3">
                        <input type="submit" class="form-control btn submit-orange" value="Buscar">
                    </div>
                </div>
            </form>

            @if (!$datosCliente && $RFC)
                <div>
                    <label style="color: #f00;">Cliente no encontrado...</label>
                    <br>
                    <h4 class="text-gray">Crear cliente nuevo</h4>
                </div>
                <form method="POST" class="mt-2" action="{{ route('facturacion.createcliente') }}">
                    @csrf

                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <label>Tipo persona</label>
                            <input type="text" name="TIPOPERSONA" class="form-control" placeholder="Tipo de persona"
                                value="{{ old('TIPOPERSONA') }}">
                        </div>
                        <div class="col-md-9 mb-3">
                            <label>RFC</label>
                            <input type="text" name="RFC" class="form-control" placeholder="Escribe el RFC"
                                value="{{ old('RFC') }}" style="text-transform:uppercase;">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8 mb-3">
                            <label>Nombre cliente</label>
                            <input type="text" name="NOMBRE" class="form-control"
                                placeholder="Escribe el nombre del cliente" value="{{ old('NOMBRE') }}">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label>Correo</label>
                            <input type="email" name="MAIL" class="form-control" placeholder="Escribe el correo"
                                value="{{ old('MAIL') }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label>Colonia</label>
                            <input type="text" name="COLONIA" class="form-control" placeholder="Escribe la colonia"
                                value="{{ old('COLONIA') }}">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Calle</label>
                            <input type="text" name="CALLE" class="form-control" placeholder="Escribe la calle"
                                value="{{ old('CALLE') }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label>Numero exterior</label>
                            <input type="text" name="NUM_EXT" class="form-control"
                                placeholder="Escribe el numero exterior" value="{{ old('NUM_EXT') }}">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label>Numero interior</label>
                            <input type="text" name="NUM_INT" class="form-control"
                                placeholder="Escribe el numero interior" value="{{ old('NUM_INT') }}">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label>Codigo postal</label>
                            <input type="text" name="CP" class="form-control"
                                placeholder="Escribe el codigo postal"value="{{ old('CP') }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label>Ciudad</label>
                            <input type="text" name="CIUDAD" class="form-control"placeholder="Escribe la ciudad"
                                value="{{ old('CIUDAD') }}">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Municipio</label>
                            <input type="text" name="MUNICIPIO" class="form-control" placeholder="Escribe el municipio"
                                value="{{ old('MUNICIPIO') }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label>Estado</label>
                            <input type="text" name="ESTADO" class="form-control" placeholder="Escribe el estado"
                                value="{{ old('ESTADO') }}">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Pais</label>
                            <input type="text" name="PAIS" class="form-control"
                                placeholder="Escribe el pais"value="{{ old('PAIS') }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mt-3">
                            <a class="form-control btn submit-orange-outline"
                                href="{{ route('facturacion.index') }}">Cancelar</a>
                        </div>
                        <div class="col-md-6 mt-3">
                            <input type="submit" class="form-control btn submit-orange" value="Crear cliente">
                        </div>
                    </div>

                </form>
            @endif

            @if ($datosCliente)
                <form method="POST" id="facturacion-update"
                    action="{{ route('facturacion.update', ['id' => $solicitud->IdSolictudFe]) }}">
                    @csrf

                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <label>Id Venta</label>
                            <input type="text" name="IdDatVentas" class="form-control"
                                value="{{ $solicitud->IdDatVentas }}" disabled>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label>Id Cliente</label>
                            <input type="text" name="ID_CLIENTE" class="form-control"
                                value="{{ $datosCliente->IdClienteCloud }}">
                        </div>
                        <div class="col-md-5 mb-3">
                            <label>RFC</label>
                            <input type="text" name="RFC" class="form-control" placeholder="Escribe el RFC"
                                value="{{ old('RFC') ?? $datosCliente->RFC }}" style="text-transform:uppercase;">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label>Bill to</label>
                            <input type="text" name="Bill_To" class="form-control"
                                value="{{ $datosCliente->Bill_To }}">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label>Ship to</label>
                            <input type="text" name="Ship_To" class="form-control"
                                value="{{ $datosCliente->Ship_To }}">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label>Tipo persona</label>
                            <input type="text" name="TIPOPERSONA" class="form-control" placeholder="Tipo de persona"
                                value="{{ old('TIPOPERSONA') ?? $datosCliente->TipoPersona }}">
                        </div>
                        {{-- <div class="col-md-4 mb-3">
                            <label>Tipo cliente</label>
                            <input type="text" name="TIPO_CLIENTE" class="form-control"
                                value="{{ $datosCliente->TipoPersona }}">
                        </div> --}}
                    </div>

                    <div class="row">
                        <div class="col-md-8 mb-3">
                            <label>Nombre cliente</label>
                            <input type="text" name="NOMBRE" class="form-control"
                                placeholder="Escribe el nombre del cliente"
                                value="{{ old('NOMBRE') ?? $datosCliente->NomCliente }}">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label>Correo</label>
                            <input type="email" name="MAIL" class="form-control" placeholder="Escribe el correo"
                                value="{{ old('MAIL') ?? $datosCliente->Email }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label>Colonia</label>
                            <input type="text" name="COLONIA" class="form-control" placeholder="Escribe la colonia"
                                value="{{ old('COLONIA') ?? $datosCliente->Colonia }}">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Calle</label>
                            <input type="text" name="CALLE" class="form-control" placeholder="Escribe la calle"
                                value="{{ old('CALLE') ?? $datosCliente->Calle }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label>Numero exterior</label>
                            <input type="text" name="NUM_EXT" class="form-control"
                                placeholder="Escribe el numero exterior"
                                value="{{ old('NUM_EXT') ?? $datosCliente->NumExt }}">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label>Numero interior</label>
                            <input type="text" name="NUM_INT" class="form-control"
                                placeholder="Escribe el numero interior"
                                value="{{ old('NUM_INT') ?? $datosCliente->NumInt }}">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label>Codigo postal</label>
                            <input type="text" name="CP" class="form-control"
                                placeholder="Escribe el codigo postal"value="{{ old('CP') ?? $datosCliente->CodigoPostal }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label>Ciudad</label>
                            <input type="text" name="CIUDAD" class="form-control"placeholder="Escribe la ciudad"
                                value="{{ old('CIUDAD') ?? $datosCliente->Ciudad }}">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Municipio</label>
                            <input type="text" name="MUNICIPIO" class="form-control"
                                placeholder="Escribe el municipio"
                                value="{{ old('MUNICIPIO') ?? $datosCliente->Municipio }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label>Estado</label>
                            <input type="text" name="ESTADO" class="form-control" placeholder="Escribe el estado"
                                value="{{ old('ESTADO') ?? $datosCliente->Estado }}">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Pais</label>
                            <input type="text" name="PAIS" class="form-control"
                                placeholder="Escribe el pais"value="{{ old('PAIS') ?? $datosCliente->Pais }}">
                        </div>
                        <div class="d-none">
                            <label>Is update</label>
                            <input type="checkbox" name="isUpdate">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 mt-3">
                            <a class="form-control btn submit-orange-outline"
                                href="{{ route('facturacion.index') }}">Cancelar</a>
                        </div>
                        <div class="col-md-4 mt-3">
                            <input type="submit" id="btn-relacionar-client"
                                class="form-control btn submit-orange-outline" value="Relacionar">
                        </div>
                        <div class="col-md-4 mt-3">
                            <input id="btn-update-client" class="form-control btn submit-orange" value="Actualizar">
                        </div>
                    </div>

                </form>
                {{-- <form method="POST" action="{{ route('facturacion.update', ['id' => $solicitud->IdSolictudFe]) }}">
                    @csrf

                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <label>Id Venta</label>
                            <input type="text" name="IdDatVentas" class="form-control"
                                value="{{ $solicitud->IdDatVentas }}" disabled>
                        </div>
                        <div class="col-md-9 mb-3">
                            <label>Id Cliente</label>
                            <input type="text" name="ID_CLIENTE" class="form-control"
                                value="{{ $datosCliente->IdClienteCloud }}">
                        </div>
                    </div>

                    <div class="row">


                        <div class="col-md-12 mb-3">
                            <label>Nombre cliente</label>
                            <input type="text" name="NOMBRE" class="form-control"
                                value="{{ $datosCliente->NomCliente }}">
                        </div>
                    </div>

                    <div class="mb-3">
                        <label>Tipo cliente</label>
                        <input type="text" name="TIPO_CLIENTE" class="form-control"
                            value="{{ $datosCliente->TipoPersona }}">
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label>Bill to</label>
                            <input type="text" name="Bill_To" class="form-control"
                                value="{{ $datosCliente->Bill_To }}">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Ship to</label>
                            <input type="text" name="Ship_To" class="form-control"
                                value="{{ $datosCliente->Ship_To }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mt-3">
                            <a class="form-control btn submit-orange-outline"
                                href="{{ route('facturacion.index') }}">Cancelar</a>
                        </div>
                        <div class="col-md-6 mt-3">
                            <input type="submit" class="form-control btn submit-orange" value="Relacionar">
                        </div>
                    </div>

                </form> --}}
            @endif
        </div>

        <div>
            @isset($solicitud->Archivo)
                {{-- <iframe width="600" height="700" src="{{ $solicitud->Archivo }}" frameborder="0"></iframe> --}}
                {{-- <embed src="{{ $solicitud->Archivo }}" type="application/pdf" width="600" height="400"> --}}
                <object data="{{ $solicitud->Archivo }}" type="application/pdf" width="600" height="400">
                    <p class="h5">
                        Tu navegador no soporta la visualización de PDFs.
                        <a href="{{ $solicitud->Archivo }}" target="_blanck"> Descargar el PDF </a>.
                    </p>
                </object>

                {{-- <embed src="https://tailwindcss.com/docs/text-color" type="application/pdf" width="600" height="400"> --}}
                {{-- <object data="https://tailwindcss.com/docs/text-color" type="application/pdf" width="600" height="400">
                    <p>
                        Tu navegador no soporta la visualización de PDFs.
                        <a href="https://tailwindcss.com/docs/text-color"> Descargar el PDF </a>.
                    </p>
                </object> --}}
            @endisset
        </div>
    </div>
@endsection

<script>
    document.addEventListener('click', (e) => {
        if (e.target.matches('#btn-update-client')) {
            let form = document.getElementById('facturacion-update');
            form.isUpdate.checked = true
            console.log(form.isUpdate.checked);
            form.submit();
        }
    })
</script>
