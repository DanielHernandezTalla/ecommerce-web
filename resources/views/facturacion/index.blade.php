@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    {{-- Tabla  --}}
    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Venta</th>
                    <th>Cliente</th>
                    <th>Nombre</th>
                    <th>Tipo cliente</th>
                    <th>Método de pago</th>
                    <th>Uso CFDI</th>
                    <th class="rounded-end"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($solicitudes as $solicitud)
                    <tr>
                        @if ($solicitud->ID_CLIENTE)
                            <td>{{ $solicitud->IdDatVentas }}</td>
                            <td>{{ $solicitud->ID_CLIENTE }}</td>
                            <td>{{ $solicitud->NOMBRE }}</td>
                            <td>{{ $solicitud->TIPO_CLIENTE }}</td>
                        @else
                            <td style="color: #f00;">{{ $solicitud->IdDatVentas }}</td>
                            <td style="color: #f00;">XXXXXXXX</td>
                            <td style="color: #f00;">XXXXXXXX</td>
                            <td style="color: #f00;">XXXXXXXX</td>
                        @endif
                        <td>
                            <p class="text-wrap-puntitos">{{ $solicitud->NomMetododePago }}</p>
                        </td>
                        <td>
                            <p class="text-wrap-puntitos">{{ $solicitud->NomCFDI }}</p>
                        </td>
                        <td class="text-end">
                            <form class="d-inline" action="{{ route('facturacion.edit') }}">
                                <input type="hidden" name="IdSolictudFe" value="{{ $solicitud->IdSolictudFe }}">

                                <button class="btn text-warning" data-bs-toggle="mensaje" title="Recepcionar">
                                    <i class="bi bi-file-earmark-text"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="d-flex justify-content-center p-4">
        {{ $solicitudes->links() }}
    </div>
@endsection
