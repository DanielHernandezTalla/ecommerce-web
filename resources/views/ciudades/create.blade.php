@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    {{-- Formulario  --}}
    <div class="card p-4" style="max-width: 700px; margin: auto;">

        <form action=" {{ route('ciudades.store') }} " method="POST">
            @csrf

            <div class="mb-3">
                <label>Nombre</label>
                <input type="text" id="descripcion" name="Descripcion" class="form-control" tabindex="1"
                    value="{{ old('Descripcion') }}" placeholder="Escribe el nombre de la ciudad" required autofocus>
            </div>

            <div class="mb-3">
                <label>Lista de precios</label>
                <select name="IdCatListaPrecio" class="form-select" tabindex="1">
                    <option value="0" selected>Elige una lista de precios</option>
                    @foreach ($listaPrecios as $lista)
                        <option value="{{ $lista->IdCatListaPrecio }}">{{ $lista->Descripcion }}</option>
                    @endforeach
                </select>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label>Hora de entrada</label>
                    <input type="time" id="Hora_entrada" name="Hora_entrada" class="form-control" tabindex="1"
                        value="{{ old('Hora_entrada') }}">
                </div>
                <div class="col-md-6 mb-3">
                    <label>Hora de salida</label>
                    <input type="time" id="Hora_salida" name="Hora_salida" class="form-control" tabindex="1"
                        value="{{ old('Hora_salida') }}">
                </div>
            </div>

            <div class="mb-3 d-flex align-items-center">
                <label class="switch">
                    <input type="checkbox" name="Status" {!! old('Hora_salida') == 1 ? 'checked' : '' !!}>
                    <span class="slider round"></span>
                </label>
                <label class="ps-2">Activo</label>
            </div>

            <div class="row mt-4">
                <div class="col-md-6 mt-3">
                    <a class="form-control btn submit-orange-outline" href="{{ route('ciudades.index') }}">Cancelar</a>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="submit" class="form-control btn submit-orange">
                </div>
            </div>
        </form>
    </div>
@endsection
