@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    {{-- Formulario  --}}
    <div class="card p-4" style="max-width: 700px; margin: auto;">

        <form id="form" class="m-0" action=" {{ route('ciudades.update', ['id' => $ciudad]) }} " method="POST">
            @csrf

            <input type="hidden" name="idCiudad" value={{ $ciudad->IdCatCiudades }}>
            <input type="hidden" name="Status" value=''>

            {{-- @dump($ciudad) --}}

            <div class="mb-3">
                <label>Nombre</label>
                <input type="text" id="descripcion" name="Descripcion" class="form-control" tabindex="1"
                    value="{{ old('Descripcion') ?? $ciudad->Descripcion }}" placeholder="Escribe el nombre de la ciudad"
                    required autofocus>
            </div>

            <div class="mb-3">
                <label>Lista de precios</label>
                <select name="IdCatListaPrecio" class="form-select" tabindex="1">
                    <option value="0" selected>Elige una lista de precios</option>
                    @foreach ($listaPrecios as $lista)
                        @if ($lista->IdCatListaPrecio == $ciudad->IdCatListaPrecio)
                            <option value="{{ $lista->IdCatListaPrecio }}" selected>{{ $lista->Descripcion }}</option>
                        @else
                            <option value="{{ $lista->IdCatListaPrecio }}">{{ $lista->Descripcion }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </form>


        <div class="mb-0 d-flex justify-content-between align-items-center">
            <label>Horarios disponibles</label>
            <button id="agregarDia" class="btn text-orange" style="color: #ff8300;">Agregar día</button>
        </div>

        <div class="mb-3 d-flex justify-content-between">
            <table class="w-100">
                <thead>
                    <tr>
                        <th><label>Dia Inicio</label></th>
                        <th><label>Dia Fin</label></th>
                        <th><label>Hora Inicio</label></th>
                        <th><label>Hora Cierre</label></th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($diasDisponibles) == 0)
                        <tr>
                            <td colspan="4" class="text-center"><label>Sin Horarios Disponibles</label></td>
                        </tr>
                    @endif
                    @foreach ($diasDisponibles as $dias)
                        <tr>
                            <td>{{ $semana[$dias->DiaInicial] }}</td>
                            <td>{{ $semana[$dias->DiaFinal] }}</td>
                            <td>{{ $dias->HoraEntrada }}</td>
                            <td>{{ $dias->HoraSalida }}</td>
                            <td>
                                <form method="POST" class="d-inline"
                                    action="{{ route('ciudades.destroy.horario', ['id' => $dias->IdCatDiaDisponible]) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn text-danger">
                                        <i class="bi bi-trash3-fill"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <form id="formHorarios" class="m-0" method="POST"
            action="{{ route('ciudades.add.horario', ['id' => $ciudad]) }}">
            @csrf
            <div id="agregarDiaBlock" class="row d-none mb-3">
                <div class="col-md-6 mb-3">
                    <label>Día de inicio</label>
                    <div class="row">
                        <div class="col-12">
                            <select name="diaInicio" class="form-select" tabindex="1" required>
                                <option value="">Selecciona día de inicio</option>
                                @foreach ($semana as $key => $dia)
                                    <option value="{{ $key }}">{{ $dia }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <label>Día de cierre</label>
                    <div class="row">
                        <div class="col-12">
                            <select name="diaCierre" class="form-select" tabindex="1" required>
                                <option value="" selected>Selecciona día de cierre</option>
                                @foreach ($semana as $key => $dia)
                                    <option value="{{ $key }}">{{ $dia }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <label>Hora de inicio</label>
                    <div class="row">
                        <div class="col-12">
                            <input type="time" id="Hora_inicio" name="Hora_inicio" class="form-control" tabindex="1"
                                value="{{ old('Hora_inicio') }}" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <label>Hora de cierre</label>
                    <div class="row">
                        <div class="col-12">
                            <input type="time" id="Hora_salida" name="Hora_salida" class="form-control" tabindex="1"
                                value="{{ old('Hora_salida') }}" required>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    <button id="saveAgregar" class="btn btn-orange">Agregar</button>
                </div>
            </div>
        </form>

        <div class="d-flex gap-2">
            <label class="switch">
                @if (old('Status') || $ciudad->Status)
                    <input class="inputStatus" type="checkbox" name="Status" checked>
                @else
                    <input class="inputStatus" type="checkbox" name="Status">
                @endif
                <span class="slider round"></span>
            </label>
            <label class="form-check-label fw-semibold text-secondary">Activo</label>
        </div>

        <div class="row mt-4">
            <div class="col-md-6 mt-3">
                <a class="form-control btn submit-orange-outline" href="{{ route('ciudades.index') }}">Cancelar</a>
            </div>
            <div class="col-md-6 mt-3">
                <input id="enviarCiudad" type="submit" class="form-control btn submit-orange">
            </div>
        </div>

    </div>
@endsection

<script>
    document.addEventListener('click', (e) => {
        if (e.target.matches('#agregarDia')) {
            e.preventDefault();

            let agregarDiaBlock = document.getElementById('agregarDiaBlock');
            if (agregarDiaBlock.classList.contains('d-none'))
                agregarDiaBlock.classList.remove('d-none');
            else
                agregarDiaBlock.classList.add('d-none');
        }

        if (e.target.matches('#enviarCiudad')) {
            let $form = document.getElementById('form');
            let input = document.querySelector('.inputStatus')

            $form.Status.value = input.checked;

            $form.submit();
        }
    })
</script>
