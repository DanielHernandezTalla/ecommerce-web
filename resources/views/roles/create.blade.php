@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    {{-- Formulario  --}}
    <div class="card p-4" style="max-width: 700px; margin: auto;">

        <form method="POST" action="{{ route('roles.store') }}">

            @csrf

            <div class="mb-3">
                <label>Nombre de rol</label>
                <input type="text" name="Descripcion" class="form-control" value="{{ old('Descripcion') }}"
                    placeholder="Escribe tu nombre de usuario" autofocus>
            </div>

            <div>
                <label class="switch">
                    <input type="checkbox" name="Status" checked>
                    <span class="slider round"></span>
                </label>
                <label class="form-check-label fw-semibold text-secondary">Activo</label>
            </div>

            <div class="row mt-4">
                <div class="col-md-6 mt-3">
                    <a class="form-control btn submit-orange-outline" href="{{ route('roles') }}">Cancelar</a>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="submit" class="form-control btn submit-orange">
                </div>
            </div>

        </form>
    </div>
@endsection
