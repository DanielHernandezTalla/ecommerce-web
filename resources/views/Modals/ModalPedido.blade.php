<!-- Modal Agregar-->
<div class="modal fade" id="ModalPedido{{ $pedido->IdDatVentas }}" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content border-0 p-2">
            <div class="modal-header border-0">
                <p class="text-secondary modal-title">Detalle de pedido</p>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body border-0">
                <div class="content-table card border-0">
                    <table>
                        <thead class="table-head">
                            <tr>
                                <th class="rounded-start">Nombre</th>
                                <th>Cantidad</th>
                                <th>Precio</th>
                                <th class="rounded-end">Servicio</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pedido->DetallesVentas as $detalle)
                                <tr>
                                    <td>{{ empty($detalle->Descripcion) ? 'no tiene nombre' : $detalle->Descripcion }}
                                    </td>
                                    <td>{{ empty($detalle->Cantidad) ? 'no tiene Cantidad' : number_format($detalle->Cantidad, 2) . ' ' . $detalle->Uom }}
                                    </td>
                                    <td>{{ empty($detalle->Precio) ? 'no tiene Precio' : '$' . $detalle->Precio }}</td>
                                    <td>{{ empty($detalle->DescServicio) ? '' : $detalle->DescServicio }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer border-0">
                <button type="button" class="btn btn-orange-outline" data-bs-dismiss="modal">Cerrar</button>
                <form class="d-flex align-items-center"
                    action="/ejecutivo/UpdatePrepararPedido/{{ $pedido->IdDatVentas }}" method="POST">
                    @csrf
                    <input type="submit" class="btn btn-orange" value="Enviar">
                </form>

            </div>
        </div>
    </div>
</div>
