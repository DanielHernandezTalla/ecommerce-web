@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
        <form class="row" id="formPrecios" action="{{ route('articuloslista.index') }}" method="GET">
            <div class="col-auto">
                <select class="form-select" name="IdCatListaPrecio" id="IdCatListaPrecio">
                    <option value="">Seleccione lista de precios</option>
                    @foreach ($listaprecios as $precio)
                        <option {!! $IdCatListaPrecio == $precio->IdCatListaPrecio ? 'selected' : '' !!} value="{{ $precio->IdCatListaPrecio }}">
                            {{ $precio->Descripcion }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-auto">
                <button class="btn btn-orange">
                    Buscar
                </button>
            </div>
        </form>
    </div>

    {{-- Formulario para agregar productos --}}
    @if($IdCatListaPrecio != 0)
        <div class="container-title pb-4 d-flex justify-content-between align-items-end">
            <form action="{{ route('articuloslista.addArticle') }}" method="POST">
                @csrf
                <div class="d-flex align-items-end">
                    <input type="hidden" name="IdCatListaPrecio" id="IdCatListaPrecio" value="{{ $IdCatListaPrecio }}">
                    <div class="col-auto">
                        <label>Buscar Articulo</label>
                        <input class="input-article form-control" list="articulos" name="Codigo" id="Codigo"
                            placeholder="Buscar articulo" autocomplete="off" onkeypress="return event.keyCode != 13;" required
                            autofocus>
                        <datalist id="articulos">
                            @foreach ($articulos as $articulo)
                                <option class="prom{{ $articulo->Codigo }}" value="{{ $articulo->Codigo }}">
                                    {{ $articulo->Descripcion }}
                                </option>
                            @endforeach
                        </datalist>
                    </div>
                    <div class="col-auto ms-4">
                        <label>Precio</label>
                        <input style="width: 100px" class="form-control form-control" name="Precio" step="any"
                            type="number" placeholder="Precio" onkeypress="return event.keyCode != 13;" required>
                    </div>
                    <div class="col-auto ms-4">
                        <button class="input-submit btn btn-orange"> Agregar </button>
                    </div>
                </div>
            </form>
        </div>
    @endif

    <div class="container">
        <div class="d-flex justify-content-center">
            <div class="col-auto">
                @include('Alertas.Alertas')
            </div>
        </div>
    </div>

    {{-- Tabla  --}}
    <div class="content-table content-table-full card p-4">
        <table id="tblStock">
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Codigo</th>
                    <th>Articulo</th>
                    <th>Precio</th>
                    <th class="rounded-end"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($precios as $precio)
                    <tr>
                        <td>{{ $precio->Codigo }}</td>
                        <th>{{ $precio->Descripcion }}</th>
                        <td>{{ $precio->Precio }}</td>
                        <td>
                            <form id="delete-user" method="POST" class="d-inline"
                                action="{{ route('articuloslista.destroy', ['id' => $precio->IdDatPrecios]) }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn text-danger">
                                    <i class="bi bi-trash3-fill"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
