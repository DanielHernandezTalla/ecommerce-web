@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    {{-- Formulario  --}}
    <div class="card p-4" style="max-width: 700px; margin: auto;">

        <form action=" {{ route('codigospostales.store') }} " method="POST">
            @csrf

            <div class="mb-3">
                <label>Codigo postal</label>
                <input type="text" name="CodigoPostal" class="form-control" tabindex="1" value="{{ old('Descripcion') }}"
                    placeholder="Escribe el codigo postal" required autofocus>
            </div>

            <div class="mb-3">
                <label>Ciudad</label>
                <select name="IdCatCiudad" class="form-select" tabindex="1" required>
                    <option value="0" selected>Elige una ciudad</option>
                    @foreach ($ciudades as $ciudad)
                        <option value="{{ $ciudad->IdCatCiudades }}">{{ $ciudad->Descripcion }}</option>
                    @endforeach
                </select>
            </div>

            <div class="row mt-4">
                <div class="col-md-6 mt-3">
                    <a class="form-control btn submit-orange-outline"
                        href="{{ route('codigospostales.index') }}">Cancelar</a>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="submit" class="form-control btn submit-orange">
                </div>
            </div>
        </form>
    </div>
@endsection
