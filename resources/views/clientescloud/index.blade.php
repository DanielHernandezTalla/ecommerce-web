@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
        <a class="btn btn-orange rounded-pill" href="{{ route('clientescloud.create') }}">Crear cliente</a>
    </div>

    {{-- Tabla  --}}
    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Cliente</th>
                    <th>Nombre</th>
                    <th>Número cliente</th>
                    <th>Tipo de cliente</th>
                    <th>SHIP TO</th>
                    <th>BILL TO</th>
                    <th>Código envío</th>
                    <th>Código postal</th>
                    <th>Dirección</th>
                    <th>Locación</th>
                    <th>Ciudad</th>
                    <th>País</th>
                    <th>Centro de venta</th>
                    <th>Tipo de pago</th>
                    <th>Metódo de pago</th>
                    <th>Uso CFDI</th>

                    <th class="rounded-end"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clientes as $cliente)
                    <tr>
                        <td>{{ $cliente->ID_CLIENTE }}</td>
                        <td>{{ $cliente->NOMBRE }}</td>
                        <td>{{ $cliente->NUMERO_CLIENTE }}</td>
                        <td>{{ $cliente->TIPO_CLIENTE }}</td>
                        <td>{{ $cliente->SHIP_TO }}</td>
                        <td>{{ $cliente->BILL_TO }}</td>
                        <td>{{ $cliente->CODIGO_ENVIO }}</td>
                        <td>{{ $cliente->CODIGO_POSTAL }}</td>
                        <td>{{ $cliente->DIRECCION }}</td>
                        <td>{{ $cliente->LOCACION }}</td>
                        <td>{{ $cliente->PAIS }}</td>
                        <td>{{ $cliente->CIUDAD }}</td>
                        <td>{{ $cliente->Descripcion }}</td>
                        <td>{{ $cliente->NomTipoPago }}</td>
                        <td>{{ $cliente->NomMetododePago }}</td>
                        <td>{{ $cliente->NomCFDI }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="d-flex justify-content-center p-4">
        {{ $clientes->links() }}
    </div>
@endsection
