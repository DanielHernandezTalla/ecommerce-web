@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    {{-- Formulario  --}}
    <div class="card p-4" style="max-width: 768px; margin: auto;">

        <form action=" {{ route('clientescloud.store') }} " method="POST">
            @csrf

            <div class="mb-3">
                <label>Cliente Oracle</label>
                <select name="IDCATCLIENTESCLOUD" class="form-select" tabindex="1">
                    <option value="0" selected>Elige un cliente</option>
                    @foreach ($clientes as $cliente)
                        <option value="{{ $cliente->IDCATCLIENTESCLOUD }}"
                            {{ $cliente->IDCATCLIENTESCLOUD == old('IDCATCLIENTESCLOUD') ? 'selected' : '' }}>
                            {{ $cliente->NOMBRE }} -
                            {{ $cliente->DIRECCION }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label>Tipo de pago</label>
                    <select name="IdTipoPago" class="form-select" tabindex="1">
                        <option value="0" selected>Elige un tipo de pago</option>
                        @foreach ($tipoPago as $pago)
                            <option value="{{ $pago->IdTipoPago }}"
                                {{ $pago->IdTipoPago == old('IdTipoPago') ? 'selected' : '' }}>{{ $pago->NomTipoPago }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6 mb-3">
                    <label>Método de pago</label>
                    <select name="IdCatMetododePago" class="form-select" tabindex="1">
                        <option value="0" selected>Elige un tipo de pago</option>
                        @foreach ($metodoPago as $pago)
                            <option value="{{ $pago->IdCatMetododePago }}"
                                {{ $pago->IdCatMetododePago == old('IdCatMetododePago') ? 'selected' : '' }}>
                                {{ $pago->NomMetododePago }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label>Centro de venta</label>
                    <select name="IdDatCentroVenta" class="form-select" tabindex="1">
                        <option value="0" selected>Elige un centro de venta</option>
                        @foreach ($centroVenta as $centro)
                            <option value="{{ $centro->IdDatCentroVenta }}"
                                {{ $centro->IdDatCentroVenta == old('IdDatCentroVenta') ? 'selected' : '' }}>
                                {{ $centro->Descripcion }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6 mb-3">
                    <label>Uso CFDI</label>
                    <select name="IdCatUsoCFDI" class="form-select" tabindex="1">
                        <option value="0" selected>Elige un uso CFDI</option>
                        @foreach ($usocfdi as $cfdi)
                            <option value="{{ $cfdi->IdCatUsoCFDI }}"
                                {{ $cfdi->IdCatUsoCFDI == old('IdCatUsoCFDI') ? 'selected' : '' }}>
                                {{ $cfdi->NomCFDI }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-6 mt-3">
                    <a class="form-control btn submit-orange-outline" href="{{ route('clientescloud.index') }}">Cancelar</a>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="submit" class="form-control btn submit-orange">
                </div>
            </div>
        </form>
    </div>
@endsection
