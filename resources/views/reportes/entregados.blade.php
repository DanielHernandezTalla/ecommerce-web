@extends('layouts.app')

@section('content')
    {{-- Titulo y botones para seleccionar los diferentes estados de las compras --}}
    <div class="container-title pb-4">
        @include('components.title')
        {{-- <form class="row" id="formPrecios" action="{{ route('reporte.ventasprint') }}" method="GET"> --}}
        <div class="d-flex align-items-end">
            <form class="row align-items-end" id="formPrecios" action="{{ route('reporte.ventas') }}" method="GET">
                <div class="col-auto">
                    <input type="hidden" class="idPagination" value="&idCentroVenta={{ $idCentroVenta }}">
                    <input type="hidden" class="idPagination" value="&fechaStart={{ $fechaStart }}">
                    <input type="hidden" class="idPagination" value="&fechaEnd={{ $fechaEnd }}">
                    <label>Fecha inicio</label>
                    <input type="date" name="fechaStart" class="form-control" value="{{ $fechaStart }}">
                </div>
                <div class="col-auto">
                    <label>Fecha fin</label>
                    <input type="date" name="fechaEnd" class="form-control" value="{{ $fechaEnd }}">
                </div>
                <div class="col-auto">
                    <select class="form-select" name="idCentroVenta" id="idCentroVenta" required>
                        <option value="0">Seleccione Centro de Ventas</option>
                        @foreach ($centrosVenta as $centroVenta)
                            <option {!! $idCentroVenta == $centroVenta->IdDatCentroVenta ? 'selected' : '' !!} value="{{ $centroVenta->IdDatCentroVenta }}">
                                {{ $centroVenta->Descripcion }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-auto">
                    <button class="btn btn-orange">
                        Buscar
                    </button>
                </div>
            </form>
            <form class="row" id="formPrecios" action="{{ route('reporte.ventasprint') }}" method="GET">
                <div class="col-auto">
                    <input type="hidden" name="idCentroVenta" value="{{ $idCentroVenta }}">
                    <input type="hidden" name="fechaStart" value="{{ $fechaStart }}">
                    <input type="hidden" name="fechaEnd" value="{{ $fechaEnd }}">
                </div>
                <div class="col-auto">
                    <button class="btn btn-blue">
                        Imprimir <i class="bi bi-printer"></i>
                    </button>
                </div>
            </form>
        </div>


    </div>

    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Cliente</th>
                    <th>Importe</th>
                    <th>Ciudad</th>
                    <th>Hora</th>
                    <th class="rounded-end">Fecha Pedido</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pedidosEntregados as $pedidosEntregado)
                    <tr>
                        <td>{{ $pedidosEntregado->Cliente->Nombre }}</td>
                        <td>${{ $pedidosEntregado->Importe }}</td>
                        <td>{{ $pedidosEntregado->Ciudad }}</td>
                        <td>{{ \Carbon\Carbon::parse($pedidosEntregado->Fecha_Pedido)->format('h:i A') }}
                        <td>{{ ucfirst(\Carbon\Carbon::parse($pedidosEntregado->Fecha_Pedido)->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}
                        </td>
                        {{-- <td>{{ $pedidosEntregado->Fecha_Pedido }}</td> --}}
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="d-flex justify-content-center p-4">
        {{ $pedidosEntregados->links() }}
    </div>
@endsection
