@extends('layouts.app')

@section('content')
    {{-- Titulo y botones para seleccionar los diferentes estados de las compras --}}
    <div class="container-title pb-4">
        @include('components.title')
        <div class="d-flex align-items-end">
            <form class="row align-items-end" id="formPrecios" action="{{ route('reporte.concentrado') }}" method="GET">
                <div class="col-auto">
                    <input type="hidden" class="idPagination" value="&idCentroVenta={{ $idCentroVenta }}">
                    <input type="hidden" class="idPagination" value="&fechaStart={{ $fechaStart }}">
                    <input type="hidden" class="idPagination" value="&fechaEnd={{ $fechaEnd }}">
                    <label>Fecha inicio</label>
                    <input type="date" name="fechaStart" class="form-control" value="{{ $fechaStart }}">
                </div>
                <div class="col-auto">
                    <label>Fecha fin</label>
                    <input type="date" name="fechaEnd" class="form-control" value="{{ $fechaEnd }}">
                </div>
                <div class="col-auto">
                    <select class="form-select" name="idCentroVenta" id="idCentroVenta" required>
                        <option value="0">Seleccione Centro de Ventas</option>
                        @foreach ($centrosVenta as $centroVenta)
                            <option {!! $idCentroVenta == $centroVenta->IdDatCentroVenta ? 'selected' : '' !!} value="{{ $centroVenta->IdDatCentroVenta }}">
                                {{ $centroVenta->Descripcion }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-auto">
                    <button class="btn btn-orange">
                        Buscar
                    </button>
                </div>
            </form>
            <form class="row" id="formPrecios" action="{{ route('reporte.concentradoprint') }}" method="GET">
                <div class="col-auto">
                    <input type="hidden" name="idCentroVenta" value="{{ $idCentroVenta }}">
                    <input type="hidden" name="fechaStart" value="{{ $fechaStart }}">
                    <input type="hidden" name="fechaEnd" value="{{ $fechaEnd }}">
                </div>
                <div class="col-auto">
                    <button class="btn btn-blue">
                        Imprimir <i class="bi bi-printer"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>

    <div class="row g-4 mb-4">
        <div class="col-md-6 col-xxl-4">
            <div class="card p-4 d-flex flex-row justify-content-between align-items-center">
                <div>
                    <p class="fw-bold m-0" style="color: #475569">Total de Tickets:</p>
                    <span class="text-secondary"> {{ isset($totalTickets) ? $totalTickets : 0 }} ventas</span>
                </div>
                <div class="d-flex justify-content-center align-items-center rounded"
                    style="width: 48px; height: 48px; background: #06b6d4;">
                    <p class="fs-3 m-0 line-center text-white">
                        <i class="bi bi-graph-up-arrow"></i>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xxl-4">
            <div class="card p-4 d-flex flex-row justify-content-between align-items-center">
                <div class="">
                    <p class="fw-bold m-0" style="color: #475569">Ingreso de envios:</p>
                    <span
                        class="text-secondary">${{ isset($costo_Envio) ? number_format($costo_Envio, 2, '.', ',') : 0 }}</span>
                </div>
                <div class="d-flex justify-content-center align-items-center rounded"
                    style="width: 48px; height: 48px; background: #64748b;">
                    <p class="fs-3 m-0 line-center text-white">
                        <i class="bi bi-cart2"></i>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xxl-4">
            <div class="card p-4 d-flex flex-row justify-content-between align-items-center">
                <div class="">
                    <p class="fw-bold m-0" style="color: #475569">Ingreso por ventas:</p>
                    <span class="text-secondary">${{ isset($importe) ? number_format($importe, 2, '.', ',') : 0 }}</span>
                </div>
                <div class="d-flex justify-content-center align-items-center rounded"
                    style="width: 48px; height: 48px; background: #059669;">
                    <p class="fs-3 m-0 line-center text-white">
                        <i class="bi bi-cash-coin"></i>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Código</th>
                    <th>Nombre</th>
                    <th>Cantidad</th>
                    <th class="rounded-end">Importe</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pedidosEntregados as $pedidosEntregado)
                    <tr>
                        <td>{{ $pedidosEntregado->Codigo }}</td>
                        <td>{{ $pedidosEntregado->DesCorta }}</td>
                        <td>{{ number_format($pedidosEntregado->Cantidad, 2, '.', ',') }}</td>
                        <td class="text-end pe-4">${{ $pedidosEntregado->Importe }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
