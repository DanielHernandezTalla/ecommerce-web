@extends('layouts.app')

@section('content')
    {{-- Titulo y botones para seleccionar los diferentes estados de las compras --}}
    <div class="container-title pb-4 d-flex flex-column align-items-start">
        @include('components.title')
        {{-- <form class="row" id="formPrecios" action="{{ route('reporte.ventasprint') }}" method="GET"> --}}
        <div class="d-flex align-items-end">
            <form class="row align-items-end" id="formPrecios" action="{{ route('reporte.ventasfallidas') }}" method="GET">
                <div class="col-auto mb-4">
                    <input type="hidden" class="idPagination" value="&idCentroVenta={{ $idCentroVenta }}">
                    <input type="hidden" class="idPagination" value="&fechaStart={{ $fechaStart }}">
                    <input type="hidden" class="idPagination" value="&fechaEnd={{ $fechaEnd }}">
                    <input type="hidden" class="idPagination" value="&statusVente={{ $statusVente }}">
                    <label>Fecha inicio</label>
                    <input type="date" name="fechaStart" class="form-control" value="{{ $fechaStart }}">
                </div>
                <div class="col-auto mb-4">
                    <label>Fecha fin</label>
                    <input type="date" name="fechaEnd" class="form-control" value="{{ $fechaEnd }}">
                </div>
                <div class="col-auto mb-4">
                    <select class="form-select" name="idCentroVenta" id="idCentroVenta" required>
                        <option value="0">Seleccione Centro de Ventas</option>
                        @foreach ($centrosVenta as $centroVenta)
                            <option {!! $idCentroVenta == $centroVenta->IdDatCentroVenta ? 'selected' : '' !!} value="{{ $centroVenta->IdDatCentroVenta }}">
                                {{ $centroVenta->Descripcion }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-auto mb-4">
                    <select class="form-select" name="statusVente" id="statusVente" required>
                        <option value="0">Seleccione Status Venta</option>
                        <option {{ $statusVente == 1 ? 'selected' : '' }} value="1">Completado</option>
                        <option {{ $statusVente == 2 ? 'selected' : '' }} value="2">Cancelado</option>
                        <option {{ $statusVente == 3 ? 'selected' : '' }} value="3">Fallido</option>
                    </select>
                </div>
                <div class="col-auto mb-4">
                    <button class="btn btn-orange">
                        Buscar
                    </button>
                </div>
            </form>
            <form class="row" id="formPrecios" action="{{ route('reporte.ventasfallidasprint') }}" method="GET">
                <div class="col-auto">
                    <input type="hidden" name="idCentroVenta" value="{{ $idCentroVenta }}">
                    <input type="hidden" name="fechaStart" value="{{ $fechaStart }}">
                    <input type="hidden" name="fechaEnd" value="{{ $fechaEnd }}">
                </div>
                <div class="col-auto mb-4">
                    <button class="btn btn-blue">
                        Imprimir <i class="bi bi-printer"></i>
                    </button>
                </div>
            </form>
        </div>


    </div>

    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Venta</th>
                    <th>Cliente</th>
                    <th>Importe</th>
                    <th>Ciudad</th>
                    <th>Fecha Pedido</th>
                    <th class="rounded-end">Status venta</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pedidosEntregados as $pedidosEntregado)
                    <tr>
                        <td>{{ $pedidosEntregado->IdDatVentas }}</td>
                        <td>{{ $pedidosEntregado->Cliente->Nombre }}</td>
                        <td>${{ $pedidosEntregado->Importe }}</td>
                        <td>{{ $pedidosEntregado->Ciudad }}</td>
                        <td>{{ ucfirst(\Carbon\Carbon::parse($pedidosEntregado->Fecha_Pedido)->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}
                        </td>
                        <td>
                            <span class="px-4 py-0 rounded-pill border d-block"
                                style="color: #b3b3b3; white-space: nowrap; width: 150px; line-height: 2rem;">
                                @if ($pedidosEntregado->IdDatPagos == null)
                                    <i class="bi bi-circle-fill" style="color: #b3b3b3; font-size: 14px;"></i> Fallido
                                @elseif ($pedidosEntregado->IdDatPagos != null && $pedidosEntregado->Status_Pedido == 0)
                                    <i class="bi bi-circle-fill" style="color: #e9c666; font-size: 14px;"></i> Cancelado
                                @else
                                    <i class="bi bi-circle-fill" style="color: #4cc077; font-size: 14px;"></i> Completada
                                @endif
                            </span>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="d-flex justify-content-center p-4">
        {{ $pedidosEntregados->links() }}
    </div>
@endsection
