@extends('layouts.pdf')

@section('content')
    <div style="padding: 16px 32px">
        <div style="padding: 16px;">
            <p style="text-align: center; font-weight: bold;">ALIMENTOS KOWI SA DE CV</p>
            <p style="text-align: center; font-weight: bold;">AKO971007558</p>
            <p style="text-align: center; font-weight: bold;">CARRETERA FEDERAL MEXICO-NOGALES KM 1788</p>
            <p style="text-align: center; font-weight: bold;">NAVOJOA, SONORA C.P. 85230</p>
        </div>
        <p style="font-size: 14px; line-height: 20px;">NOMBRE: {{ $pedidosEnviados[0]->Cliente->Nombre }}</p>
        <p style="font-size: 14px; line-height: 20px;">DIRECCION: {{ $pedidosEnviados[0]->ClienteDir->Direccion }}</p>
        <p style="font-size: 14px; line-height: 20px;">COLONIA: {{ $pedidosEnviados[0]->ClienteDir->Colonia }}</p>
        <p style="font-size: 14px; line-height: 20px;">TELEFONO: {{ $pedidosEnviados[0]->Cliente->Telefono }}</p>
        <p style="font-size: 14px; line-height: 20px;">CIUDAD: {{ $pedidosEnviados[0]->ClienteDir->Ciudad }}</p>
        {{-- <p style="font-size: 14px; line-height: 20px;">CORREO: {{ $pedidosEnviados[0]->Cliente->Email }}</p> --}}
        <p style="font-size: 14px; line-height: 20px;">FECHA:
            {{ ucfirst(\Carbon\Carbon::parse($pedidosEnviados[0]->Fecha_Pago)->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}
            {{ date('H:i:s', strtotime($pedidosEnviados[0]->Fecha_Pago)) }}</p>
        <p style="font-size: 14px; line-height: 20px;">TICKET: <b>{{ $pedidosEnviados[0]->IdDatVentas }}</b> </p>

        <br>
        <br>
        <p style="display: none">{{ $subtotal = 0 }}</p>
        <table style="width: 100%;">
            <thead>
                <tr>
                    <th style="text-align: left; background: #f1f5f9; padding: 4px 8px ;">ARTICULO</th>
                    <th style="text-align: left; background: #f1f5f9; padding: 4px 8px ;">CANTIDAD</th>
                    <th style="text-align: left; background: #f1f5f9; padding: 4px 8px ;">PRECIO</th>
                    <th style="text-align: left; background: #f1f5f9; padding: 4px 8px ;">IMPORTE</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pedidosEnviados[0]->DetallesVentas as $index => $item)
                    <tr>
                        <td style="padding: 4px 8px ;">{{ str_pad(substr($item->DesCorta, 0, 16), 16) }}</td>
                        <td style="padding: 4px 8px ;">{{ str_pad(number_format($item->CantidadSecundario, 2), 7) }}</td>
                        <td style="padding: 4px 8px ;">{{ str_pad(number_format($item->Precio, 2), 7) }}</td>
                        <td style="padding: 4px 8px ;">{{ number_format($item->Importe, 2) }} </td>
                        {{ $subtotal += $item->Importe }}
                    </tr>
                @endforeach
            </tbody>
        </table>

        <p style="text-align: right; font-size: 14px;">Subtotal:
            <span style="display: inline-block; width: 64px; padding-top: 8px;">${{ number_format($subtotal, 2) }}</span>
        </p>
        <p style="text-align: right; font-size: 14px;">ENVIO:
            <span
                style="display: inline-block; width: 64px; padding-top: 8px;">${{ $pedidosEnviados[0]->Costo_Envio }}</span>
        </p>
        <p style="text-align: right; font-size: 14px;">TOTAL:
            <span
                style="display: inline-block; width: 64px; font-weight: bold; padding-top: 8px;">${{ $pedidosEnviados[0]->Importe_Pago }}</span>
        </p>
        <br>
        <br>
        <p style="text-align: center; font-size: 12px; line-height: 24px">¡ALTA CALIDAD EN CARNE DE CERDO!</p>
        <p style="text-align: center; font-size: 12px; line-height: 24px">WWW.KOWI.COM.MX</p>
        <p style="text-align: center; font-size: 12px; line-height: 24px">¡ GRACIAS POR SU COMPRA=) !</p>
    </div>
@endsection
