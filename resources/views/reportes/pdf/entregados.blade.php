@extends('layouts.pdf')

@section('content')
    <h4>{{ $title }}</h4>

    <h5 class="date">
        <b>Fecha impresión reporte: </b>
        {{ ucfirst(\Carbon\Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}
    </h5>


    <h5 class="date"><b>Centro de venta:</b>
        {{ count($centrosVenta) == 1 ? $centrosVenta[0]->Descripcion : 'Todos los centros de venta' }}</h5>
    {{-- @if (count($centrosVenta) == 1)
    @endif --}}


    @if (!$fechaStart && !$fechaEnd)
        <h5 class="date"><b>Fecha: </b>Todas las ventas</h5>
    @endif

    @if ($fechaStart)
        <h5 class="date"><b>Fecha inicio:</b>
            {{ ucfirst(\Carbon\Carbon::parse($fechaStart)->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}</h5>
    @else
        <h5 class="date"><b>Fecha inicio: </b>Desde el inicio</h5>
    @endif

    @if ($fechaEnd)
        <h5 class="date"><b>Fecha fin:</b>
            {{ ucfirst(\Carbon\Carbon::parse($fechaEnd)->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}</h5>
    @else
        <h5 class="date"><b>Fecha fin:</b>
            {{ ucfirst(\Carbon\Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}</h5>
    @endif

    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">#</th>
                    <th>Cliente</th>
                    <th>Importe</th>
                    <th>Ciudad</th>
                    <th colspan="2" class="rounded-end">Fecha Pedido</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pedidosEntregados as $key => $pedidosEntregado)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $pedidosEntregado->Cliente->Nombre }}</td>
                        <td>${{ $pedidosEntregado->Importe }}</td>
                        <td>{{ $pedidosEntregado->Ciudad }}</td>
                        <td colspan="2">
                            {{ ucfirst(\Carbon\Carbon::parse($pedidosEntregado->Fecha_Pedido)->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="2" style="text-align: right">Total Importe: </td>
                    <td>${{ number_format($total, 2, '.', ',') }}</td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
