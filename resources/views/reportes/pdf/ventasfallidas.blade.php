@extends('layouts.pdf')

@section('content')
    <h4>{{ $title }}</h4>

    <h5 class="date">
        <b>Fecha impresión reporte: </b>
        {{ ucfirst(\Carbon\Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}
    </h5>


    <h5 class="date"><b>Centro de venta:</b>
        {{ count($centrosVenta) == 1 ? $centrosVenta[0]->Descripcion : 'Todos los centros de venta' }}</h5>

    @if (!$fechaStart && !$fechaEnd)
        <h5 class="date"><b>Fecha: </b>Todas las ventas</h5>
    @endif

    @if ($fechaStart)
        <h5 class="date"><b>Fecha inicio:</b>
            {{ ucfirst(\Carbon\Carbon::parse($fechaStart)->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}</h5>
    @else
        <h5 class="date"><b>Fecha inicio: </b>Desde el inicio</h5>
    @endif

    @if ($fechaEnd)
        <h5 class="date"><b>Fecha fin:</b>
            {{ ucfirst(\Carbon\Carbon::parse($fechaEnd)->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}</h5>
    @else
        <h5 class="date"><b>Fecha fin:</b>
            {{ ucfirst(\Carbon\Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}</h5>
    @endif

    <br>
    <h5 class="date"><b>Completados: </b>{{ isset($completados) ? $completados : 0 }}</h5>
    <h5 class="date"><b>Cancelados: </b>{{ isset($cancelados) ? $cancelados : 0 }}</h5>
    <h5 class="date"><b>Fallidos: </b>{{ isset($fallidos) ? $fallidos : 0 }} </h5>

    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Venta</th>
                    <th colspan="3">Cliente</th>
                    <th>Importe</th>
                    <th>Ciudad</th>
                    <th colspan="3">Fecha Pedido</th>
                    <th class="rounded-end">Estatus venta</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pedidosEntregados as $pedidosEntregado)
                    <tr>
                        <td style="line-height: 16px">{{ $pedidosEntregado->IdDatVentas }}</td>
                        <td style="line-height: 16px" colspan="3">{{ $pedidosEntregado->Cliente->Nombre }}</td>
                        <td style="line-height: 16px">${{ $pedidosEntregado->Importe }}</td>
                        <td style="line-height: 16px">{{ $pedidosEntregado->Ciudad }}</td>
                        <td style="line-height: 16px" colspan="3">
                            {{ ucfirst(\Carbon\Carbon::parse($pedidosEntregado->Fecha_Pedido)->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}
                        </td>
                        <td style="line-height: 16px">
                            @if ($pedidosEntregado->IdDatPagos == null)
                                Fallido
                            @elseif ($pedidosEntregado->IdDatPagos != null && $pedidosEntregado->Status_Pedido == 0)
                                Cancelado
                            @else
                                Completada
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
