@extends('layouts.pdf')

@section('content')
    <h4>{{ $title }}</h4>

    <h5 class="date">
        <b>Fecha impresión reporte: </b>
        {{ ucfirst(\Carbon\Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}
    </h5>


    <h5 class="date"><b>Centro de venta:</b>
        {{ count($centrosVenta) == 1 ? $centrosVenta[0]->Descripcion : 'Todos los centros de venta' }}</h5>

    @if (!$fechaStart && !$fechaEnd)
        <h5 class="date"><b>Fecha: </b>Todas las ventas</h5>
    @endif

    @if ($fechaStart)
        <h5 class="date"><b>Fecha inicio:</b>
            {{ ucfirst(\Carbon\Carbon::parse($fechaStart)->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}</h5>
    @else
        <h5 class="date"><b>Fecha inicio: </b>Desde el inicio</h5>
    @endif

    @if ($fechaEnd)
        <h5 class="date"><b>Fecha fin:</b>
            {{ ucfirst(\Carbon\Carbon::parse($fechaEnd)->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}</h5>
    @else
        <h5 class="date"><b>Fecha fin:</b>
            {{ ucfirst(\Carbon\Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}</h5>
    @endif

    <br>

    <h5 class="date"><b>Total de Tickets: </b>{{ isset($totalTickets) ? $totalTickets : 0 }} ventas</h5>
    <h5 class="date"><b>Ingreso de envios: </b>${{ isset($costo_Envio) ? number_format($costo_Envio, 2, '.', ',') : 0 }}
    </h5>
    <h5 class="date"><b>Ingreso por ventas: </b>${{ isset($importe) ? number_format($importe, 2, '.', ',') : 0 }}</h5>

    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Código</th>
                    <th colspan="3">Nombre</th>
                    <th>Cantidad</th>
                    <th class="rounded-end">Importe</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pedidosEntregados as $pedidosEntregado)
                    <tr>
                        <td style="line-height: 16px">{{ $pedidosEntregado->Codigo }}</td>
                        <td style="line-height: 16px" colspan="3">{{ $pedidosEntregado->DesCorta }}</td>
                        <td style="line-height: 16px">{{ number_format($pedidosEntregado->Cantidad, 2, '.', ',') }}</td>
                        <td style="line-height: 16px" class="text-end pe-4">${{ $pedidosEntregado->Importe }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
