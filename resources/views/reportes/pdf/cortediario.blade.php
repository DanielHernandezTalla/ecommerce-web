@extends('layouts.pdf')

@section('content')
    <h4>{{ $title }}</h4>

    <h5 class="date">{{ $ecommerce }}</h5>
    <h5 class="date">
        <b>Fecha reporte: </b>
        <span>
            {{ ucfirst(\Carbon\Carbon::parse($fecha)->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}
        </span>
    </h5>
    <h5 class="date">
        <b>Fecha impresión reporte: </b>
        {{ ucfirst(\Carbon\Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}
    </h5>


    {{-- <p class="date">
        <b>Fecha impresión reporte: </b>
        {{ ucfirst(\Carbon\Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}
    </p>
    <p class="date">
        <b>Fecha reporte: </b>
        {{ ucfirst(\Carbon\Carbon::parse($fecha)->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}
    </p> --}}

    <p class="d-none">{{ $total = 0 }}</p>
    <div class="content-table card p-4">
        @foreach ($pedidos as $pedido)
            @if (count($pedido) > 0)
                <table>
                    <thead class="table-head">
                        <tr>
                            <th class="rounded-start">Codigo</th>
                            <th>Articulo</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th class="rounded-end">Importe</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pedido as $item)
                            @if (count($item['cliente']->Ventas) > 0)
                                <p class="d-none">{{ $acumulador = 0 }} </p>
                                <p class="d-none">{{ $acumuladorCostoEnvio = 0 }} </p>
                                <tr class="bg-gray-light">
                                    <td colspan="3" class="bg-light">
                                        <div class="">Cliente: {{ $item['cliente']->NOMBRE }}</div>
                                    </td>
                                    <td colspan="2" class="bg-light">
                                        <div class="text-end">Pedido Oracle {{ $item['source'] }}</div>
                                    </td>
                                </tr>
                                @foreach ($item['cliente']->Ventas as $venta)
                                    <tr>
                                        <td>{{ $venta->Codigo }}</td>
                                        <td>{{ $venta->Descripcion }}</td>
                                        <td>${{ number_format($venta->Precio, 2, '.', ',') }}</td>
                                        <td>{{ number_format($venta->Cantidad, 0, '.', ',') }}
                                            {{ $venta->Cantidad == 1 ? 'pieza' : 'piezas' }}</td>
                                        <td>${{ number_format($venta->Importe, 2, '.', ',') }}</td>
                                        <p class="d-none">{{ $acumulador += $venta->Importe }}</p>
                                        <p class="d-none">{{ $acumuladorCostoEnvio += $venta->Costo_Envio }}</p>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="5" class="text-end" style="line-height: 16px">Subtotal:
                                        ${{ number_format($total += $acumulador, 2, '.', ',') }}</td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="text-end" style="line-height: 16px">Costo envio:
                                        ${{ number_format($acumuladorCostoEnvio ? $item['cliente']->Total[0]->Costo_Envio : $item['cliente']->Costo_Envio, 2, '.', ',') }}

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="text-end" style="line-height: 16px">Total:
                                        ${{ number_format($acumuladorCostoEnvio ? $item['cliente']->Total[0]->ImportePago : $total + $item['cliente']->Costo_Envio, 2, '.', ',') }}
                                    </td>
                                </tr>
                                <p class="d-none">{{ $total = 0 }}</p>
                                <p class="d-none">{{ $acumuladorCostoEnvio = 0 }}</p>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            @endif
        @endforeach
    </div>

    <p class="text-end" style="padding-top: 16px">
        <b>Subtotal General:</b>
        <span>${{ number_format($subtotalGeneral, 2, '.', ',') }}</span>
    </p>

    <p class="text-end" style="padding-top: 16px">
        <b>Total General:</b>
        <span>${{ number_format($totalGeneral, 2, '.', ',') }}</span>
    </p>
@endsection
