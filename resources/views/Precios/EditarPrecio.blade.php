<!-- Modal Editar Precio-->
<div class="modal fade" data-bs-backdrop="static" id="EditarPrecio{{ $precio->IdDatPrecios }}" tabindex="-1"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Actualizar Precio: {{ $precio->Descripcion }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="/admin/editarprecio/{{ $precio->IdDatPrecios }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row mb-3">
                        <div class="col-auto">
                            <input class="form-control" type="number" name="precio" id="precio" value="{{ $precio->Precio }}" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-bs-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-primary" value="Editar">
                </div>
            </form>
        </div>
    </div>
</div>