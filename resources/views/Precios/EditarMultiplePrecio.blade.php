@extends('layouts.app')

@section('content')
    <form action="guardarmultipleprecio/{{ $IdCatListaPrecio }}" method="POST">
        <input type="hidden" name="IdCatListaPrecio" id="IdCatListaPrecio" value="{{ $IdCatListaPrecio }}">
        @csrf
        <div class="container-title pb-4">
            @include('components.title')
            <div>
                <button class="btn btn-orange">
                    Guardar
                </button>
            </div>
        </div>

        <div class="content-table content-table-full card p-4">
            <table id="tblStock">
                <thead class="table-head">
                    <tr>
                        <th class="rounded-start">Codigo</th>
                        <th>Articulo</th>
                        <th>Precio Actual</th>
                        <th class="rounded-end">Nuevo Precio</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($precios as $precio)
                        <tr>
                            <td>{{ $precio->Codigo }}</td>
                            <td>{{ $precio->Descripcion }}</td>
                            <td>{{ $precio->Precio }}</td>
                            <td>
                                <input class="form-control" type="number" name="precioNuevo[{{ $precio->IdDatPrecios }}]"
                                    value="{{ $precio->Precio }}" required>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </form>
@endsection
