@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
        <form class="row" id="formPrecios" action="{{ route('precios.index') }}" method="GET">
            <div class="col-auto">
                <select class="form-select" name="IdDatCentroVenta" id="IdDatCentroVenta">
                    <option value="">Seleccione lista de precios</option>
                    @foreach ($listaprecios as $precio)
                        <option {!! $IdDatCentroVenta == $precio->IdCatListaPrecio ? 'selected' : '' !!} value="{{ $precio->IdCatListaPrecio }}">
                            {{ $precio->Descripcion }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-auto">
                <button class="btn btn-orange">
                    Buscar
                </button>
            </div>
        </form>
    </div>

    @if ($IdDatCentroVenta != 0)
        <form action="{{ route('precios.edit') }}" method="GET">

            @isset($IdDatCentroVenta)
                <input type="hidden" name="IdDatCentroVenta" id="IdDatCentroVenta" value="{{ $IdDatCentroVenta }}">
                <div class="container-title pb-4">
                    <div>
                        <input class="form-control" type="text" name="filtro" id="filtro" placeholder="Buscar articulo">
                    </div>
                    <div>
                        <button class="btn btn-orange">Actualizar precios</button>
                    </div>
                </div>
            @endisset
            <div class="container">
                <div class="d-flex justify-content-center">
                    <div class="col-auto">
                        @include('Alertas.Alertas')
                    </div>
                </div>
            </div>

            {{-- Tabla  --}}
            <div class="content-table content-table-full card p-4">
                <table id="tblStock">
                    <thead class="table-head">
                        <tr>
                            <th class="rounded-start">Codigo</th>
                            <th>Articulo</th>
                            <th>Precio</th>
                            <th class="rounded-end"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($precios as $precio)
                            <tr>
                                <td>{{ $precio->Codigo }}</td>
                                <th>{{ $precio->Descripcion }}</th>
                                <td>{{ $precio->Precio }}</td>
                                <td>
                                    <input class="form-check-input" type="checkbox" name="chkPrecio[]" id="chkPrecio"
                                        value="{{ $precio->Codigo }}">
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </form>
    @endif
@endsection
