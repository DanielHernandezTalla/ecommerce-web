@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
        <a class="btn btn-orange rounded-pill" href="{{ route('listadeprecios.create') }}">Crear lista de precios</a>
    </div>

    {{-- Tabla  --}}
    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">#</th>
                    <th>Lista de precios</th>
                    <th>Estatus</th>
                    <th class="rounded-end"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($listaPrecios as $precio)
                    <tr>
                        <td>{{ $precio->IdCatListaPrecio }}</td>
                        <td>{{ $precio->Descripcion }}</td>
                        <td style="color: rgb(22 163 74);">
                            <span class="px-4 py-0 rounded-pill border d-block"
                                style="color: #b3b3b3; white-space: nowrap; width: 100px; line-height: 2rem;">
                                @if ($precio->Status == 1)
                                    <i class="bi bi-circle-fill" style="color: #4cc077; font-size: 14px;"></i>
                                @else
                                    <i class="bi bi-circle-fill" style="color: #b3b3b3; font-size: 14px;"></i>
                                @endif

                                Activo
                            </span>
                        </td>
                        <td class="text-end">
                            <a class="btn text-warning" href="{{ route('listadeprecios.edit', ['lista' => $precio->IdCatListaPrecio]) }}">
                                <i class="bi bi-pencil-fill"></i>
                            </a>
                            <form id="delete-user" method="POST" class="d-inline" action="{{ route('listadeprecios.destroy', ['id' => $precio->IdCatListaPrecio]) }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn text-danger">
                                    <i class="bi bi-trash3-fill"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>    
    <div class="d-flex justify-content-center p-4">
        {{$listaPrecios->links()}}
    </div>
@endsection
