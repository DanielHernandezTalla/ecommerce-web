@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    {{-- Formulario  --}}
    <div class="card p-4" style="max-width: 700px; margin: auto;">

        <form action=" {{ route('imagenespromocioens.store') }} " method="POST"  method="POST" enctype="multipart/form-data">
            @csrf

            <div class="mb-3">
                <label>Imagen articulo</label>
                <input type="file" name="imagen" class="form-control" required autofocus placeholder="seleciona una imagen">
            </div>

            <div class="row mt-4">
                <div class="col-md-6 mt-3">
                    <a class="form-control btn submit-orange-outline" href="{{ route('imagenespromocioens.index') }}">Cancelar</a>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="submit" class="form-control btn submit-orange">
                </div>
            </div>
        </form>
    </div>
@endsection
