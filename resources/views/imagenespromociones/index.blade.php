@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
        <a class="btn btn-orange rounded-pill" href="{{ route('imagenespromocioens.create') }}">Agregar imagen</a>
    </div>

    {{-- Tabla  --}}
    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Id</th>
                    <th>Imagen</th>
                    <th class="rounded-end"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($imagenes as $key => $imagen)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $imagen->imagen }}</td>
                        <td>
                            {{-- <a class="btn text-warning" href="{{ route('imagenespromocioens.edit', ['imagen' => $imagen->id]) }}">
                                <i class="bi bi-pencil-fill"></i>
                            </a> --}}

                            <form id="delete-user" method="POST" class="d-inline" action="{{ route('imagenespromocioens.destroy', ['imagen' => $imagen->id]) }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn text-danger">
                                    <i class="bi bi-trash3-fill"></i>
                                </button>
                            </form>
                        </td>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
