<div>
    <h4>{{ $title }}</h4>
    @isset ($textSecondary)
        <p class="text-secondary fw-semibold">{{ $textSecondary }}</p>
    @endisset
    <p class="text-secondary fw-semibold">
        {{ ucfirst(\Carbon\Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}</p>
</div>
