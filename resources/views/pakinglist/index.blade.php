@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    {{-- Tabla  --}}
    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Id</th>
                    <th>Origen</th>
                    <th>Llegada</th>
                    <th>Status</th>
                    <th class="rounded-end"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($recepcion as $recepcionCV)
                    <tr>
                        <td>{{ $recepcionCV->IdCapRecepcion }}</td>
                        <td>{{ $recepcionCV->PackingList }}</td>
                        <td>{{ ucfirst(\Carbon\Carbon::parse($recepcionCV->FechaLlegada)->locale('es')->isoFormat('D \d\e MMMM \d\e\l Y, H\:mm')) }}
                        </td>
                        <td>{{ $recepcionCV->IdStatusRecepcion }}</td>
                        <td class="text-end">
                            <form class="d-inline" action="{{ route('pakinglist.index') }}">
                                <input type="hidden" name="idPaking" value="{{ $recepcionCV->IdCapRecepcion }}">
                                <input type="hidden" name="page" value="{{ $recepcion->currentPage() }}">

                                <button class="btn text-warning" data-bs-toggle="mensaje" title="Recepcionar">
                                    <i class="bi bi-file-earmark-text"></i>
                                </button>
                            </form>
                        </td>
                        @include('recepcion.ModalCancelarRecepcion')
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="d-flex justify-content-center p-4">
        {{-- {{ substr($recepcion->nextPageUrl(), -1) }} --}}
        {{ $recepcion->links() }}
    </div>


    @if (!empty($idRecepcion))

        <form class="detalle-products" action="{{ route('pakinglist.print') }}" method="GET">
            <input type="hidden" name="idRecepcion" value="{{ $idRecepcion }}">
            <div class="container-title py-4">
                <h4>Detalle de productos</h4>
                <button class="btn btn-blue">
                    Imprimir <i class="bi bi-printer"></i>
                </button>
            </div>
            <div class="content-table card p-4">
                <table>
                    <thead class="table-head">
                        <tr>
                            <th class="rounded-start">Origen</th>
                            <th>Paking list</th>
                            <th>Articulo</th>
                            <th>Cant Enviada</th>
                            <th>Cant Recepcionada</th>
                            <th class="rounded-end">Diferencia</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (empty($detalleRecepcion))
                            <tr>
                                <td colspan="6">Productos a Recepcionar</td>
                            </tr>
                        @else
                            @foreach ($detalleRecepcion as $recepcion)
                                @if (number_format($recepcion->CantEnviada, 2) - $recepcion->CantRecepcionada > 0)
                                    <tr>
                                        <td>{{ $recepcion->PackingList }}</td>
                                        <td>{{ $recepcion->CodArticulo }}</td>
                                        <td>{{ $recepcion->Descripcion }}</td>
                                        <td>{{ number_format($recepcion->CantEnviada, 2) }} KG</td>
                                        <td>{{ number_format($recepcion->CantRecepcionada, 2) }} KG</td>
                                        <td>{{ number_format($recepcion->CantEnviada - $recepcion->CantRecepcionada, 2) }}
                                            KG</td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </form>
    @endif
@endsection
