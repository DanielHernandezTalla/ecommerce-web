@extends('layouts.pdf')

@section('content')

    <h4>{{ $title }}</h4>

    <h5 class="date">
        <b>Fecha impresión reporte: </b>
        {{ ucfirst(\Carbon\Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}
    </h5>

    <h5 class="date"> <b>Fecha recepción: </b>
        {{ ucfirst(\Carbon\Carbon::parse($recepcion->FechaRecepcion)->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}
    </h5>
    <h5 class="date"> <b>Packing list: </b> {{ $recepcion->PackingList }} </h5>
    <h5 class="date"> <b>Almacen: </b> {{ $recepcion->Almacen }} </h5>

    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Codigo</th>
                    <th>Articulo</th>
                    <th>Cant Enviada</th>
                    <th>Cant Recepcionada</th>
                    <th class="rounded-end">Diferencia</th>
                </tr>
            </thead>
            <tbody>
                @if (empty($detalleRecepcion))
                    <tr>
                        <td colspan="6">Productos a Recepcionar</td>
                    </tr>
                @else
                    @foreach ($detalleRecepcion as $recepcion)
                        @if (number_format($recepcion->CantEnviada, 2) - $recepcion->CantRecepcionada > 0)
                            <tr>
                                <td>{{ $recepcion->CodArticulo }}</td>
                                <td>{{ $recepcion->Descripcion }}</td>
                                <td>{{ number_format($recepcion->CantEnviada, 2) }} KG</td>
                                <td>{{ number_format($recepcion->CantRecepcionada, 2) }} KG</td>
                                <td>{{ number_format($recepcion->CantEnviada - $recepcion->CantRecepcionada, 2) }}
                                    KG</td>
                            </tr>
                        @endif
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
@endsection
