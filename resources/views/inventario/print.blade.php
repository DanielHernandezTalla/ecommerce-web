@extends('layouts.pdf')

@section('content')
    <h4>{{ $title }}</h4>

    <h5 class="date">
        <b>Fecha reporte: </b>
        {{ ucfirst(\Carbon\Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')) }}
    </h5>

    <h5 class="date"><b>Total productos: </b>{{ count($productos) }}</h5>
    <h5 class="date"><b>Productos con inventario: </b>{{ $conInventario }}</h5>
    <h5 class="date"><b>Productos sin inventario: </b>{{ $sinInventario }}</h5>


    <div class="content-table content-table-full card p-4">
        <table id="tblStock">
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Código</th>
                    <th colspan="3">Descripción</th>
                    <th>Piezas</th>
                    <th class="text-wrap">Peso prom</th>
                    <th class="rounded-end">Stock</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($productos as $producto)
                    <tr>
                        <td>{{ $producto->Codigo }}</td>
                        <td colspan="3">{{ $producto->Descripcion }}</td>
                        <td>{{ number_format($producto->Stock / $producto->CantPesoProm, 2) }} </td>
                        <td>{{ ($producto->CantPesoProm >= 1 ? $producto->CantPesoProm : '0' . $producto->CantPesoProm) . ' ' . $producto->Unidad }}
                        </td>
                        <td>{{ number_format($producto->Stock, 2) }} KG </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
