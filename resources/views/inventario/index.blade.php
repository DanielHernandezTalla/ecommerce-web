@extends('layouts.app')

@section('content')

    <div class="container-title pb-4">
        @include('components.title')
        @if (!empty($centrosVenta))
            <div class="d-flex">
                <form class="row" id="formPrecios" action="{{ route('inventario.index') }}" method="GET">
                    <div class="col-auto">
                        <select class="form-select" name="idCentroVenta" id="idCentroVenta" required>
                            <option value="0">Seleccione Centro de Ventas</option>
                            @foreach ($centrosVenta as $centroVenta)
                                <option {!! $idCentroVenta == $centroVenta->IdDatCentroVenta ? 'selected' : '' !!} value="{{ $centroVenta->IdDatCentroVenta }}">
                                    {{ $centroVenta->Descripcion }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-auto">
                        <button class="btn btn-orange">
                            Buscar
                        </button>
                    </div>
                </form>
                @if ($idCentroVenta || $idCentroVenta != 0)
                    <form class="row" id="formPrecios" action="{{ route('inventario.print') }}" method="GET">
                        <div class="col-auto">
                            <input type="hidden" name="idCentroVenta" value="{{ $idCentroVenta }}">
                        </div>
                        <div class="col-auto">
                            <button class="btn btn-blue">
                                Imprimir <i class="bi bi-printer"></i>
                            </button>
                        </div>
                    </form>
                @endif
            </div>
        @else
            <form class="row" id="formPrecios" action="{{ route('inventario.print') }}" method="GET">
                <div class="col-auto">
                    <input type="hidden" name="idCentroVenta" value="{{ $idCentroVenta }}">
                </div>
                <div class="col-auto">
                    <button class="btn btn-blue">
                        Imprimir <i class="bi bi-printer"></i>
                    </button>
                </div>
            </form>
        @endif
    </div>

    <div class="container-title pb-4">
        {{-- @include('components.title') --}}
        <div class="">
            <input type="text" name="filtro" id="filtro" class="form-control" placeholder="Buscar por código">
        </div>

        <div>
            <label class="form-check-label fw-semibold text-secondary">Sin inventario</label>
            <label class="switch">
                <input id="filtro-check" type="checkbox">
                <span class="slider round"></span>
            </label>
        </div>
    </div>

    {{-- Tabla  --}}
    <div class="content-table content-table-full card p-4">
        <table id="tblStock">
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Código</th>
                    <th>Descripción</th>
                    <th>Piezas</th>
                    <th>Peso promedio</th>
                    <th class="rounded-end">Stock</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($productos as $producto)
                    <tr>
                        <td>{{ $producto->Codigo }}</td>
                        <td>{{ $producto->Descripcion }}</td>
                        <td>{{ $producto->Stock / $producto->CantPesoProm }} </td>
                        <td>{{ ($producto->CantPesoProm >= 1 ? $producto->CantPesoProm : '0' . $producto->CantPesoProm) . ' ' . $producto->Unidad }}
                        </td>
                        <td>{{ number_format($producto->Stock, 2) }} KG </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="text-secondary d-flex justify-content-end gap-4 pt-3">
        <p class="">Total productos: <b>{{ count($productos) }}</b></p>
        <p class="">Productos con inventario: <b>{{ $conInventario }}</b></p>
        <p class="">Productos sin inventario: <b>{{ $sinInventario }}</b></p>
    </div>
@endsection
