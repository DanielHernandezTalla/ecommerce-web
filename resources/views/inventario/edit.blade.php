@extends('layouts.app')

@section('content')
    {{-- Title --}}
    <div class="container-title pb-4">
        @include('components.title')
        @if (Auth::user()->IdCatTipoUser != '2')
            <form class="row" id="formPrecios" action="{{ route('inventario.edit') }}" method="GET">
                <div class="col-auto">
                    <select class="form-select" name="idCentroVenta" id="idCentroVenta" required>
                        <option value="0">Seleccione Centro de Ventas</option>
                        @foreach ($centrosVenta as $centroVenta)
                            <option {!! $idCentroVenta == $centroVenta->IdDatCentroVenta ? 'selected' : '' !!} value="{{ $centroVenta->IdDatCentroVenta }}">
                                {{ $centroVenta->Descripcion }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-auto">
                    <button class="btn btn-orange">
                        Buscar
                    </button>
                </div>
            </form>
        @endif
    </div>

    <form
        action="{{ route(Auth::user()->IdCatTipoUser == 2 ? 'ejecutivo.inventario.update' : 'inventario.update', ['id' => $idCentroVenta]) }}"
        method="POST">
        @csrf



        {{-- Search --}}
        <div class="container-title pb-4">
            <div class="">
                <input type="text" name="filtro" id="filtro" class="form-control" placeholder="Buscar por código"
                    onkeypress="return event.keyCode != 13;" autofocus>
            </div>
            <button class="input-submit btn btn-orange rounded-pill">Guardar cambios</button>
        </div>

        {{-- Alerts --}}
        <div class="container">
            <div class="d-flex justify-content-center">
                <div class="col-auto">
                    @include('Alertas.Alertas')
                </div>
            </div>
        </div>

        {{-- Tabla  --}}
        <div class="content-table content-table-full card p-4">
            <table id="tblStock">
                <thead class="table-head">
                    <tr>
                        <th class="rounded-start">Código</th>
                        <th>Descripción</th>
                        <th>Stock piezas</th>
                        <th>Peso promedio</th>
                        <th class="rounded-end">Stock kilos</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($productos as $producto)
                        <tr>
                            <td>{{ $producto->Codigo }}</td>
                            <td>{{ $producto->Descripcion }}</td>
                            <td>
                                <input class="input-cantidad-piezas form-control form-control-sm" min="0"
                                    max="1000" step="any" type="number" data-prom="{{ $producto->CantPesoProm }}"
                                    name="size[{{ $producto->Codigo }}]"
                                    value="{{ $producto->Stock / $producto->CantPesoProm }}"
                                    onkeypress="return event.keyCode != 13;" required>
                            </td>
                            <td>{{ ($producto->CantPesoProm >= 1 ? $producto->CantPesoProm : '0' . $producto->CantPesoProm) . ' ' . $producto->Unidad }}
                            </td>
                            <td>
                                {{-- <span>{{ $producto->Stock . ' KG' }}</span> --}}
                                <input class="input-cantidad-kilos form-control form-control-sm" min="0"
                                    max="1000" step="any" type="number" data-prom="{{ $producto->CantPesoProm }}"
                                    name="stock[{{ $producto->Codigo }}]" id="stock" value="{{ $producto->Stock }}"
                                    onkeypress="return event.keyCode != 13;" required>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </form>

    {{-- Calcular de piezas a kilos --}}
    <script>
        document.addEventListener("keyup", (e) => {
            changeValues(e);
        });

        document.addEventListener("change", (e) => {
            changeValues(e);
        });

        function changeValues(e) {
            if (e.target.matches('.input-cantidad-piezas')) {
                let value = e.target.value;

                let pesoProm = e.target.getAttribute('data-prom');
                e.target.parentNode.parentNode.querySelectorAll('input')[1].value = (value * pesoProm).toFixed(3);

                if (Number.isInteger(parseFloat(value)))
                    document.querySelector('.input-submit').classList.remove('disabled');
                else document.querySelector('.input-submit').classList.add('disabled');
            }
            if (e.target.matches('.input-cantidad-kilos')) {
                let value = e.target.value;

                let pesoProm = e.target.getAttribute('data-prom');
                e.target.parentNode.parentNode.querySelector('input').value = (value / pesoProm).toFixed(3);

                value = e.target.parentNode.parentNode.querySelector('input').value
                if (Number.isInteger(parseFloat(value)))
                    document.querySelector('.input-submit').classList.remove('disabled');
                else document.querySelector('.input-submit').classList.add('disabled');
            }
        }
    </script>
@endsection
