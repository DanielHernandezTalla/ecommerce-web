@extends('layouts.login')

@section('content')
    <main class="main d-flex align-items-center justify-content-center">

        {{-- Formulario  --}}
        <div class="p-4 login-form">
            <form class="pt-5 d-flex flex-column align-items-center" action="/Autenticarse" method="POST">
                @csrf
                {{ csrf_field() }}

                <div class="text-white text-center">
                    <h3><strong>Inicio de sesión</strong></h3>
                    <h5 style="font-style: italic;">Ingresa tus datos</h5>
                    <div class="mt-2">
                        @include('Alertas.alertas')
                    </div>
                </div>

                <div class="input-group pt-2">
                    <input id="username" type="text" placeholder="Usuario" class="form-control py-2" name="username"
                        value="{{ old('username') }}" required autofocus>
                </div>


                <div class="input-group pt-4">
                    <input id="idPassword" placeholder="Contraseña" id="password" type="password"
                        class="form-control text-size" name="password" required />
                </div>

                <div class="input-group pt-2">
                    <label>
                        <input id="idRemember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        Recordar tus datos
                    </label>
                </div>

                <button type="submit" class="mt-4 rounded-pill btn-signin">
                    <strong>Iniciar Sesión</strong>
                </button>
            </form>
        </div>
    </main>
@endsection
