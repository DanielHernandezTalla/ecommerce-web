@extends('layouts.app')

@section('content')
    {{-- Titulo y botones para seleccionar los diferentes estados de las compras --}}
    <div class="container-title pb-4">
        @include('components.title')
        <div class="">
            <a class="btn btn-orange-unselect rounded-pill" href="{{ route('pedidos.pendientes') }}">Pendientes</a>
            <a class="btn btn-orange rounded-pill" href="{{ route('pedidos.preparados') }}">Preparados</a>
            <a class="btn btn-orange-unselect rounded-pill" href="{{ route('pedidos.enviados') }}">Enviados</a>
            <a class="btn btn-orange-unselect rounded-pill" href="{{ route('pedidos.entregados') }}">Entregados</a>
        </div>
    </div>


    {{-- Tabla de preparados --}}
    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">#</th>
                    <th>Usuario</th>
                    <th>Correo</th>
                    <th>Teléfono</th>
                    <th>Fecha Pedido</th>
                    <th>Dirección</th>
                    <th>Importe</th>
                    <th>Primer pedido</th>
                    <th>Comentario</th>
                    <th class="rounded-end"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pedidosPreparados as $pedidoPreparado)
                    <tr>
                        <td>{{ $pedidoPreparado->IdDatVentas }}</td>
                        <td>{{ $pedidoPreparado->Cliente->Nombre }}</td>
                        <td>{{ $pedidoPreparado->Cliente->Email }}</td>
                        <td>{{ $pedidoPreparado->Cliente->Telefono }}</td>
                        <td>{{ \Carbon\Carbon::parse($pedidoPreparado->Fecha_Pedido)->format('d/m/Y') }}</td>
                        <td>{{ $pedidoPreparado->Direccion }}</td>
                        <td class="text-end">${{ number_format($pedidoPreparado->Importe, 2, '.', ',') }}</td>
                        <td>
                            <span class="px-4 py-0 rounded-pill border d-block"
                                style="color: #b3b3b3; white-space: nowrap; width: 100px; line-height: 2rem;">
                                @if ($pedidoPreparado->isFirst)
                                    <i class="bi bi-circle-fill" style="color: #4cc077; font-size: 14px;"></i> Si
                                @else
                                    <i class="bi bi-circle-fill" style="color: #b3b3b3; font-size: 14px;"></i> No
                                @endif
                            </span>
                        </td>
                        <td>{{ $pedidoPreparado->Comentario }}</td>
                        <td>
                            <button class="btn text-warning" data-bs-toggle="modal"
                                data-bs-target="#ModalPedido{{ $pedidoPreparado->IdDatVentas }}">
                                <i class="bi bi-pencil-fill"></i>
                            </button>
                            <a href="/ejecutivo/pedidos/print/{{ $pedidoPreparado->IdDatVentas }}"
                                class="btn text-warning">
                                <i class="bi bi-file-earmark-text"></i>
                            </a>
                            @include('Modals.ModalPedidoPreparado')
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    {{-- paginacion --}}
    <div class="d-flex justify-content-center p-4">
        {{ $pedidosPreparados->links() }}
    </div>
@endsection

<script>
    document.addEventListener('DOMContentLoaded', (e) => {

        let contenido = document.querySelector('tbody tr');

        Notification.requestPermission().then(resultado => {
            console.log('Respuesta: ', resultado)
        })

        if (Notification.permission === 'granted' && contenido)

            Notification.requestPermission().then(resultado => {
                const notification = new Notification('Ecommerce APP', {
                    icon: '/images/notification.png',
                    body: 'Tienes pedidos pendientes en ecommerce'
                });

                notification.onclick = function() {
                    window.open('/ejecutivo/pedidos/preparados')
                }
            })
    })
</script>
