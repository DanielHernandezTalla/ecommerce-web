@extends('layouts.app')

@section('content')
    {{-- Titulo y botones para seleccionar los diferentes estados de las compras --}}
    <div class="container-title pb-4">
        @include('components.title')
        <div class="">
            <a class="btn btn-orange rounded-pill" href="{{ route('pedidos.pendientes') }}">Pendientes</a>
            <a class="btn btn-orange-unselect rounded-pill" href="{{ route('pedidos.preparados') }}">Preparados</a>
            <a class="btn btn-orange-unselect rounded-pill" href="{{ route('pedidos.enviados') }}">Enviados</a>
            <a class="btn btn-orange-unselect rounded-pill" href="{{ route('pedidos.entregados') }}">Entregados</a>
        </div>
    </div>

    {{-- Tabla de pendientes --}}
    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">#</th>
                    <th>Usuario</th>
                    <th>Correo</th>
                    <th>Teléfono</th>
                    <th>Fecha Pedido</th>
                    <th>Dirección</th>
                    <th>Importe</th>
                    <th>Primer pedido</th>
                    <th>Comentario</th>
                    <th class="rounded-end"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pedidos as $pedido)
                    <tr>
                        <td>{{ $pedido->IdDatVentas }}</td>
                        <td>{{ $pedido->Cliente->Nombre }}</td>
                        <td>{{ $pedido->Cliente->Email }}</td>
                        <td>{{ $pedido->Cliente->Telefono }}</td>
                        <td>{{ \Carbon\Carbon::parse($pedido->Fecha_Pedido)->format('d/m/Y') }}</td>
                        <td>{{ $pedido->Direccion }}</td>
                        <td class="text-end">${{ number_format($pedido->Importe, 2, '.', ',') }}</td>
                        <td>
                            <span class="px-4 py-0 rounded-pill border d-block"
                                style="color: #b3b3b3; white-space: nowrap; width: 100px; line-height: 2rem;">
                                @if ($pedido->isFirst)
                                    <i class="bi bi-circle-fill" style="color: #4cc077; font-size: 14px;"></i> Si
                                @else
                                    <i class="bi bi-circle-fill" style="color: #b3b3b3; font-size: 14px;"></i> No
                                @endif
                            </span>
                        </td>
                        <td>{{ $pedido->Comentario }}</td>
                        <td>
                            <button class="btn text-warning" data-bs-toggle="modal"
                                data-bs-target="#ModalPedido{{ $pedido->IdDatVentas }}">
                                <i class="bi bi-pencil-fill"></i>
                            </button>
                            <a href="/ejecutivo/pedidos/print/{{ $pedido->IdDatVentas }}" class="btn text-warning">
                                <i class="bi bi-file-earmark-text"></i>
                            </a>
                            @include('Modals.ModalPedido')
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    {{-- paginacion --}}
    <div class="d-flex justify-content-center p-4">
        {{ $pedidos->links() }}
    </div>
@endsection

<script>
    document.addEventListener('DOMContentLoaded', (e) => {

        let contenido = document.querySelector('tbody tr');
        let notificationHour = localStorage.getItem("notification");
        let time = new Date().getTime();

        Notification.requestPermission().then(resultado => {
            console.log('Respuesta: ', resultado)
        })

        if (Notification.permission === 'granted' && contenido && (time - notificationHour) / 1000 / 60 > 2)

            Notification.requestPermission().then(resultado => {
                const notification = new Notification('Ecommerce APP', {
                    icon: '/images/notification.png',
                    body: 'Tienes pedidos pendientes en ecommerce'
                });

                notification.onclick = function() {
                    window.open('/ejecutivo/pedidos/pendientes')
                }

                localStorage.setItem("notification", new Date().getTime());
            })

        setInterval(() => {
            location.reload();
        }, 1000 * 60);
    })
</script>
