@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    {{-- Tabla  --}}
    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Id Pedido</th>
                    <th>Fecha Pedido</th>
                    <th>Importe</th>
                    <th>Usuario</th>
                    <th>Direccion</th>
                    <th>Telefono</th>
                    <th>Primer pedido</th>
                    <th>Comentario</th>
                    <th class="rounded-end"></th>
                </tr>
            </thead>
            <tbody>

                @foreach ($pedidos as $pedido)
                    <tr>
                        <td>{{ $pedido->IdDatVentas }}</td>
                        <td>{{ $pedido->Fecha_Pedido }}</td>
                        <td>${{ $pedido->Importe }}</td>
                        <td>{{ $pedido->Cliente->Nombre }}</td>
                        <td>{{ $pedido->Direccion }}</td>
                        <td>{{ $pedido->Cliente->Telefono }}</td>
                        <td>
                            <span class="px-4 py-0 rounded-pill border d-block"
                                style="color: #b3b3b3; white-space: nowrap; width: 100px; line-height: 2rem;">
                                @if ($pedido->isFirst)
                                    <i class="bi bi-circle-fill" style="color: #4cc077; font-size: 14px;"></i> Si
                                @else
                                    <i class="bi bi-circle-fill" style="color: #b3b3b3; font-size: 14px;"></i> No
                                @endif
                            </span>
                        </td>
                        <td>{{ $pedido->Comentario ?? 'Sin comentario' }}</td>
                        <td>
                            <button class="btn text-warning" data-bs-toggle="modal"
                                data-bs-target="#ModalPedido{{ $pedido->IdDatVentas }}">
                                <i class="bi bi-pencil-fill"></i>
                            </button>
                            <a href="/armador/pedidos/print/{{ $pedido->IdDatVentas }}" class="btn text-warning">
                                <i class="bi bi-file-earmark-text"></i>
                            </a>
                            @include('Modals.ModalArmador')
                        </td>

                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>

    {{-- Paginacion --}}
    <div class="d-flex justify-content-center p-4">
        {{ $pedidos->links() }}
    </div>
@endsection
