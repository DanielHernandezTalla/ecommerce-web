@extends('layouts.app')

@section('content')
    {{-- Titulo y botones para seleccionar los diferentes estados de las compras --}}
    <div class="container-title pb-4">
        @include('components.title')
        <div class="">
            <a class="btn btn-orange-unselect rounded-pill" href="{{ route('pedidos.pendientes') }}">Pendientes</a>
            <a class="btn btn-orange-unselect rounded-pill" href="{{ route('pedidos.preparados') }}">Preparados</a>
            <a class="btn btn-orange rounded-pill" href="{{ route('pedidos.enviados') }}">Enviados</a>
            <a class="btn btn-orange-unselect rounded-pill" href="{{ route('pedidos.entregados') }}">Entregados</a>
        </div>
    </div>

    {{-- Tabla de enviados --}}
    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">#</th>
                    <th>Usuario</th>
                    <th>Correo</th>
                    <th>Teléfono</th>
                    <th>Fecha Pedido</th>
                    <th>Dirección</th>
                    <th>Importe</th>
                    <th>Primer pedido</th>
                    <th>Comentario</th>
                    <th class="rounded-end"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pedidosEnviados as $pedidosEnviado)
                    <tr>
                        <td>{{ $pedidosEnviado->IdDatVentas }}</td>
                        <td>{{ $pedidosEnviado->Cliente->Nombre }}</td>
                        <td>{{ $pedidosEnviado->Cliente->Email }}</td>
                        <td>{{ $pedidosEnviado->Cliente->Telefono }}</td>
                        <td>{{ \Carbon\Carbon::parse($pedidosEnviado->Fecha_Pedido)->format('d/m/Y') }}</td>
                        <td>{{ $pedidosEnviado->Direccion }}</td>
                        <td class="text-end">${{ number_format($pedidosEnviado->Importe, 2, '.', ',') }}</td>
                        <td>
                            <span class="px-4 py-0 rounded-pill border d-block"
                                style="color: #b3b3b3; white-space: nowrap; width: 100px; line-height: 2rem;">
                                @if ($pedidosEnviado->isFirst)
                                    <i class="bi bi-circle-fill" style="color: #4cc077; font-size: 14px;"></i> Si
                                @else
                                    <i class="bi bi-circle-fill" style="color: #b3b3b3; font-size: 14px;"></i> No
                                @endif
                            </span>
                        </td>
                        <td>{{ $pedidosEnviado->Comentario }}</td>
                        <td>
                            <button class="btn text-warning" data-bs-toggle="modal"
                                data-bs-target="#ModalPedido{{ $pedidosEnviado->IdDatVentas }}">
                                <i class="bi bi-pencil-fill"></i>
                            </button>
                            <a href="/ejecutivo/pedidos/print/{{ $pedidosEnviado->IdDatVentas }}" class="btn text-warning">
                                <i class="bi bi-file-earmark-text"></i>
                            </a>
                            @include('Modals.ModalPedidoEnviado')
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    {{-- paginacion --}}
    <div class="d-flex justify-content-center p-4">
        {{ $pedidosEnviados->links() }}
    </div>
@endsection
