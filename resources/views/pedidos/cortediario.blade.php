@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
        <div class="d-flex align-items-end">
            <form class="row align-items-end" id="formPrecios" action="{{ route('cortediario') }}" method="GET">
                <div class="col-auto">
                    <label>Fecha venta</label>
                    <input class="search-input form-control" type="date" name="fecha">
                </div>
                <div class="col-auto">
                    <button class="btn btn-orange">
                        Buscar
                    </button>
                </div>
            </form>
            <form class="row" id="formPrecios" action="{{ route('reporte.cortediario') }}" method="GET">
                <div class="col-auto">
                    <input type="hidden" name="fecha" value="{{ $fecha }}">
                </div>
                <div class="col-auto">
                    <button class="btn btn-blue">
                        Imprimir <i class="bi bi-printer"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>

    <!-- Tabla  -->
    <p class="d-none">{{ $total = 0 }}</p>
    <div class="content-table content-table-full card p-4">
        @foreach ($pedidos as $pedido)
            @if (count($pedido) > 0)
                <table id="tblStock">
                    <thead class="table-head">
                        <tr>
                            <th class="rounded-start">Codigo</th>
                            <th>Articulo</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th class="rounded-end">Importe</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pedido as $item)
                            @if (count($item['cliente']->Ventas) > 0)
                                <p class="d-none">{{ $acumulador = 0 }} </p>
                                <p class="d-none">{{ $acumuladorCostoEnvio = 0 }} </p>
                                <tr>
                                    <td colspan="5" class="bg-light">
                                        <div class="d-flex justify-content-between pe-4">
                                            <div class="">Cliente: {{ $item['cliente']->NOMBRE }}</div>
                                            <div>Pedido Oracle {{ $item['source'] }}</div>
                                        </div>
                                    </td>
                                </tr>

                                @foreach ($item['cliente']->Ventas as $venta)
                                    <tr>
                                        <td>{{ $venta->Codigo }}</td>
                                        <td>{{ $venta->Descripcion }}</td>
                                        {{-- <td>{{ $venta->Costo_Envio }}</td> --}}
                                        <td>${{ number_format($venta->Precio, 2, '.', ',') }}</td>
                                        <td>{{ number_format($venta->Cantidad, 0, '.', ',') }}
                                            {{ $venta->Cantidad == 1 ? 'pieza' : 'piezas' }}</td>
                                        <td>${{ number_format($venta->Importe, 2, '.', ',') }}</td>
                                        <p class="d-none">{{ $acumulador += $venta->Importe }}</p>
                                        <p class="d-none">{{ $acumuladorCostoEnvio += $venta->Costo_Envio }}</p>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="5" class="text-end" style="line-height: 28px">Subtotal:
                                        ${{ number_format($total += $acumulador, 2, '.', ',') }}</td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="text-end" style="line-height: 28px">Costo envio:
                                        {{-- @dd($item['cliente']->Total[0]) --}}
                                        {{-- ${{ number_format($item['cliente']->Total[0]->Costo_Envio, 2, '.', ',') }} --}}
                                        {{-- ${{ number_format($total + ($acumuladorCostoEnvio ? $item['cliente']->Total[0]->Costo_Envio : $item['cliente']->Costo_Envio), 2, '.', ',') }} --}}
                                        ${{ number_format($acumuladorCostoEnvio ? $item['cliente']->Total[0]->Costo_Envio : $item['cliente']->Costo_Envio, 2, '.', ',') }}

                                    </td>
                                    {{-- <td colspan="5" class="text-end" style="line-height: 28px">Costo envio:
                                        ${{ number_format($acumuladorCostoEnvio, 2, '.', ',') }}</td> --}}
                                </tr>
                                <tr>
                                    <td colspan="5" class="text-end" style="line-height: 28px">Total:
                                        {{-- ${{ number_format($item['cliente']->Total[0]->ImportePago, 2, '.', ',') }} --}}
                                        {{-- {{ $item['cliente']->Total[0]->ImportePago }} --}}
                                        ${{ number_format($acumuladorCostoEnvio ? $item['cliente']->Total[0]->ImportePago : $total + $item['cliente']->Costo_Envio, 2, '.', ',') }}
                                        {{-- ${{ number_format($total + $item['cliente']->Costo_Envio, 2, '.', ',') }} --}}
                                    </td>
                                </tr>
                                <p class="d-none">{{ $total = 0 }}</p>
                                <p class="d-none">{{ $acumuladorCostoEnvio = 0 }}</p>
                                {{-- <p class="d-none">{{ $item['cliente']->Costo_Envio }}</p> --}}
                            @endif
                        @endforeach
                    </tbody>
                </table>
            @endif
        @endforeach
    </div>
    {{-- <p class="text-end fs-5 mt-4">
        <span style="font-weight: 500;">Subtotal:</span>
        <span class="ps-3 text-start"
            style="width: 120px; display: inline-block;">${{ number_format($totales['subTotal'], 2, '.', ',') }}</span>
    </p>
    <p class="text-end fs-5 mt-4">
        <span style="font-weight: 500;">Costo Envio:</span>
        <span class="ps-3 text-start"
            style="width: 120px; display: inline-block;">${{ number_format($totales['costoEnvio'], 2, '.', ',') }}</span>
    </p> --}}
    @if ($fecha > '2023-10-15')
        <p class="text-end fs-5 mt-4 m-0">
            <span class="fs-6" style="font-weight: 500;">Subtotal:</span>
            <span class="ps-3 text-start"
                style="width: 120px; display: inline-block;">${{ number_format($subtotalGeneral, 2, '.', ',') }}</span>
        </p>
    @endif
    <p class="text-end fs-5 {{ $fecha <= '2023-10-15' ? 'mt-4' : '' }}">
        <span style="font-weight: 500;">Total General:</span>
        <span class="ps-3 text-start"
            style="width: 120px; display: inline-block;">${{ number_format($totalGeneral, 2, '.', ',') }}</span>
    </p>

    {{-- Script para poner la fecha del query en el input search --}}
    <script>
        let urltotal = location.href;
        let query = new URL(urltotal);
        let searchParams = query.searchParams;

        document.querySelector('.search-input').value = searchParams.get('fecha');
    </script>
@endsection
