@extends('layouts.app')

@section('content')
    {{-- Datatables --}}
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.5.0/css/responsive.bootstrap5.min.css">


    {{-- Titulo y botones para seleccionar los diferentes estados de las compras --}}
    <div class="container-title pb-4">
        @include('components.title')
        <div class="">
            <a class="btn btn-orange-unselect rounded-pill" href="{{ route('pedidos.pendientes') }}">Pendientes</a>
            <a class="btn btn-orange-unselect rounded-pill" href="{{ route('pedidos.preparados') }}">Preparados</a>
            <a class="btn btn-orange-unselect rounded-pill" href="{{ route('pedidos.enviados') }}">Enviados</a>
            <a class="btn btn-orange rounded-pill" href="{{ route('pedidos.entregados') }}">Entregados</a>
        </div>
    </div>

    <div class="content-table card p-4">
        <table id="tabla" class="border-0">
            <thead class="table-head">
                <tr style="line-height: 2rem;">
                    <th class="rounded-start border-0">#</th>
                    <th class="border-0">Usuario</th>
                    <th class="border-0">Correo</th>
                    <th class="border-0">Fecha Pedido</th>
                    <th class="border-0">Primer pedido</th>
                    <th class="border-0">Importe</th>
                    <th class="border-0">Dirección</th>
                    <th class="rounded-end border-0">Comentario</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pedidos as $pedidosEntregado)
                    <tr style="line-height: 2rem;">
                        <td>{{ $pedidosEntregado->IdDatVentas }}</td>
                        <td>{{ $pedidosEntregado->Cliente->Nombre }}</td>
                        <td>{{ $pedidosEntregado->Cliente->Email }}</td>
                        <td>{{ \Carbon\Carbon::parse($pedidosEntregado->Fecha_Pedido)->format('d/m/Y') }}</td>
                        <td>{{ $pedidosEntregado->Direccion }}</td>
                        <td class="text-end">${{ number_format($pedidosEntregado->Importe, 2, '.', ',') }}</td>
                        <td>
                            <span class="px-4 py-0 rounded-pill border d-block"
                                style="color: #b3b3b3; white-space: nowrap; width: 100px; line-height: 2rem;">
                                @if ($pedidosEntregado->isFirst)
                                    <i class="bi bi-circle-fill" style="color: #4cc077; font-size: 14px;"></i> Si
                                @else
                                    <i class="bi bi-circle-fill" style="color: #b3b3b3; font-size: 14px;"></i> No
                                @endif
                            </span>
                        </td>
                        <td>{{ $pedidosEntregado->Comentario }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>


    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.5.0/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.5.0/js/dataTables.responsive.min.js"></script>

    <script>
        document.addEventListener('DOMContentLoaded', e => {
            setTimeout(() => {
                new DataTable('#tabla', {
                    responsive: true,
                    autoWidth: false,
                    'language': {
                        'lengthMenu': "Mostrar _MENU_ registros por página",
                        "zeroRecords": "No se encontraron resultados",
                        "loadingRecords": "Cargando...",
                        'info': "Mostrando la página _PAGE_ de _PAGES_",
                        'infoEmpty': "Mostrando la página _PAGE_ de _PAGES_",
                        'infoFiltered': "(filtrado de _MAX_ registros totales)",
                        'search': 'Buscar:',
                        'paginate': {
                            'next': 'Siguiente',
                            'previous': 'Anterior'
                        }
                    },
                });
            }, 50);
        })
    </script>
@endsection
