@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
    </div>

    {{-- Formulario  --}}
    <div class="card p-4" style="max-width: 700px; margin: auto;">
        <form method="POST" action="{{ route('costoenvio.update', ['id' => $centroVenta->IdDatCentroVenta]) }}">
            @csrf
            <div class="mb-3">
                <label>Nombre centro de venta</label>
                <input type="text" name="Descripcion" class="form-control"
                    value="{{ old('Descripcion') ?? $centroVenta->centroVenta }}"
                    placeholder="Escribe tu nombre de centro de venta" autofocus>
            </div>
            <div class="mb-3">
                <label>Ciudad</label>
                <select name="IdCatCiudad" class="form-select">
                    <option value="0">Elige una ciudad</option>
                    @foreach ($ciudades as $ciudad)
                        @if ($ciudad->Descripcion == $centroVenta->ciudad)
                            <option value="{{ $ciudad->IdCatCiudades }}" selected>{{ $ciudad->Descripcion }}</option>
                        @else
                            <option value="{{ $ciudad->IdCatCiudades }}">{{ $ciudad->Descripcion }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label>Almacen</label>
                    <input type="text" name="Almacen_Oracle" class="form-control"
                        value="{{ old('Almacen_Oracle') ?? $centroVenta->Almacen_Oracle }}"
                        placeholder="Nombre del almacen">
                </div>
                <div class="col-md-6 mb-3">
                    <label>Tipo orden oracle</label>
                    <input type="text" name="ORDER_TYPE_CLOUD" class="form-control"
                        value="{{ old('ORDER_TYPE_CLOUD') ?? $centroVenta->ORDER_TYPE_CLOUD }}" placeholder="Tipo de orden">
                </div>
            </div>
            <div class="mb-3">
                <label>Costo envio</label>
                <input type="number" name="CostoEnvio" class="form-control"
                    value="{{ old('CostoEnvio') ?? $centroVenta->CostoEnvio }}" placeholder="$ 0.00" autofocus>
            </div>
            <div>
                <label class="switch">
                    @if (old('Status') || $centroVenta->Status)
                        <input type="checkbox" name="Status" checked>
                    @else
                        <input type="checkbox" name="Status">
                    @endif
                    <span class="slider round"></span>
                </label>
                <label class="form-check-label fw-semibold text-secondary">Activo</label>
            </div>

            <div class="row mt-4">
                <div class="col-md-6 mt-3">
                    <a class="form-control btn submit-orange-outline" href="{{ route('costoenvio.index') }}">Cancelar</a>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="submit" class="form-control btn submit-orange">
                </div>
            </div>
        </form>
    </div>
@endsection
