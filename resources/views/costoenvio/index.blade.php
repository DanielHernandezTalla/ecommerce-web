@extends('layouts.app')

@section('content')
    <div class="container-title pb-4">
        @include('components.title')
        <a class="btn btn-orange rounded-pill" href="{{ route('costoenvio.create') }}">Crear centro de venta</a>
    </div>

    {{-- Tabla  --}}
    <div class="content-table card p-4">
        <table>
            <thead class="table-head">
                <tr>
                    <th class="rounded-start">Nombre</th>
                    <th>Ciudad</th>
                    <th>Almacen</th>
                    <th>Tipo orden</th>
                    <th>Costo de envio</th>
                    <th>Estatus</th>
                    <th class="rounded-end"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($centroDeVentas as $centroDeventa)
                    <tr>
                        <td>{{ $centroDeventa->centroVenta }}</td>
                        <td>{{ $centroDeventa->ciudad }}</td>
                        <td>{{ $centroDeventa->Almacen_Oracle }}</td>
                        <td>{{ $centroDeventa->ORDER_TYPE_CLOUD }}</td>
                        <td>{{ $centroDeventa->CostoEnvio }}</td>
                        <td style="color: rgb(22 163 74);">
                            <span class="px-4 py-0 rounded-pill border d-block"
                                style="color: #b3b3b3; white-space: nowrap; width: 100px; line-height: 2rem;">
                                @if ($centroDeventa->Status == 1)
                                    <i class="bi bi-circle-fill" style="color: #4cc077; font-size: 14px;"></i>
                                @else
                                    <i class="bi bi-circle-fill" style="color: #b3b3b3; font-size: 14px;"></i>
                                @endif

                                Activo
                            </span>
                        </td>
                        <td>
                            <a class="btn text-warning" href="{{ route('costoenvio.edit', ['centrodeventa' => $centroDeventa->IdDatCentroVenta]) }}">
                                <i class="bi bi-pencil-fill"></i>
                            </a>
                            <form id="delete-user" method="POST" class="d-inline"
                                action="{{ route('costoenvio.destroy', ['id' => $centroDeventa->IdDatCentroVenta]) }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn text-danger">
                                    <i class="bi bi-trash3-fill"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="d-flex justify-content-center p-4">
        {{ $centroDeVentas->links() }}
    </div>
@endsection
