let $form = "";
// Funcion para eliminar la notificacion success de cualquier evento
document.addEventListener("DOMContentLoaded", (e) => {
    // Desaparecer el aler de success
    let alertSuccess = document.querySelector("#alert-success");
    if (alertSuccess) {
        setTimeout(() => {
            document.querySelector("#main-content").removeChild(alertSuccess);
        }, 3000);
    }

    // Desaparecer el aler de error
    let alertDanger = document.querySelector("#alert-danger");
    if (alertDanger) {
        setTimeout(() => {
            document.querySelector("#main-content").removeChild(alertDanger);
        }, 5000);
    }

    // Para que se seleccione automaticamente el link del menu
    let links = document.querySelectorAll(".nav-links a");
    let url = location.href.split("/").toString();

    for (let i = 1; i < links.length; i++) {
        let linkMenu = links[i].textContent
            .replaceAll(" ", "")
            .toLocaleLowerCase();
        if (
            url
                .toLocaleLowerCase()
                .includes(
                    linkMenu.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
                )
        ) {
            links[i].parentNode.parentNode.classList.remove("dropdown-small");
            links[i].classList.add("nav-link-select");
        }
    }

    // Para reescribir las urls de la paginacion de algunas paginas
    let idPagination = document.querySelectorAll(".idPagination");
    if (idPagination) {
        idPagination.forEach((pag) => {
            let pagination = document.querySelectorAll(".page-link");
            pagination.forEach((item) => {
                if (item.href)
                    item.href += pag.value;
            });
        });
    }
});

document.addEventListener("click", (e) => {
    if (e.target.matches("#alert-button-cancel")) {
        document
            .querySelector("#alert-confirm")
            .classList.add("confirm-alert-hide");
        $form = "";
    }
    if (e.target.matches("#alert-button-confirm")) {
        document
            .querySelector("#alert-confirm")
            .classList.add("confirm-alert-hide");
        $form.submit();
        $form = "";
    }
    if (
        e.target.matches("#btn-toggle-menu") ||
        e.target.matches("#btn-toggle-menu *")
    ) {
        if (document.querySelector("nav").classList.contains("nav-hidden"))
            document.querySelector("nav").classList.remove("nav-hidden");
        else document.querySelector("nav").classList.add("nav-hidden");
    }
    if (e.target.matches(".btn-dropdown")) {
        if (e.target.parentNode.parentNode.classList.contains("dropdown-small"))
            e.target.parentNode.parentNode.classList.remove("dropdown-small");
        else e.target.parentNode.parentNode.classList.add("dropdown-small");
    }
});

document.addEventListener("submit", async (e) => {
    if (e.target.matches("#delete-user")) {
        document
            .querySelector("#alert-confirm")
            .classList.remove("confirm-alert-hide");
        e.preventDefault();
        $form = e.target;
    }
    if (e.target.matches("#imprimir-ticket")) {
        e.preventDefault();

        let pedido = e.target.querySelector(".pedido").value;

        try {
            let res = await fetch(`/ImprimirTicket/${pedido}`);
            let _data = await res.json();
            // console.log(_data);

            const response = await fetch("http://127.0.0.1:62000/ticket.php", {
                method: "POST",
                body: JSON.stringify(_data),
            });

            console.log(response);
        } catch (error) {
            console.error("ERROR");
            console.error(error);
        }
    }
});

// Evento para hacer busquedas en una tabla, ocultando, las rows que no councidan con la busqueda
document.addEventListener("keyup", (e) => {
    if (e.target.matches("#filtro")) {
        if (document.querySelector("#filtro-check"))
            document.querySelector("#filtro-check").checked = false;

        let filter = document.getElementById("filtro").value.toUpperCase();
        let rows = document.querySelectorAll("#tblStock tr");

        for (let i = 0; i < rows.length; i++) {
            td = rows[i].getElementsByTagName("td")[0];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    rows[i].style.display = "";
                } else {
                    rows[i].style.display = "none";
                }
            }
        }
    }
});

document.addEventListener("change", (e) => {
    if (e.target.matches("#filtro-check")) {
        document.querySelector("#filtro").value = "";
        let rows = document.querySelectorAll("#tblStock tr");

        for (let i = 0; i < rows.length; i++) {
            td = rows[i].getElementsByTagName("td")[2];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (e.target.checked && txtValue == 0)
                    rows[i].style.display = "";
                else if (!e.target.checked && txtValue != 0)
                    rows[i].style.display = "";
                else rows[i].style.display = "none";
            }
        }
    }
});
