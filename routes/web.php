<?php

use Illuminate\Support\Facades\Route;
// use PDF;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Inicio de sesion
Route::get('/', 'App\Http\Controllers\Auth\LoginController@index')->name('login.home');
Route::get('/login', 'App\Http\Controllers\Auth\LoginController@showLoginForm')->middleware('guest')->name('login');
Route::post('/logout', 'App\Http\Controllers\Auth\LoginController@Logout')->name('logout');
Route::post('/Autenticarse', 'App\Http\Controllers\Auth\LoginController@Autenticarse')->name('Autenticarse');
Route::get('/403', 'App\Http\Controllers\Auth\LoginController@unautorize')->name('unautorize');

// Imprimir ticket
Route::get('/ImprimirTicket/{id}', 'App\Http\Controllers\TicketController@DataImprimirTicket');
Route::post('/ImprimirTicket', 'App\Http\Controllers\TicketController@ImprimirTicket');

//Corte diario
Route::middleware('role:facturista')
    ->get('/cortediario', 'App\Http\Controllers\PedidoController@CorteDiario')
    ->name('cortediario');

//Inventario
Route::middleware('role:ejecutivo,armador,auditor,mercadotecnia')
    ->get('/inventarios', 'App\Http\Controllers\InventarioController@index')
    ->name('inventario.index');
Route::middleware('role:ejecutivo,armador,auditor,mercadotecnia')
    ->get('/inventariosprint', 'App\Http\Controllers\InventarioController@print')
    ->name('inventario.print');

// Ruta auditor
Route::middleware(['role:auditor'])->prefix('auditor')
    ->group(function () {
        //Obtener vista para editar inventario
        Route::get('/ajustedeinventario', 'App\Http\Controllers\InventarioController@edit')->name('inventario.edit');
        //GuardarAjusteInventario
        Route::post('/inventario/update/{id}', 'App\Http\Controllers\InventarioController@update')->name('inventario.update');
    });

// Ruta ejecutivo
Route::middleware(['role:ejecutivo,admin'])->prefix('ejecutivo')
    ->group(function () {
        //pedidos pendientes
        Route::get('/pedidos/pendientes', 'App\Http\Controllers\PedidoController@pendientes')->name('pedidos.pendientes');
        //pedidos preparados
        Route::get('/pedidos/preparados', 'App\Http\Controllers\PedidoController@preparados')->name('pedidos.preparados');
        //pedidos enviados
        Route::get('/pedidos/enviados', 'App\Http\Controllers\PedidoController@enviados')->name('pedidos.enviados');
        //pedidos entregados
        Route::get('/pedidos/entregados', 'App\Http\Controllers\PedidoController@entregados')->name('pedidos.entregados');

        //actualizar pedido a preparar
        Route::post('/UpdatePrepararPedido/{id}', 'App\Http\Controllers\PedidoController@UpdatePreapararPedido');
        //actualizar pedido a enviar
        Route::post('/pedidos/UpdatePedidoEnviado/{id}', 'App\Http\Controllers\PedidoController@UpdatePedidoEnviado');
        //actualizar pedido a entregar
        Route::post('/UpdatePedidoEntregado/{id}', 'App\Http\Controllers\PedidoController@UpdatePedidoEntregado');

        //exportar pedido en PDF
        Route::get('/pedidos/print/{id}', 'App\Http\Controllers\PedidoController@pedidosPrint');

        //GuardarAjusteInventario ejecutivo.inventario.edit
        Route::get('/ajustedeinventario', 'App\Http\Controllers\InventarioController@edit')->name('ejecutivo.inventario.edit');
        //GuardarAjusteInventario
        Route::post('/inventario/update/{id}', 'App\Http\Controllers\InventarioController@update')->name('ejecutivo.inventario.update');

        //Formulario facturacion
        Route::get('/facturacion', 'App\Http\Controllers\FacturacionController@index')->name('facturacion.index');
        Route::post('/facturacion/createcliente', 'App\Http\Controllers\FacturacionController@create')->name('facturacion.createcliente');
        Route::get('/facturacion/edit', 'App\Http\Controllers\FacturacionController@edit')->name('facturacion.edit');
        Route::post('/facturacion/update/{id}', 'App\Http\Controllers\FacturacionController@update')->name('facturacion.update');

        Route::get('/clientesnuevos', 'App\Http\Controllers\ClientesNuevosFacturacionController@index')->name('clientesnuevos.index');
    });

//     Almacenista
Route::middleware(['role:admin,armador'])->prefix('armador')
    ->group(function () {
        //pedidos pendientes
        Route::get('/pedidos/proceso', 'App\Http\Controllers\PedidoController@proceso')->name('pedidos.proceso');
        Route::post('/UpdatePreapararPedido/{id}', 'App\Http\Controllers\PedidoController@UpdatePorEnviar');
        Route::get('/pedidos/print/{id}', 'App\Http\Controllers\PedidoController@pedidosPrint');

        //Recepcion
        Route::get('/recepcion', 'App\Http\Controllers\RecepcionController@index')->name('recepcion.index');
        //RecepcionarProducto
        Route::post('/recepcion/store/{idRecepcion}', 'App\Http\Controllers\RecepcionController@RecepcionarProducto')->name('recepcion.store');
        //CancelarRecepcion
        Route::post('/cancelar/{idRecepcion}', 'App\Http\Controllers\RecepcionController@CancelarRecepcion')->name('recepcion.destroy');

        //Recepcion
        Route::get('/pakingslist', 'App\Http\Controllers\PakingsListController@index')->name('pakinglist.index');
        Route::get('/pakingslist/print', 'App\Http\Controllers\PakingsListController@print')->name('pakinglist.print');

        //Se muestran los productos que estan para recepcionar
        Route::get('/recepcion/manual', 'App\Http\Controllers\RecepcionController@ProductoManual')->name('recepcion.manual.index');
        //Agrega un producto a recepcion manual, aqui apenas se estan seleccionando los productos a recepcionar
        Route::post('/recepcion/manual', 'App\Http\Controllers\RecepcionController@TmpProductoManual')->name('recepcion.manual');
        //Se elimina un producto de los productos que estan en espera
        Route::post('/recepcion/EliminarProductoTmp/{idProductoTmp}', 'App\Http\Controllers\RecepcionController@EliminarProductoTmp');
        //Recepciona la lista de productos, aqui ya se recepcional y se agregan al inventario
        Route::post('/recepcion/productos', 'App\Http\Controllers\RecepcionController@TmpRecepcionarProducto')->name('recepcion.productos');
    });

// Ruta administrador
Route::middleware(['role:admin,mercadotecnia'])->prefix('admin')
    ->group(function () {

        // Categoria articulos
        Route::get('/categorias', 'App\Http\Controllers\CategoriaArticulosController@index')->name('categoriaarticulos.index');
        Route::get('/categorias/create', 'App\Http\Controllers\CategoriaArticulosController@create')->name('categoriaarticulos.create');
        Route::post('/categorias', 'App\Http\Controllers\CategoriaArticulosController@store')->name('categoriaarticulos.store');
        Route::get('/categorias/{id}/edit', 'App\Http\Controllers\CategoriaArticulosController@edit')->name('categoriaarticulos.edit');
        Route::put('/categorias/{id}', 'App\Http\Controllers\CategoriaArticulosController@update')->name('categoriaarticulos.update');
        Route::delete('/categorias/{id}', 'App\Http\Controllers\CategoriaArticulosController@destroy')->name('categoriaarticulos.destroy');

        // Articulos
        Route::get('/articulos', 'App\Http\Controllers\ArticulosController@index')->name('articulos.index');
        Route::get('/articulos/create', 'App\Http\Controllers\ArticulosController@create')->name('articulos.create');
        Route::post('/articulos', 'App\Http\Controllers\ArticulosController@store')->name('articulos.store');
        Route::get('/articulos/{articulo}/edit', 'App\Http\Controllers\ArticulosController@edit')->name('articulos.edit');
        Route::post('/articulos/{id}', 'App\Http\Controllers\ArticulosController@update')->name('articulos.update');
        Route::delete('/articulos/{id}', 'App\Http\Controllers\ArticulosController@destroy')->name('articulos.destroy');

        // Articulos por centro de venta
        Route::get('/articuloporcentro', 'App\Http\Controllers\ArticulosCentroController@index')->name('articuloscentro.index');
        Route::post('/articuloporcentro', 'App\Http\Controllers\ArticulosCentroController@addArticle')->name('articuloscentro.addArticle');
        Route::put('/articuloporcentro/update', 'App\Http\Controllers\ArticulosCentroController@update')->name('articuloscentro.update');
        Route::get('/articuloporcentro/{id}', 'App\Http\Controllers\ArticulosCentroController@destroy')->name('articuloscentro.destroy');
        // Route::delete('/articuloporlista/{id}', 'App\Http\Controllers\ArticulosListaController@destroy')->name('articuloslista.destroy');

        // Articulos por lista
        Route::get('/articuloporlista', 'App\Http\Controllers\ArticulosListaController@index')->name('articuloslista.index');
        Route::post('/articuloporlista/addarticle', 'App\Http\Controllers\ArticulosListaController@addArticle')->name('articuloslista.addArticle');
        Route::delete('/articuloporlista/{id}', 'App\Http\Controllers\ArticulosListaController@destroy')->name('articuloslista.destroy');
        // Route::get('/articulosporlista/edit', 'App\Http\Controllers\ArticulosListaController@edit')->name('articuloslista.edit');

        // Lista de precios
        Route::get('/listadeprecios', 'App\Http\Controllers\ListaPreciosController@index')->name('listadeprecios.index');
        Route::get('/listadeprecios/create', 'App\Http\Controllers\ListaPreciosController@create')->name('listadeprecios.create');
        Route::post('/listadeprecios', 'App\Http\Controllers\ListaPreciosController@store')->name('listadeprecios.store');
        Route::get('/listadeprecios/{lista}/edit', 'App\Http\Controllers\ListaPreciosController@edit')->name('listadeprecios.edit');
        Route::put('/listadeprecios/{id}', 'App\Http\Controllers\ListaPreciosController@update')->name('listadeprecios.update');
        Route::delete('/listadeprecios/{id}', 'App\Http\Controllers\ListaPreciosController@destroy')->name('listadeprecios.destroy');

        //Precios //EditarPrecio //EditarNultiplePrecio //GuardarMultiplePrecios
        Route::get('/preciosporlista', 'App\Http\Controllers\PreciosController@Precios')->name('precios.index');
        Route::post('/editarprecio/{idDatPrecio}', 'App\Http\Controllers\PreciosController@EditarPrecio')->name('EditarPrecio');
        Route::get('/preciosporlista/edit', 'App\Http\Controllers\PreciosController@EditarNultiplePrecio')->name('precios.edit');
        Route::post('/preciosporlista/guardarmultipleprecio/{IdCatListaPrecio}', 'App\Http\Controllers\PreciosController@GuardarMultiplePrecios')->name('GuardarMultiplePrecios');

        //Ciudades
        Route::get('/ciudades', 'App\Http\Controllers\CiudadesController@index')->name('ciudades.index');
        Route::get('/ciudades/create', 'App\Http\Controllers\CiudadesController@create')->name('ciudades.create');
        Route::post('/ciudades', 'App\Http\Controllers\CiudadesController@store')->name('ciudades.store');
        Route::post('/ciudades/horario/{id}', 'App\Http\Controllers\CiudadesController@horario')->name('ciudades.add.horario');
        Route::delete('/ciudades/horario/{id}', 'App\Http\Controllers\CiudadesController@horarioDestroy')->name('ciudades.destroy.horario');
        Route::get('/ciudades/{ciudad}/edit', 'App\Http\Controllers\CiudadesController@edit')->name('ciudades.edit');
        Route::post('/ciudades/{id}', 'App\Http\Controllers\CiudadesController@update')->name('ciudades.update');
        Route::delete('/ciudades/{idCiudad}', 'App\Http\Controllers\CiudadesController@destroy')->name('ciudades.destroy');

        //Codigos postales
        Route::get('/codigospostales', 'App\Http\Controllers\CodigosPostalesController@index')->name('codigospostales.index');
        Route::get('/codigospostales/create', 'App\Http\Controllers\CodigosPostalesController@create')->name('codigospostales.create');
        Route::post('/codigospostales', 'App\Http\Controllers\CodigosPostalesController@store')->name('codigospostales.store');
        Route::delete('/codigospostales/{id}', 'App\Http\Controllers\CodigosPostalesController@destroy')->name('codigospostales.destroy');

        //Centro de venta
        Route::get('/centrodeventa', 'App\Http\Controllers\CostoEnvioController@index')->name('costoenvio.index');
        Route::get('/centrodeventa/create', 'App\Http\Controllers\CostoEnvioController@create')->name('costoenvio.create');
        Route::post('/costoenvio', 'App\Http\Controllers\CostoEnvioController@store')->name('costoenvio.store');
        Route::get('/centrodeventa/{centrodeventa}/edit', 'App\Http\Controllers\CostoEnvioController@edit')->name('costoenvio.edit');
        Route::post('/costoenvio/{id}', 'App\Http\Controllers\CostoEnvioController@update')->name('costoenvio.update');
        Route::delete('/costoenvio/{id}', 'App\Http\Controllers\CostoEnvioController@destroy')->name('costoenvio.destroy');

        //Servicios
        Route::get('/servicios', 'App\Http\Controllers\HorarioAsadoController@index')->name('horarioasado.index');
        Route::get('/servicios/create', 'App\Http\Controllers\HorarioAsadoController@create')->name('horarioasado.create');
        Route::post('/servicios', 'App\Http\Controllers\HorarioAsadoController@store')->name('horarioasado.store');
        Route::get('/servicios/{servicio}/edit', 'App\Http\Controllers\HorarioAsadoController@edit')->name('horarioasado.edit');
        Route::post('/servicios/{id}', 'App\Http\Controllers\HorarioAsadoController@update')->name('horarioasado.update');
        Route::delete('/servicios/{id}', 'App\Http\Controllers\HorarioAsadoController@destroy')->name('horarioasado.destroy');

        //Clientes cloud
        Route::get('/clientescloud', 'App\Http\Controllers\ClientesCloudCentroVentaController@index')->name('clientescloud.index');
        Route::get('/clientescloud/create', 'App\Http\Controllers\ClientesCloudCentroVentaController@create')->name('clientescloud.create');
        Route::post('/clientescloud', 'App\Http\Controllers\ClientesCloudCentroVentaController@store')->name('clientescloud.store');

        //Usuarios
        Route::get('/usuarios', 'App\Http\Controllers\UserController@index')->name('usuarios');
        Route::get('/usuarios/create', 'App\Http\Controllers\UserController@create')->name('usuarios.create');
        Route::post('/usuarios', 'App\Http\Controllers\UserController@store')->name('usuarios.store');
        Route::get('/usuarios/{usuario}/edit', 'App\Http\Controllers\UserController@edit')->name('usuarios.edit');
        Route::put('/usuarios/{usuario}', 'App\Http\Controllers\UserController@update')->name('usuarios.update');
        Route::delete('/usuarios/{usuario}', 'App\Http\Controllers\UserController@destroy')->name('usuarios.destroy');

        //Roles
        Route::get('/roles', 'App\Http\Controllers\RolesController@index')->name('roles');
        Route::get('/roles/create', 'App\Http\Controllers\RolesController@create')->name('roles.create');
        Route::post('/roles', 'App\Http\Controllers\RolesController@store')->name('roles.store');
        Route::get('/roles/{rol}/edit', 'App\Http\Controllers\RolesController@edit')->name('roles.edit');
        Route::put('/roles/{rol}', 'App\Http\Controllers\RolesController@update')->name('roles.update');
        Route::delete('/roles/{rol}', 'App\Http\Controllers\RolesController@destroy')->name('roles.destroy');
    });

// Ruta Reportes
Route::middleware(['role:admin,mercadotecnia,facturista'])->prefix('reportes')
    ->group(function () {
        // Ventas
        Route::get('/reporteventas', 'App\Http\Controllers\ReportesController@ventas')->name('reporte.ventas');
        Route::get('/reporteconcentrado', 'App\Http\Controllers\ReportesController@concentrado')->name('reporte.concentrado');
        Route::get('/reporteventas/pdf', 'App\Http\Controllers\ReportesController@ventasPrint')->name('reporte.ventasprint');
        Route::get('/reporteconcentrado/pdf', 'App\Http\Controllers\ReportesController@concentradoPrint')->name('reporte.concentradoprint');
        Route::get('/cortediario/pdf', 'App\Http\Controllers\ReportesController@corteDiario')->name('reporte.cortediario');

        // Reporte de ventas fallidas
        Route::get('/ventasfallidas', 'App\Http\Controllers\ReportesController@ventasFallidas')->name('reporte.ventasfallidas');
        Route::get('/ventasfallidasprint', 'App\Http\Controllers\ReportesController@ventasFallidasPrint')->name('reporte.ventasfallidasprint');

        // Imagenes promociones
        Route::get('/imagenespromociones', 'App\Http\Controllers\ImagenesPromocionesController@index')->name('imagenespromocioens.index');
        Route::get('/imagenespromociones/create', 'App\Http\Controllers\ImagenesPromocionesController@create')->name('imagenespromocioens.create');
        Route::post('/imagenespromociones', 'App\Http\Controllers\ImagenesPromocionesController@store')->name('imagenespromocioens.store');
        Route::get('/imagenespromociones/{imagen}/edit', 'App\Http\Controllers\ImagenesPromocionesController@edit')->name('imagenespromocioens.edit');
        Route::put('/imagenespromociones/{imagen}', 'App\Http\Controllers\ImagenesPromocionesController@update')->name('imagenespromocioens.update');
        Route::delete('/imagenespromociones/{imagen}', 'App\Http\Controllers\ImagenesPromocionesController@destroy')->name('imagenespromocioens.destroy');

        // Mensajes de notificación
        Route::get('/mensajes', 'App\Http\Controllers\MensajesesController@index')->name('mensajes.index');
        Route::post('/sendmensajes', 'App\Http\Controllers\MensajesesController@send')->name('mensajes.send');
    });

Route::fallback(function () {
    return view('error.404');
});
