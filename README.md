<p align="center">
    <a href="https://ecommerce.kowi.com.mx/" target="_blank"><img src="https://ecommerce.kowi.com.mx/images/logotipo.png" width="400"></a>
</p>

<p align="center">
    <a href="https://laravel.com/docs/11.x/releases">
        <img src="https://img.shields.io/badge/%5E11.0-orange?label=Laravel" alt="Laravel 11">
    </a>
    <a href="https://getbootstrap.com/docs/5.2/getting-started/introduction/">
        <img src="https://img.shields.io/badge/5.2.2-purple?label=bootstrap" alt="Tailwind">
    </a>
    <a href="#">
        <img src="https://img.shields.io/badge/SQL-blue" alt="SQL">
    </a>
</p>

## Acerca del proyecto

Ecommerce Web es una aplicación desarrollada por Kowi diseñada para gestionar las ventas en línea de la sucursal. Esta plataforma está construida sobre el framework Laravel 11, conocido por su robustez y capacidad de desarrollo ágil. Utiliza Bootstrap como framework front-end para garantizar un diseño moderno y responsivo, adaptado a diversos dispositivos y pantallas. La base de datos subyacente está implementada en SQL, asegurando un almacenamiento eficiente y escalable de todos los datos relacionados con el comercio electrónico.

La combinación de Laravel, Bootstrap y SQL proporciona a Ecommerce Web una base sólida para gestionar de manera efectiva todas las operaciones de venta en línea. Desde la gestión de productos hasta el procesamiento de pedidos y la administración de inventarios, la aplicación está diseñada para optimizar la experiencia de los administradores, asegurando un rendimiento confiable y una interfaz intuitiva.

## Insatalación

````sql
git clone https://gitlab.com/DanielHernandezTalla/ecommerce-web.git
cd ecommerce-web

composer install
php artisan key:generate
php artisan optimize
php artisan serve
````
